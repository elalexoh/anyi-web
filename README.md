## Projecto Angie, Laravel/Vue Monolitica

## Comandos de entorno

npm run watch //-Levantar frontend (DEV)
php artisan serve //-Levantar backend

## Instalacion

composer install //-Instalacion de dependencias php(LARAVEL)
cp .env.example .env //-Creacion de archivo env se debe configurar con la bd
php artisan key:generate //-Crea el key unico de la aplicacion
npm install //-Instalacion de dependencias de js(VUE)
php artisan storage:link //-Creacion de Enlace simbolico, permite subida de imagenes
