/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
window.Vue = require("vue");
Vue.use(require('bootstrap-vue'));
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import router from "./router";
import App from "./views/App.vue";
import store from "./store";

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const app = new Vue({
    el: "#app",
    components: { App },
    router,
    store
});

// Make BootstrapVue available throughout your project
// Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
// Vue.use(IconsPlugin)
