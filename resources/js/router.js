import acudienteRoutes from "./routes/acudiente";
import docentesRoutes from "./routes/docente";
import docentesAdminRoutes from "./routes/docenteAdmin";
import generals from "./routes/generals";

import Vue from "vue";
import VueRouter from "vue-router";
import VueAxios from "vue-axios";
import axios from "axios";
import {
	docAdminAuth,
	adminAuth,
	docenteAuth,
	acudienteAuth
} from "./adminAuth";

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

import home from "./components/HomeComponent.vue";
import example from "./components/ExampleComponent.vue";

// Login Component
import login from "./components/LoginComponent/LoginComponent.vue";
import logout from "./components/LoginComponent/LogoutComponent.vue";
import resetPass from "./components/LoginComponent/ResetPasswordComponent.vue";
import checkToken from "./components/LoginComponent/CheckResetPassToken.vue";
import changePassReset from "./components/LoginComponent/ChangePasswordResetComponent.vue";

// Roles Component
import test from "./components/RolComponent/TestComponent.vue";
import registerRol from "./components/RolComponent/RolesCreateComponent.vue";
import editRol from "./components/RolComponent/RolesUpdateComponent.vue";

// Admin Component
import indexDocente from "./components/AdminComponent/DocenteComponent.vue";
import createDocente from "./components/AdminComponent/DocenteCreateComponent.vue";
import editDocente from "./components/AdminComponent/DocenteUpdateComponent.vue";
import indexCentro from "./components/AdminComponent/CentroEducativoComponent.vue";
import editCentro from "./components/AdminComponent/CentroEducativoUpdateComponent.vue";
import detailDocente from "./components/AdminComponent/DocenteDetailComponent.vue";

// ChangeUserPass
import changePass from "./components/ChangeUserPass/ChangeUserPass.vue";

// Comentarios
import indexComentarios from "./views/generals/Comentarios/index.vue";
import sendComentarios from "./views/generals/Comentarios/sendComment.vue";

// usuarios
import usuarios from "./components/Docente/Usuarios/Usuarios.vue";
import addUsuario from "./components/Docente/Usuarios/addUsuario.vue";
import editUsuario from "./components/Docente/Usuarios/editUsuario.vue";

// grados
import grados from "./components/Docente/Grados/index.vue";
import grado from "./components/Docente/Grados/grado.vue";
import estudiantesMatriculados from "./components/Docente/Grados/estudiantesMatriculados.vue";
import areas from "./components/Docente/Grados/areas.vue";
import calificarEstudiantes from "./components/Docente/Grados/calificarEstudiantes.vue";
import modificaciones from "./components/Docente/Grados/modificaciones.vue";
import detalleEstudiantesMatriculados from "./components/Docente/Grados/detalleEstudiantesMatriculados.vue";

import angieComponent from "./views/generals/angie.vue";

const routes = [
	{
		path: "/",
		name: "home",
		component: home
	},
	{
		path: "/example",
		component: example
	},

	{
		path: "/login",
		component: login
	},
	{
		path: "/logout/:sure",
		component: logout
	},
	{
		path: "/checkToken",
		component: checkToken
	},
	{
		path: "/changePassReset",
		component: changePassReset
	},
	{
		path: "/resetPass",
		component: resetPass
	},

	{
		path: "/changePass",
		component: changePass
	},

	{
		path: "/test",
		component: test,
		beforeEnter: docAdminAuth
	},
	{
		path: "/registerRol",
		component: registerRol
	},
	{
		name: "edit",
		path: "/editarRol/:id",
		component: editRol
	},

	{
		path: "/admin/indexDocentes",
		component: indexDocente,
		beforeEnter: adminAuth
	},
	{
		path: "/admin/createDocentes",
		component: createDocente,
		beforeEnter: adminAuth
	},
	{
		name: "editDocente",
		path: "/admin/editDocente/:id",
		component: editDocente,
		beforeEnter: adminAuth
	},

	{
		path: "/admin/indexCentros",
		component: indexCentro,
		beforeEnter: adminAuth
	},
	{
		name: "editCentro",
		path: "/admin/editCentro/:id",
		component: editCentro,
		beforeEnter: adminAuth
	},
	{
		name: "detailDocente",
		path: "/docentes/:id",
		component: detailDocente
	},
	{
		name: "Angie",
		path: "/angie",
		component: angieComponent
	},
	...acudienteRoutes,
	...docentesRoutes,
	...docentesAdminRoutes,
	...generals
];
export function verifyResponse(response, next) {
	let data = response;

	if (data.status == "success") {
		next();
	} else {
		alert("No estás autorizado para acceder aquí");
		next({ name: home });
	}
}
export default new VueRouter({ mode: "history", routes: routes });
