import axios from "axios";
import UsuarioService from "./services/UsuarioService";
import RutaService from "./services/RutaService";
import { verifyResponse } from "./router";

export function adminAuth(from, to, next) {
	let token = UsuarioService.getToken();
	let host = RutaService.getUrl();
	const qs = require("querystring");

	if (token) {
		axios({
			method: "post",
			url: host + "/api/auth",
			data: qs.stringify({
				rol: "ADMINISTRADOR"
			}),

			headers: {
				"content-type": "application/x-www-form-urlencoded;charset=utf-8",
				Authorization: token
			}
		})
			.then(response =>
				// console.log(response),
				verifyResponse(response.data, next)
			)
			.catch(error => alert("No estás autorizado para acceder aquí"));
	}
}

export function docAdminAuth(from, to, next) {
	let token = UsuarioService.getToken();
	let host = RutaService.getUrl();
	const qs = require("querystring");

	if (token) {
		axios({
			method: "post",
			url: host + "/api/auth",
			data: qs.stringify({
				rol: "DOCENTE ADMINISTRATIVO"
			}),

			headers: {
				"content-type": "application/x-www-form-urlencoded;charset=utf-8",
				Authorization: token
			}
		})
			.then(response =>
				// console.log(response),
				verifyResponse(response.data, next)
			)
			.catch(error => alert("No estás autorizado para acceder aquí"));
	}
}

export function docenteAuth(from, to, next) {
	let token = UsuarioService.getToken();
	let host = RutaService.getUrl();
	const qs = require("querystring");

	if (token) {
		axios({
			method: "post",
			url: host + "/api/auth",
			data: qs.stringify({
				rol: "DOCENTE"
			}),

			headers: {
				"content-type": "application/x-www-form-urlencoded;charset=utf-8",
				Authorization: token
			}
		})
			.then(response =>
				// console.log(response),
				verifyResponse(response.data, next)
			)
			.catch(error => alert("No estás autorizado para acceder aquí"));
	}
}

export function acudienteAuth(from, to, next) {
	let token = UsuarioService.getToken();
	let host = RutaService.getUrl();
	const qs = require("querystring");

	if (token) {
		axios({
			method: "post",
			url: host + "/api/auth",
			data: qs.stringify({
				rol: "ACUDIENTE"
			}),

			headers: {
				"content-type": "application/x-www-form-urlencoded;charset=utf-8",
				Authorization: token
			}
		})
			.then(response =>
				// console.log(response),
				verifyResponse(response.data, next)
			)
			.catch(error => alert("No estás autorizado para acceder aquí"));
	}
}
