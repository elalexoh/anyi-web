// usuarios
import index from "../views/docente/usuarios/index.vue";
import usuariosAdd from "../views/docente/usuarios/add.vue";
import usuariosEdit from "../views/docente/usuarios/edit.vue";
import userDetail from "../views/docente/usuarios/detail.vue";

// grados
import grados from "../views/docente/grados/index.vue";
import gradosEdit from "../views/docente/grados/edit.vue";
import gradosDetail from "../views/docente/grados/detail.vue";
import gradosAsignar from "../views/docente/grados/addGrados.vue";
import gradosAdd from "../views/docenteAdministrativo/grados/add.vue";
import estudiantesMatriculados from "../views/docente/grados/estudiantes-matriculados/index.vue";
import estudiantesMatriculadosDetail from "../views/docente/grados/estudiantes-matriculados/detail.vue";
import areas from "../views/docente/grados/areas/index.vue";
import areasEdit from "../views/docente/grados/areas/edit.vue";
import areasAdd from "../views/docente/grados/areas/add.vue";
import calificarEstudiantes from "../views/docente/grados/calificar-estudiantes/index.vue";
import notas from "../views/acudiente/estudiante/notas/index.vue";
import notasAdd from "../views/acudiente/estudiante/notas/add.vue";
import notasList from "../views/acudiente/estudiante/notas/index.vue";
import notasEdit from "../views/acudiente/estudiante/notas/edit.vue";
import notificaciones from "../views/docente/grados/notificaciones/index.vue";
import notificacionesAdd from "../views/docente/grados/notificaciones/add.vue";

// matricular estudiantes
import matricularEstudiantes from "../views/docente/matricular-estudiantes/index.vue";
import matricularEstudiantesAdd from "../views/docente/matricular-estudiantes/add.vue";
import matricularEstudiantesEdit from "../views/docente/matricular-estudiantes/edit.vue";
import matricularEstudiantesCancel from "../views/docente/matricular-estudiantes/cancel.vue";

//notificaciones
import notificacionesExtra from "../views/docente/notificaciones/index.vue";
import notificacionesExtraLoad from "../views/docente/notificaciones/load.vue";

export default [
	// usuarios
	{
		path: "/docente/usuarios",
		name: "index",
		component: index
	},
	{
		path: "/docente/usuarios/add",
		name: "usuariosAdd",
		component: usuariosAdd
	},
	{
		path: "/docente/usuarios/edit/:id",
		name: "usuariosEdit",
		component: usuariosEdit
	},
	{
		path: "/docente/usuarios/:id",
		name: "userDetail",
		component: userDetail
	},
	// grados
	{
		path: "/docente/grados",
		name: "grados",
		component: grados
	},
	{
		path: "/docente/grado/:id/edit",
		name: "gradosEdit",
		component: gradosEdit
	},
	{
		path: "/docente/grados/areas",
		name: "areas",
		component: areas
	},
	{
		path: "/docente/grados/areas/add",
		name: "areasAdd",
		component: areasAdd
	},
	{
		path: "/docente/grados/areas/:id",
		name: "areasEdit",
		component: areasEdit
	},
	{
		path: "/docente/grados/estudiantes-matriculados",
		name: "estudiantesMatriculados",
		component: estudiantesMatriculados
	},
	{
		path: "/docente/grados/estudiantes-matriculados/:id",
		name: "estudiantesMatriculadosDetail",
		component: estudiantesMatriculadosDetail
	},
	{
		path: "/docente/grado/:id/estudiantes",
		name: "calificarEstudiantes",
		component: calificarEstudiantes
	},
	{
		path: "/docente/estudiante/:id/notas",
		name: "notas",
		component: notas
	},
	{
		path: "/docente/grados/:id_grado/estudiante/:id/notas",
		name: "notasList",
		component: notasList
	},
	{
		path: "/docente/grados/:id_grado/estudiante/:id/notas/add",
		name: "notasAdd",
		component: notasAdd
	},
	{
		path: "/docente/grados/estudiante/:id_estudiante/notas/:id_nota/edit",
		name: "notasEdit",
		component: notasEdit
	},
	{
		path: "/docente/grados/notificaciones",
		name: "notificacionesDocente",
		component: notificaciones
	},
	{
		path: "/docente/grados/notificaciones/add",
		name: "notificacionesAdd",
		component: notificacionesAdd
	},
	{
		path: "/docente/grados/add",
		name: "gradoAñadir",
		component: gradosAdd
	},

	{
		path: "/docente/grados/asignar",
		name: "gradosAsignar",
		component: gradosAsignar
	},
	{
		path: "/docente/grados/:id",
		name: "gradosDetail",
		component: gradosDetail
	},

	// matricular estudiante
	{
		path: "/docente/matricular-estudiante",
		name: "matricularEstudiantes",
		component: matricularEstudiantes
	},
	{
		path: "/docente/matricular-estudiante/matricula/:id",
		name: "matricularEstudiantesEdit",
		component: matricularEstudiantesEdit
	},
	{
		path: "/docente/matricular-estudiante/add",
		name: "matricularEstudiantesAdd",
		component: matricularEstudiantesAdd
	},
	{
		path: "/matricula/:id",
		name: "matriculaDetalle",
		component: matricularEstudiantesCancel
	},
	{
		path: "/docente/matricular-estudiante/cancel/:id",
		name: "matricularEstudiantesCancel",
		component: matricularEstudiantesCancel
	},
	{
		path: "/docente/notificaciones",
		name: "notificacionesExtra",
		component: notificacionesExtra
	},
	{
		path: "/docente/notificacion/:id/load",
		name: "notificacionesExtraLoad",
		component: notificacionesExtraLoad
	}
];
