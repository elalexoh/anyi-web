// matricular estudiantes
import solicitudesAcademicas from "../views/docenteAdministrativo/solicitudes-academicas/index.vue";
import solicitudesAcademicasDetail from "../views/docenteAdministrativo/solicitudes-academicas/edit.vue";
import certificadoAdd from "../views/docenteAdministrativo/solicitudes-academicas/certificados/index.vue";

// matricular estudiantes
import certificados from "../views/docenteAdministrativo/certificados/index.vue";
export default [
    // solicitudes-acedemicas
    {
        path: "/docenteAdministrativo/solicitudes",
        name: "solicitudesAcademicas",
        component: solicitudesAcademicas
    },
    {
        path:
            "/docenteAdministrativo/solicitudes/sede/:id_sede/solicitud/:id_solicitud",
        name: "solicitudesAcademicasDetail",
        component: solicitudesAcademicasDetail
    },
    {
        path: "/docenteAdministrativo/solicitudes/sede/nuevo-certificado",
        name: "certificadoAdd",
        component: certificadoAdd
    },
    // certificados
    {
        path: "/docenteAdministrativo/certificados",
        name: "certificados",
        component: certificados
    }

    //     // comentarios
    // {
    //     path: "/docenteAdministrativo/comentarios",
    //     name: 'home',
    //     component: home
    // }
];
