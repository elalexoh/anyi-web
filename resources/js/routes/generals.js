import comentarios from "../views/generals/Comentarios/index.vue";
import comentariosAdd from "../views/generals/Comentarios/sendComment.vue";
import politicas from "../views/generals/policies.vue";
import politicasAceptadas from "../views/generals/policies-accepted.vue";
export default [
	{
		path: "/comentarios",
		name: "comentarios",
		component: comentarios
	},
	{
		path: "/comentarios/add",
		name: "comentariosAdd",
		component: comentariosAdd
	},
	{
		path: "/politicas",
		name: "politicas",
		component: politicas
	},
	{
		path: "/politicas-de-privacidad",
		name: "Politicas",
		component: politicasAceptadas
	}
];
