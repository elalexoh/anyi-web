// estudiantes
import index from "../views/acudiente/estudiante/index.vue";
import estudianteDetail from "../views/acudiente/estudiante/detail.vue";
// matricula
// import matriculas from "../views/acudiente/estudiante/matricular-estudiante/index.vue";
import matriculasAdd from "../views/acudiente/estudiante/matricular-estudiante/add.vue";
import matriculasEdit from "../views/acudiente/estudiante/matricular-estudiante/edit.vue";
import notificaciones from "../views/acudiente/estudiante/matricular-estudiante/notificaciones.vue";
// notas
import notas from "../views/acudiente/estudiante/notas/index.vue";
// certificados
import certificados from "../views/acudiente/estudiante/solicitar-certificados/index.vue";
import certificadosAdd from "../views/acudiente/estudiante/solicitar-certificados/add.vue";
import certificadosEdit from "../views/acudiente/estudiante/solicitar-certificados/edit.vue";
export default [
	// estudiantes
	{
		path: "/acudiente/estudiantes",
		name: "Estudiantes",
		component: index
	},
	{
		path: "/acudiente/estudiante/:id",
		name: "DetailStudent",
		component: estudianteDetail
	},
	// matricular
	// {
	//     path: "/acudiente/estudiantes/matriculas",
	//     name: "matriculas",
	//     component: matriculas
	// },
	{
		path: "/acudiente/estudiantes/matricular/edit",
		name: "matriculasEdit",
		component: matriculasEdit
	},
	{
		path: "/acudiente/estudiantes/matricular/add",
		name: "matriculasAdd",
		component: matriculasAdd
	},
	{
		path: "/acudiente/estudiantes/matricular/notificaciones",
		name: "notificaciones",
		component: notificaciones
	},
	// notas
	// {
	//     path: "/acudiente/estudiantes/notas",
	//     name: "notas",
	//     component: notas
	// },
	// certificados
	{
		path: "/acudiente/estudiantes/certificados",
		name: "certificados",
		component: certificados
	},
	{
		path: "/acudiente/estudiantes/certificados/edit",
		name: "certificadosEdit",
		component: certificadosEdit
	},
	{
		path: "/acudiente/estudiantes/certificados/add",
		name: "certificadosAdd",
		component: certificadosAdd
	}
	// comentarios
	// {
	//     path: "/acudiente/estudiantes/comentarios",
	//     name: home,
	//     component: home
	// }
];
