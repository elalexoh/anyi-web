import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        sede: "1"
    },
    mutations: {
        UPDATE_SEDE(state, sede) {
            state.sede = sede;
        }
    },
    getters: {
        getSedeInfo(state) {
            return state.sede;
        }
    },
    actions: {
        setSede({ commit }, sede) {
            commit("UPDATE_SEDE", sede);
        }
    }
});
