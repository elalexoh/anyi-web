import RutaService from "../services/RutaService";
import { mapActions } from "vuex";
export default {
	data() {
		return {
			host: RutaService.getUrl()
		};
	},
	methods: {
		stringSearch(string, query) {
			const str = string;
			const n = str.search(query);
			let response = false;
			if (n > 0) {
				response = true;
			}
			return response;
		},
		nullField(field) {
			return field == "null" || field === null ? "" : field;
		},
		getSede(userInfo) {
			const { id_user } = userInfo;
			console.debug(`${this.host}/api/docadmin/sede/${id_user}`);
			this.axios
				.get(`${this.host}/api/docadmin/sede/${id_user}`)
				.then(({ data }) => {
					console.debug(data);
					this.sede = data;
					this.setSede(data);
				})
				.catch(e => {
					console.debug("error al obtener informacion de la sede", e);
				});
		},
		...mapActions(["setSede"])
	}
};
