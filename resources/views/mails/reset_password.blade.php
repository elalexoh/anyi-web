
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Restablecimiento de Contraseña</title>
</head>
<body>
    <p>Hola! Esto es el cuerpo del email</p>
    <p>Estos son los datos del usuario que ha realizado la solicitud de restablecimiento de contraseña:</p>
    <ul>
        <li>Nombre de usuario:  {{ $usuarios->nombre_usuario }}</li>
        <li>Código de Recuperación:  {{ $reset_pass->token_recuperacion }}</li>
    </ul>
    </ul>
</body>
</html>