<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
  <div id="app">
    <app></app>
  </div>
</body>
<footer class="footer position-fixed w-100 d-flex justify-content-center align-items-center" style="background-color: #343a40; bottom:0; height: 50px; z-index: 100;">
  <div class="py-3 text-center text-white footer-copyright ">
    2020 © Derechos Reservados,
    <a href="#" class="font-weight-bold text-decoration-none"> Angie </a>
  </div>
</footer>

</html>