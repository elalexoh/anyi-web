-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 05-04-2021 a las 20:09:36
-- Versión del servidor: 10.2.3-MariaDB-log
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `angie_centroeducativo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id` int(255) NOT NULL,
  `nombre` varchar(56) NOT NULL,
  `intensidad` tinyint(4) NOT NULL,
  `fk_periodo_academico` int(255) NOT NULL,
  `fk_grado` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`id`, `nombre`, `intensidad`, `fk_periodo_academico`, `fk_grado`) VALUES
(8, 'Castellano', 1, 3, 1),
(9, 'Castellano I', 1, 1, 8),
(10, 'Castellano II', 1, 1, 15),
(11, 'Castellano III', 1, 1, 16),
(12, 'Castellano IV', 1, 1, 17),
(15, 'Matematica I', 1, 1, 18),
(16, 'Castellano II', 2, 1, 19),
(18, 'Ingles basico', 1, 1, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centros_educativos`
--

CREATE TABLE `centros_educativos` (
  `identificacion` int(255) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `resolucion` varchar(128) NOT NULL,
  `dane` bigint(20) NOT NULL,
  `nit` varchar(128) NOT NULL,
  `consecutivo_sede` bigint(20) NOT NULL,
  `nombre_autorizado` varchar(128) NOT NULL,
  `cargo` varchar(128) NOT NULL,
  `img_logo` varchar(256) NOT NULL,
  `img_firma` varchar(256) NOT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `dni_director` varchar(15) DEFAULT NULL,
  `lugar_expedicion` varchar(56) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `centros_educativos`
--

INSERT INTO `centros_educativos` (`identificacion`, `nombre`, `resolucion`, `dane`, `nit`, `consecutivo_sede`, `nombre_autorizado`, `cargo`, `img_logo`, `img_firma`, `direccion`, `dni_director`, `lugar_expedicion`) VALUES
(1, 'Santa Emilia', 'Barry lorem2', 7635352176352, 'Rudyard Davenport', 266088000196, 'Harper, Jade C.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/GOJCJzYQdjLo1oYxrz4zU7royBObFYfltMNcQCzH.png', 'http://127.0.0.1:8000/storage/centros/firmas/XIpO52DEcQ3ORsPxsLzayvAS7SWmRoeQUcIS6SuY.png', 'Municipio de Belén, Risaralda', '25211817', 'Bogota (Cundinamarca)2'),
(2, 'Luctus Aliquet Odio Institute', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(3, 'Centro 3', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(4, 'Centro 4', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(5, 'Centro 5', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(6, 'Centro 6', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(7, 'Centro 7', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(8, 'Centro 8', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(9, 'Centro 9', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(10, 'Centro 10', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(11, 'Centro 11', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)'),
(12, 'Centro 12', 'Ivor', 239483749874, 'Amery Carrillo', 266088000187, 'Lambert, Alika S.', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/n5bPstUOE6po8xVFmcn7IIJfitUljanIcB8LWi3R.png', 'http://127.0.0.1:8000/storage/centros/firmas/NNemaIYpmgeEaXj2SbiiPYoc2DeYdI7U8RcWN5ax.png', 'Municipio de Belén, Risaralda', '12312312', 'Bogota (Cundinamarca)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificados`
--

CREATE TABLE `certificados` (
  `id` int(255) NOT NULL,
  `tipo` varchar(56) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` varchar(32) DEFAULT NULL,
  `url_certificado` varchar(256) DEFAULT NULL,
  `fk_usuario` int(255) NOT NULL COMMENT 'Docente',
  `fk_persona` int(255) NOT NULL COMMENT 'Estudiante',
  `motivo` varchar(255) DEFAULT NULL,
  `firmado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `certificados`
--

INSERT INTO `certificados` (`id`, `tipo`, `descripcion`, `estatus`, `url_certificado`, `fk_usuario`, `fk_persona`, `motivo`, `firmado`) VALUES
(5, 'estudio', 'sda', 'RECHAZADO', NULL, 2, 93, 'rechazado', 0),
(6, 'estudio', 'asd', 'RECHAZADO', NULL, 4, 93, 'Rechazada por xyz', 1),
(11, 'estudio', 'PRUEBA', 'RECHAZADO', NULL, 4, 94, 'hey', 1),
(12, 'paz y salvo', 'PRUEBA', 'APROBADO', NULL, 4, 94, 'Rechazada por prueba', 1),
(13, 'notas_full', 'Solicitud de notas completas', 'ENVIADO', 'http://127.0.0.1:8000/storage/usuarios/documentos//rMr4Mv571U7wk3lrXKnGS0Tn8lGOcBui3xJxCYLF.pdf', 3, 93, NULL, 0),
(14, 'notas_full', 'Solicitud de notas completas', 'ENVIADO', 'http://127.0.0.1:8000/storage/usuarios/documentos//K2FIbayGtK4R2rjXBcpVFXsQPEKGQk6bhlGHRwLQ.pdf', 3, 99, NULL, 0),
(15, 'notas_full', 'Solicitud de notas completas', 'ENVIADO', 'http://127.0.0.1:8000/storage/usuarios/documentos//e0eawzvTbumt85rILvpwJ3lkSs5upfakALVFf4tz.pdf', 3, 94, NULL, 0),
(16, 'estudio', 'asdasd', 'APROBADO', NULL, 2, 93, NULL, 1),
(17, 'estudio', 'asdasd', 'APROBADO', NULL, 2, 93, NULL, 1),
(18, 'paz y salvo', 'asdasasd', 'PENDIENTE', NULL, 2, 93, NULL, 0),
(19, 'estudio', 'asdasd', 'PENDIENTE', NULL, 4, 93, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos_electronicos`
--

CREATE TABLE `correos_electronicos` (
  `id` int(255) NOT NULL,
  `direccion_correo` varchar(128) NOT NULL,
  `fk_persona` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `correos_electronicos`
--

INSERT INTO `correos_electronicos` (`id`, `direccion_correo`, `fk_persona`) VALUES
(1, 'frederick.gonzalez@jirehpro.co', 78),
(25, 'prueba1@gmail.com', 77),
(26, 'prueba2@gmail.com', 24),
(27, 'prueba3@gmail.com', 32),
(35, 'prueba4@gmail.com', 92);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiantes_areas`
--

CREATE TABLE `estudiantes_areas` (
  `id` int(255) NOT NULL,
  `nota_parcial` decimal(3,1) DEFAULT NULL,
  `fk_estudiante` int(255) NOT NULL,
  `fk_areas` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiantes_areas`
--

INSERT INTO `estudiantes_areas` (`id`, `nota_parcial`, `fk_estudiante`, `fk_areas`) VALUES
(1, '12.0', 94, 18),
(2, '12.0', 94, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiantes_grados`
--

CREATE TABLE `estudiantes_grados` (
  `id` int(255) NOT NULL,
  `fk_estudiante` int(11) NOT NULL,
  `fk_grado` int(11) NOT NULL,
  `estatus` varchar(25) NOT NULL DEFAULT 'cursando'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiantes_grados`
--

INSERT INTO `estudiantes_grados` (`id`, `fk_estudiante`, `fk_grado`, `estatus`) VALUES
(3, 93, 8, 'cursando'),
(4, 94, 17, 'cursando'),
(5, 93, 1, 'cursando'),
(6, 93, 15, 'cursando'),
(7, 93, 16, 'cursando'),
(8, 93, 17, 'cursando'),
(9, 95, 1, 'cursando'),
(10, 99, 1, 'cursando'),
(11, 93, 18, 'cursando'),
(13, 94, 18, 'cursando'),
(14, 99, 19, 'cursando'),
(15, 93, 20, 'cursando'),
(16, 93, 16, 'cursando'),
(17, 95, 18, 'cursando'),
(18, 99, 18, 'cursando'),
(19, 93, 21, 'cursando'),
(20, 94, 1, 'cursando'),
(21, 94, 8, 'cursando');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familiares`
--

CREATE TABLE `familiares` (
  `id` int(255) NOT NULL,
  `fk_estudiante` int(11) NOT NULL,
  `fk_parentesco` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `familiares`
--

INSERT INTO `familiares` (`id`, `fk_estudiante`, `fk_parentesco`) VALUES
(6, 93, 77),
(7, 94, 77),
(8, 95, 77),
(9, 99, 77);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros`
--

CREATE TABLE `foros` (
  `id` int(255) NOT NULL,
  `tipo` varchar(56) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `estatus` varchar(32) DEFAULT NULL,
  `fk_usuario_comenta` int(255) NOT NULL,
  `fk_usuario_notifica` int(255) DEFAULT NULL,
  `fk_estudiante` int(255) DEFAULT NULL,
  `fk_cancela_matricula` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `foros`
--

INSERT INTO `foros` (`id`, `tipo`, `descripcion`, `fecha_creacion`, `estatus`, `fk_usuario_comenta`, `fk_usuario_notifica`, `fk_estudiante`, `fk_cancela_matricula`) VALUES
(1, 'FORO', 'Hola mundo!', '2021-03-24 16:06:55', 'VISTO', 2, 3, NULL, NULL),
(2, 'FORO', 'Como estas?', '2021-03-24 16:07:39', 'VISTO', 3, 2, NULL, NULL),
(3, 'FORO', 'hola necesito un certificado', '2021-03-24 16:42:13', 'VISTO', 4, 2, NULL, NULL),
(4, 'FORO', 'Hey friend que tal estas?', '2021-03-25 14:16:49', 'NO VISTO', 3, 1, NULL, NULL),
(5, 'FORO', 'hola que tal?', '2021-03-30 14:30:15', 'NO VISTO', 3, 4, NULL, NULL),
(6, 'FORO', 'Hola necesito un favor', '2021-03-30 15:25:18', 'NO VISTO', 4, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grados`
--

CREATE TABLE `grados` (
  `id` int(255) NOT NULL,
  `nombre` varchar(24) NOT NULL,
  `fk_periodo_academico` int(255) NOT NULL,
  `fk_persona` int(255) NOT NULL COMMENT 'esto corresponde al docente'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grados`
--

INSERT INTO `grados` (`id`, `nombre`, `fk_periodo_academico`, `fk_persona`) VALUES
(1, 'Preescolar', 3, 32),
(8, 'Primer grado', 1, 32),
(15, 'Segundo', 1, 32),
(16, 'Tercer', 1, 32),
(17, 'Cuarto', 1, 32),
(18, 'Primer', 1, 24),
(19, 'Segundo', 1, 24),
(20, 'Tercer Grado', 1, 92),
(21, 'Prueba', 1, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informaciones_adicionales`
--

CREATE TABLE `informaciones_adicionales` (
  `id` int(255) NOT NULL,
  `tipo` varchar(64) NOT NULL,
  `enfermedades_sufridas` text DEFAULT NULL,
  `sisben` text DEFAULT NULL,
  `estrato_socioeconimico` varchar(64) DEFAULT NULL,
  `nro_hermanos` tinyint(4) DEFAULT NULL,
  `posicion_hermanos` varchar(24) DEFAULT NULL,
  `procedencia` varchar(156) DEFAULT NULL,
  `nuevo` tinyint(1) DEFAULT NULL,
  `repitiente` tinyint(1) DEFAULT NULL,
  `fk_matricula` int(255) NOT NULL,
  `fk_estudiante` int(255) NOT NULL,
  `p_nombre_mama` varchar(255) DEFAULT NULL,
  `s_nombre_mama` varchar(255) DEFAULT NULL,
  `p_apellido_mama` varchar(255) DEFAULT NULL,
  `s_apellido_mama` varchar(255) DEFAULT NULL,
  `dni_madre` int(10) DEFAULT NULL,
  `p_nombre_padre` varchar(255) DEFAULT NULL,
  `s_nombre_padre` varchar(255) DEFAULT NULL,
  `p_apellido_padre` varchar(255) DEFAULT NULL,
  `s_apellido_padre` varchar(255) DEFAULT NULL,
  `lugar_expedicion` varchar(255) NOT NULL,
  `lugar_nacimiento` varchar(255) NOT NULL,
  `rh` varchar(20) NOT NULL,
  `dni_padre` int(10) DEFAULT NULL,
  `fk_grado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informaciones_adicionales`
--

INSERT INTO `informaciones_adicionales` (`id`, `tipo`, `enfermedades_sufridas`, `sisben`, `estrato_socioeconimico`, `nro_hermanos`, `posicion_hermanos`, `procedencia`, `nuevo`, `repitiente`, `fk_matricula`, `fk_estudiante`, `p_nombre_mama`, `s_nombre_mama`, `p_apellido_mama`, `s_apellido_mama`, `dni_madre`, `p_nombre_padre`, `s_nombre_padre`, `p_apellido_padre`, `s_apellido_padre`, `lugar_expedicion`, `lugar_nacimiento`, `rh`, `dni_padre`, `fk_grado`) VALUES
(3, 'registro_civil', 'PRUEBA_ACU', 'PRUEBA_ACU', 'PRUEBA_ACU', 12, 'PRUEBA_ACU', 'PRUEBA_ACU', 1, 1, 6, 93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alturas', 'PRUEBA_ACU', 'PRUEBA_ACU', NULL, 8),
(4, 'registro_civil', 'PRUEBA', 'PRUEBA', 'PRUEBA', 12, 'PRUEBA', 'PRUEBA', 1, 1, 7, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alturas', 'PRUEBA', 'PRUEBA', NULL, 17),
(5, 'registro_civil', 'FILETEST', 'FILETEST', 'FILETEST', 2, 'FILETEST', 'FILETEST', 1, 1, 8, 95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alturas', 'FILETEST', 'FILETEST', NULL, 1),
(6, 'registro_civil', 'Estudiante', 'Estudiante', 'Estudiante', 2, 'Estudiante', 'Estudiante', 1, 1, 9, 99, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Alturas', 'Prueba', 'Estudiante', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugares`
--

CREATE TABLE `lugares` (
  `id` int(255) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `tipo` varchar(32) NOT NULL,
  `fk_lugar` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lugares`
--

INSERT INTO `lugares` (`id`, `nombre`, `tipo`, `fk_lugar`) VALUES
(1, 'Colombia', 'Pais', NULL),
(3, 'Bogota', 'Estado', 1),
(5, 'Alturas', 'Municipio', 3),
(17, 'Antioquia', 'Estado', 1),
(18, 'Medellin', 'Municipio', 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculas`
--

CREATE TABLE `matriculas` (
  `id` int(255) NOT NULL,
  `fecha_matricula` date NOT NULL,
  `img_firma_acudiente` varchar(256) DEFAULT NULL,
  `img_firma_estudiante` varchar(256) DEFAULT NULL,
  `fk_estudiante` int(255) NOT NULL,
  `estatus` varchar(56) DEFAULT NULL,
  `fecha_renovacion` date DEFAULT NULL,
  `religion` tinyint(1) DEFAULT NULL,
  `img_cedula_papa` varchar(256) DEFAULT NULL,
  `img_cedula_mama` varchar(256) DEFAULT NULL,
  `img_firma_familiar` varchar(256) DEFAULT NULL,
  `img_cedula_acudiente` varchar(256) DEFAULT NULL,
  `img_cedula_estudiante` varchar(256) DEFAULT NULL,
  `notas` varchar(256) NOT NULL,
  `tipo_estudiante` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `matriculas`
--

INSERT INTO `matriculas` (`id`, `fecha_matricula`, `img_firma_acudiente`, `img_firma_estudiante`, `fk_estudiante`, `estatus`, `fecha_renovacion`, `religion`, `img_cedula_papa`, `img_cedula_mama`, `img_firma_familiar`, `img_cedula_acudiente`, `img_cedula_estudiante`, `notas`, `tipo_estudiante`) VALUES
(6, '2021-03-01', 'http://127.0.0.1:8000/storage/matriculas/firmas/acudientes/HPM2qkgCZ61yf7Y7rKJNGwD7lYRziTU2d3DPptoE.png', 'http://127.0.0.1:8000/storage/matriculas/firmas/estudiantes/Y5czNCDhemlakzW7NJZMudgGitvaekfTng6uFLoc.png', 93, 'CANCELADO', '2021-03-24', 1, 'http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage/matriculas/identificaciones/padres/gesYsmNpFkaaIA1cwSDmq3q6vbcwrThwsXiL4jyg.pdf', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/madres/AYoGr6cw6F5tI5gpzqou7PK2GolcDvrz0xRaW1hK.pdf', 'http://127.0.0.1:8000/storage/matriculas/firmas/familiares/QqYdLvwKS7nuwm5GCj7CebeStlP8KGrRtx6bw2gB.pdf', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/acudientes/rY8RJcZVNoQelTjHlJWySbotjxKzxqRFaEZs8IUt.pdf', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/estudiantes/FdlRWUHg4nIe7bWy0u3wsvVk67zl1F8K6QrPhHKB.jpg', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/notas/t3NtnsMiYrruSd76gUIEnIO3Bj0U2n2TVB8lMUQ0.jpg', NULL),
(7, '2021-03-01', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 94, 'Pendiente', '2021-03-22', 1, 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/madres/zRE9LyB7voCDH8MpEd1KjKAs1l71kFQUNRhM4ihS.jpg', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/acudientes/h3LW0CXlXNuVsZ9ocy1AoifMSrvtu1ZzX0XRnqly.png', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/estudiantes/2TwNn52OUoVQUia9Lok1CUv1xhbKS9VgAxvQtsSG.jpg', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/notas/hm1UBckHn3s2kkA9lKAzjhoRG2hFQ1Avd1rbVqet.jpg', NULL),
(8, '2021-03-03', 'http://127.0.0.1:8000/storage/matriculas/firmas/acudientes/ZJ0Rd0vmqarzog6mcMarVBRCZA5HABrXaTCI21XB.pdf', 'http://127.0.0.1:8000/storage/matriculas/firmas/estudiantes/uuJz7qImggeEFWim2llPa1UbspU7SKhjeOk62D28.pdf', 95, 'Pendiente', '2021-03-22', 1, 'http://127.0.0.1:8000/storage/matriculas/identificaciones/padres/n99CdQZr5MGwnqMPRPOeJouVmCzD0I4zQGN9QJxv.jpg', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/madres/TNRNSvJcFb0ECNhqfyqylfwtmCXuyxh9utmctY0d.pdf', 'http://127.0.0.1:8000/storage/matriculas/firmas/familiares/eFNHaeoeSsGSiYVur8Oab1UOf6EJzHwAiaCkB0fY.pdf', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/acudientes/Red7lnHxhtObJZbqb6oTRgrsWHuS3nBUvdTUTIny.pdf', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/estudiantes/pd7yzPqODSHOJ5dW4Qluc0JZZspvHKAOQGfst4zw.pdf', 'http://127.0.0.1:8000/storage/matriculas/notas/Yntl2ygUX6NDBSc2YZGKGxxvS76V8B5wGL2qPsxl.pdf', NULL),
(9, '2021-03-01', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 99, 'Rechazada', NULL, 1, 'http://127.0.0.1:8000/storage/matriculas/identificaciones/padres/gYM9h7qMf0vzXHgugmApfQP6IFSsyXPuv9onft3l.pdf', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/madres/iWLwWw5ldXB6183buKCTZQ5QtzJX5odsAej5cXSl.png', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculas_canceladas`
--

CREATE TABLE `matriculas_canceladas` (
  `id` int(255) NOT NULL,
  `motivo_cancelacion` text NOT NULL,
  `destino_estudiante` text NOT NULL,
  `fecha_cancelacion` date DEFAULT NULL,
  `fk_matricula` int(255) NOT NULL,
  `fk_estudiante` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `matriculas_canceladas`
--

INSERT INTO `matriculas_canceladas` (`id`, `motivo_cancelacion`, `destino_estudiante`, `fecha_cancelacion`, `fk_matricula`, `fk_estudiante`) VALUES
(1, 'Me mudo a otro país', 'PRUEBA', '2021-03-01', 6, 93);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_academicos`
--

CREATE TABLE `periodos_academicos` (
  `id` int(255) NOT NULL,
  `nombre` varchar(32) DEFAULT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `periodos_academicos`
--

INSERT INTO `periodos_academicos` (`id`, `nombre`, `fecha_inicio`, `fecha_fin`) VALUES
(1, 'Primero', '2020-12-25', '2020-12-25'),
(2, 'Segundo', '2020-12-25', '2020-12-25'),
(3, 'Tercero', '2021-10-12', '2021-02-26'),
(4, 'Cuarto', '2020-12-25', '2020-12-25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(255) NOT NULL,
  `p_nombre` varchar(64) NOT NULL,
  `s_nombre` varchar(64) DEFAULT NULL,
  `p_apellido` varchar(64) NOT NULL,
  `s_apellido` varchar(64) DEFAULT NULL,
  `tipo` varchar(56) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `img_foto` varchar(256) DEFAULT NULL,
  `dni` int(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `fk_sede` int(255) NOT NULL,
  `sexo` varchar(25) NOT NULL DEFAULT 'masculino'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `p_nombre`, `s_nombre`, `p_apellido`, `s_apellido`, `tipo`, `fecha_nacimiento`, `img_foto`, `dni`, `direccion`, `fk_sede`, `sexo`) VALUES
(24, 'DocenteADM', '', 'Administrativo', NULL, 'DOCENTE ADMINISTRATIVO', '2016-12-08', 'http://127.0.0.1:8000/storage/usuarios/fotos//FI2aXQjWieY4OHmk37vvR2OIw2BthUlHkQfTKSj7.png', 20202, 'lorem12', 1, 'masculino'),
(32, 'asdasd@gmail.com', 'asd', 'asd', 'null', 'Docente', '2016-12-01', 'http://127.0.0.1:8000/storage/usuarios/fotos//lExUWOJjsspUHMIR8rTySznu1mZIitalPWiCdXnk.jpg', 2021, 'adsasd', 1, 'masculino'),
(77, 'Acudiente', 'null', 'Acudiente', 'null', 'acudiente', '2021-03-17', NULL, 2022, 'prueba', 1, 'masculino'),
(78, 'Administrador', NULL, 'Administrador', NULL, 'Administrador', '2021-03-02', NULL, 2023, 'prueba', 1, 'masculino'),
(92, 'RESPONSABLE', 'Responsable', 'RESPONSABLE', NULL, 'DOCENTE ADMINISTRATIVO', '2016-12-09', 'http://127.0.0.1:8000/storage/usuarios/fotos//2Oj1xIh4BKf07fEvfcl3xgt1DQ33R5GAmF3HtLHZ.png', 25252525, 'RESPONSABLE', 2, 'masculino'),
(93, 'PRUEBA_ACU_UP', NULL, 'PRUEBA_ACU', NULL, 'estudiante', '2021-03-04', NULL, 121219, 'ahora tengo otra direccion', 1, 'masculino'),
(94, 'Jasmine', NULL, 'Jasmine', NULL, 'estudiante', '2021-03-02', NULL, 45698721, 'PRUEBA', 1, 'masculino'),
(95, 'FILETEST', NULL, 'FILETEST', NULL, 'estudiante', '2021-03-06', NULL, 3215555, 'FILETEST', 2, 'masculino'),
(98, 'ACUDIENTE', NULL, 'ACUDIENTE', NULL, 'ACUDIENTE', '2021-03-02', 'http://127.0.0.1:8000/storage/usuarios/fotos//qUf3eIXpCZDuk0kXVdLqjjpOrM0zyqUM83AdlA5Q.png', 99999999, 'PRUEBA', 1, 'masculino'),
(99, 'Estudiante', NULL, 'Estudiante', NULL, 'estudiante', '2021-03-18', NULL, 77777777, 'Estudiante', 1, 'masculino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reset_pass`
--

CREATE TABLE `reset_pass` (
  `id` int(255) NOT NULL,
  `token_recuperacion` varchar(128) NOT NULL,
  `fecha_solicitud` datetime NOT NULL,
  `fk_usuario` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reset_pass`
--

INSERT INTO `reset_pass` (`id`, `token_recuperacion`, `fecha_solicitud`, `fk_usuario`) VALUES
(1, '352fd6f6aa5db413c81d7644566eea8ddfe12b4f9b2d6f4905f2c92708fa0da50f35e70decb6e51995246d139dc161b6f770cf7fefb07f9f443d3e12f1bf3bf7', '2021-03-17 21:06:03', 1),
(2, '13cc10a7848e77828c5a83cdb8f277adb72717c4ca4ce4ab0b1e45ad66324c62c7c9086fc65e435753a81e4379ecf01a753d69d080dd9d6bcde5a000f84fbeb8', '2021-03-17 21:07:21', 1),
(3, '7356e5349b70a9ae8ada20cfe644444a3c35140d9fe98e82a3f9f3df869bf39e1405fd45bf4363dbe8966c47a182760a97c3e058bfcbae5df08d39d376a4b3a5', '2021-03-18 19:19:26', 2),
(4, 'b2c6b9c0db8c6b7bfa86df8a42c31320274079673c6e089a26ea41cf4e40ccc57c085af445370ae2633b0730c4cff678e228e00838958077d838f1fa4a6962a0', '2021-03-24 15:01:35', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(255) NOT NULL,
  `nombre` varchar(56) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'ADMINISTRADOR'),
(2, 'DOCENTE ADMINISTRATIVO'),
(3, 'DOCENTE'),
(4, 'ACUDIENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sedes`
--

CREATE TABLE `sedes` (
  `id` int(255) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `dane` bigint(20) NOT NULL,
  `consecutivo_sede` bigint(20) NOT NULL,
  `fk_centro_educativo` int(255) NOT NULL,
  `fk_lugar` int(255) NOT NULL,
  `fk_responsable` int(11) DEFAULT NULL COMMENT 'Persona responsable de la sede'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sedes`
--

INSERT INTO `sedes` (`id`, `nombre`, `dane`, `consecutivo_sede`, `fk_centro_educativo`, `fk_lugar`, `fk_responsable`) VALUES
(1, 'Alturas', 266088000480, 26608800019613, 1, 5, 32),
(2, 'Abejero', 266088000315, 26608800019612, 2, 5, 92),
(3, 'El progreso', 266088000285, 26608800019611, 2, 5, 92),
(4, 'Santa Emilia', 266088000196, 26608800019601, 2, 5, 92),
(5, 'La tribuna', 266088000099, 26608800019609, 2, 5, 92),
(6, 'Vista Hermosa', 266088000668, 26608800019610, 2, 5, 92),
(7, 'Selva Baja', 266088000331, 26608800019608, 2, 5, 92),
(8, 'Selva Alta', 266088000706, 26608800019607, 2, 5, 92),
(9, 'Guayabal', 266088000129, 26608800019606, 2, 5, 92),
(10, 'El Tigre', 266088000366, 26608800019605, 2, 5, 92),
(11, 'Frisolera Baja', 266088000251, 26608800019604, 2, 5, 92),
(12, 'Frisolera Alta', 266088000633, 26608800019603, 2, 5, 92);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

CREATE TABLE `telefonos` (
  `id` int(255) NOT NULL,
  `cod_area` smallint(6) NOT NULL,
  `numero_telf` int(11) NOT NULL,
  `fk_persona` int(255) DEFAULT NULL,
  `fk_sede` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`id`, `cod_area`, `numero_telf`, `fk_persona`, `fk_sede`) VALUES
(4, 235, 1234568, 92, NULL),
(7, 123, 3216549, 98, NULL),
(9, 123, 1231231231, 77, NULL),
(10, 123, 1231231231, 32, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(255) NOT NULL,
  `nombre_usuario` varchar(64) NOT NULL,
  `clave_usuario` varchar(128) NOT NULL,
  `politicas` tinyint(1) NOT NULL,
  `fecha_registro` datetime NOT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `fecha_politicas` datetime DEFAULT NULL,
  `descripcion_modif` text DEFAULT NULL,
  `estado` varchar(10) DEFAULT NULL,
  `fk_correo` int(255) NOT NULL,
  `fk_rol` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre_usuario`, `clave_usuario`, `politicas`, `fecha_registro`, `fecha_modificacion`, `fecha_politicas`, `descripcion_modif`, `estado`, `fk_correo`, `fk_rol`) VALUES
(1, 'deeply', '$2y$10$CLjk4f6iBf4GYg1a7Qm3z.JNfe2mwgWjuW4W3AGyhJ.c3hniRrvsG', 1, '2021-03-11 17:03:59', '2021-03-30 15:10:28', NULL, 'CAMBIO DE CONTRASEÑA', 'Activo', 1, 1),
(2, 'deeply_docadm', '$2y$10$CLjk4f6iBf4GYg1a7Qm3z.JNfe2mwgWjuW4W3AGyhJ.c3hniRrvsG', 1, '2021-03-11 17:03:59', '2021-03-24 16:01:50', NULL, 'definiendo responsable', 'ACTIVO', 26, 2),
(3, 'deeply_doc', '$2y$10$CLjk4f6iBf4GYg1a7Qm3z.JNfe2mwgWjuW4W3AGyhJ.c3hniRrvsG', 1, '2021-03-11 17:03:59', '2021-03-30 15:55:10', NULL, 'CAMBIO DE CONTRASEÑA', 'ACTIVO', 27, 3),
(4, 'deeply_acu', '$2y$10$CLjk4f6iBf4GYg1a7Qm3z.JNfe2mwgWjuW4W3AGyhJ.c3hniRrvsG', 1, '2021-03-11 17:03:59', '2021-03-30 15:54:52', NULL, NULL, 'ACTIVO', 25, 4),
(12, 'deeply_2', '$2y$10$CLjk4f6iBf4GYg1a7Qm3z.JNfe2mwgWjuW4W3AGyhJ.c3hniRrvsG', 1, '2021-03-18 19:04:03', '2021-03-24 14:55:54', NULL, 'cambiando estado', 'INACTIVO', 35, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grado_areas` (`fk_grado`,`fk_periodo_academico`),
  ADD KEY `fk_periodo_academico` (`fk_periodo_academico`);

--
-- Indices de la tabla `centros_educativos`
--
ALTER TABLE `centros_educativos`
  ADD PRIMARY KEY (`identificacion`),
  ADD UNIQUE KEY `identificacion` (`identificacion`),
  ADD KEY `fk_direccion` (`direccion`);

--
-- Indices de la tabla `certificados`
--
ALTER TABLE `certificados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_certiicados` (`fk_usuario`),
  ADD KEY `fk_persona_certiicados` (`fk_persona`);

--
-- Indices de la tabla `correos_electronicos`
--
ALTER TABLE `correos_electronicos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `direccion_correo` (`direccion_correo`),
  ADD KEY `fk_persona_correos_electronicos` (`fk_persona`);

--
-- Indices de la tabla `estudiantes_areas`
--
ALTER TABLE `estudiantes_areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_estudiante` (`fk_estudiante`),
  ADD KEY `fk_area` (`fk_areas`);

--
-- Indices de la tabla `estudiantes_grados`
--
ALTER TABLE `estudiantes_grados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_estudiante` (`fk_estudiante`),
  ADD KEY `fk_grado` (`fk_grado`);

--
-- Indices de la tabla `familiares`
--
ALTER TABLE `familiares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_estudiante` (`fk_estudiante`),
  ADD KEY `fk_parentesco` (`fk_parentesco`);

--
-- Indices de la tabla `foros`
--
ALTER TABLE `foros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_comenta_foros` (`fk_usuario_comenta`),
  ADD KEY `fk_usuario_notifica_foros` (`fk_usuario_notifica`),
  ADD KEY `fk_estudiante_foros` (`fk_estudiante`),
  ADD KEY `fk_cancela_matricula_foros` (`fk_cancela_matricula`);

--
-- Indices de la tabla `grados`
--
ALTER TABLE `grados`
  ADD PRIMARY KEY (`id`,`fk_periodo_academico`),
  ADD KEY `fk_periodo_academico_grados` (`fk_periodo_academico`),
  ADD KEY `fk_persona` (`fk_persona`);

--
-- Indices de la tabla `informaciones_adicionales`
--
ALTER TABLE `informaciones_adicionales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_matricula_estudiantes_informaciones_adicionales` (`fk_matricula`,`fk_estudiante`),
  ADD KEY `fk_grado` (`fk_grado`),
  ADD KEY `informaciones_adicionales_ibfk_2` (`fk_estudiante`);

--
-- Indices de la tabla `lugares`
--
ALTER TABLE `lugares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lugar_lugar` (`fk_lugar`);

--
-- Indices de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD PRIMARY KEY (`id`,`fk_estudiante`),
  ADD KEY `fk_estudiante_matriculas` (`fk_estudiante`),
  ADD KEY `id` (`id`,`img_cedula_mama`);

--
-- Indices de la tabla `matriculas_canceladas`
--
ALTER TABLE `matriculas_canceladas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_estudiante_matricula_matriculas_canceladas` (`fk_matricula`,`fk_estudiante`);

--
-- Indices de la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni` (`dni`),
  ADD KEY `fk_lugar_personas` (`direccion`),
  ADD KEY `fk_sede_personas` (`fk_sede`);

--
-- Indices de la tabla `reset_pass`
--
ALTER TABLE `reset_pass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_reset_pass` (`fk_usuario`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sedes`
--
ALTER TABLE `sedes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_centro_educativo_sedes` (`fk_centro_educativo`),
  ADD KEY `fk_lugar_sedes` (`fk_lugar`),
  ADD KEY `fk_responsable` (`fk_responsable`);

--
-- Indices de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_persona_telefonos` (`fk_persona`),
  ADD KEY `fk_sede_telefonos` (`fk_sede`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_usuario` (`nombre_usuario`),
  ADD KEY `fk_correo_usuarios` (`fk_correo`),
  ADD KEY `fk_rol_usuarios` (`fk_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `certificados`
--
ALTER TABLE `certificados`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `correos_electronicos`
--
ALTER TABLE `correos_electronicos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `estudiantes_areas`
--
ALTER TABLE `estudiantes_areas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `estudiantes_grados`
--
ALTER TABLE `estudiantes_grados`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `familiares`
--
ALTER TABLE `familiares`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `foros`
--
ALTER TABLE `foros`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `grados`
--
ALTER TABLE `grados`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `informaciones_adicionales`
--
ALTER TABLE `informaciones_adicionales`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `lugares`
--
ALTER TABLE `lugares`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `matriculas_canceladas`
--
ALTER TABLE `matriculas_canceladas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT de la tabla `reset_pass`
--
ALTER TABLE `reset_pass`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sedes`
--
ALTER TABLE `sedes`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`fk_grado`) REFERENCES `grados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `areas_ibfk_2` FOREIGN KEY (`fk_periodo_academico`) REFERENCES `periodos_academicos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `certificados`
--
ALTER TABLE `certificados`
  ADD CONSTRAINT `fk_persona_certiicados` FOREIGN KEY (`fk_persona`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_usuario_certiicados` FOREIGN KEY (`fk_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `correos_electronicos`
--
ALTER TABLE `correos_electronicos`
  ADD CONSTRAINT `fk_persona_correos_electronicos` FOREIGN KEY (`fk_persona`) REFERENCES `personas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `estudiantes_areas`
--
ALTER TABLE `estudiantes_areas`
  ADD CONSTRAINT `estudiantes_areas_ibfk_1` FOREIGN KEY (`fk_areas`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `estudiantes_areas_ibfk_2` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `estudiantes_grados`
--
ALTER TABLE `estudiantes_grados`
  ADD CONSTRAINT `estudiantes_grados_ibfk_1` FOREIGN KEY (`fk_grado`) REFERENCES `grados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `estudiantes_grados_ibfk_2` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `familiares`
--
ALTER TABLE `familiares`
  ADD CONSTRAINT `familiares_ibfk_1` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `familiares_ibfk_2` FOREIGN KEY (`fk_parentesco`) REFERENCES `personas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `foros`
--
ALTER TABLE `foros`
  ADD CONSTRAINT `fk_cancela_matricula_foros` FOREIGN KEY (`fk_cancela_matricula`) REFERENCES `matriculas_canceladas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_estudiante_foros` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_usuario_comenta_foros` FOREIGN KEY (`fk_usuario_comenta`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_usuario_notifica_foros` FOREIGN KEY (`fk_usuario_notifica`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `grados`
--
ALTER TABLE `grados`
  ADD CONSTRAINT `fk_periodo_academico_grados` FOREIGN KEY (`fk_periodo_academico`) REFERENCES `periodos_academicos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `grados_ibfk_1` FOREIGN KEY (`fk_persona`) REFERENCES `personas` (`id`);

--
-- Filtros para la tabla `informaciones_adicionales`
--
ALTER TABLE `informaciones_adicionales`
  ADD CONSTRAINT `informaciones_adicionales_ibfk_1` FOREIGN KEY (`fk_grado`) REFERENCES `grados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `informaciones_adicionales_ibfk_2` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `informaciones_adicionales_ibfk_3` FOREIGN KEY (`fk_matricula`) REFERENCES `matriculas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `lugares`
--
ALTER TABLE `lugares`
  ADD CONSTRAINT `fk_lugar_lugar` FOREIGN KEY (`fk_lugar`) REFERENCES `lugares` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD CONSTRAINT `fk_estudiante_matriculas` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`);

--
-- Filtros para la tabla `matriculas_canceladas`
--
ALTER TABLE `matriculas_canceladas`
  ADD CONSTRAINT `fk_estudiante_matricula_matriculas_canceladas` FOREIGN KEY (`fk_matricula`,`fk_estudiante`) REFERENCES `matriculas` (`id`, `fk_estudiante`) ON DELETE CASCADE;

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `personas_ibfk_2` FOREIGN KEY (`fk_sede`) REFERENCES `sedes` (`id`);

--
-- Filtros para la tabla `reset_pass`
--
ALTER TABLE `reset_pass`
  ADD CONSTRAINT `fk_usuario_reset_pass` FOREIGN KEY (`fk_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `sedes`
--
ALTER TABLE `sedes`
  ADD CONSTRAINT `fk_centro_educativo_sedes` FOREIGN KEY (`fk_centro_educativo`) REFERENCES `centros_educativos` (`identificacion`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lugar_sedes` FOREIGN KEY (`fk_lugar`) REFERENCES `lugares` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sedes_ibfk_1` FOREIGN KEY (`fk_responsable`) REFERENCES `personas` (`id`);

--
-- Filtros para la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD CONSTRAINT `fk_persona_telefonos` FOREIGN KEY (`fk_persona`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_sede_telefonos` FOREIGN KEY (`fk_sede`) REFERENCES `sedes` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_correo_usuarios` FOREIGN KEY (`fk_correo`) REFERENCES `correos_electronicos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_rol_usuarios` FOREIGN KEY (`fk_rol`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
