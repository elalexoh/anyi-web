-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 06-04-2021 a las 14:40:08
-- Versión del servidor: 10.2.3-MariaDB-log
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `angie_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id` int(255) NOT NULL,
  `nombre` varchar(56) NOT NULL,
  `intensidad` tinyint(4) NOT NULL,
  `fk_periodo_academico` int(255) NOT NULL,
  `fk_grado` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`id`, `nombre`, `intensidad`, `fk_periodo_academico`, `fk_grado`) VALUES
(22, 'fisica', 3, 1, 29),
(23, 'español', 2, 1, 30),
(24, 'español', 5, 1, 33),
(25, 'ingles', 5, 1, 33),
(26, 'edu.fisica', 5, 1, 34),
(27, 'sociales', 3, 1, 30),
(28, 'español', 3, 1, 31),
(29, 'jhk', 3, 1, 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centros_educativos`
--

CREATE TABLE `centros_educativos` (
  `identificacion` int(255) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `resolucion` varchar(128) NOT NULL,
  `dane` bigint(20) NOT NULL,
  `nit` varchar(128) NOT NULL,
  `consecutivo_sede` bigint(20) NOT NULL,
  `nombre_autorizado` varchar(128) NOT NULL,
  `cargo` varchar(128) NOT NULL,
  `img_logo` varchar(256) NOT NULL,
  `img_firma` varchar(256) NOT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `dni_director` varchar(15) DEFAULT NULL,
  `lugar_expedicion` varchar(56) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `centros_educativos`
--

INSERT INTO `centros_educativos` (`identificacion`, `nombre`, `resolucion`, `dane`, `nit`, `consecutivo_sede`, `nombre_autorizado`, `cargo`, `img_logo`, `img_firma`, `direccion`, `dni_director`, `lugar_expedicion`) VALUES
(1, 'Santa Emilia', 'de integración 2412 de 08 de noviembre de 2002', 266088000196, '900067553-4', 266088000196, 'prueba', 'DIRECTORA RURAL', 'http://127.0.0.1:8000/storage/centros/logos/bjX5imvMLHawAiZZnI8yIddXM7faEMUyP5WI1oAy.jpg', 'http://127.0.0.1:8000/storage/centros/firmas/XIpO52DEcQ3ORsPxsLzayvAS7SWmRoeQUcIS6SuY.png', 'Municipio de Belén, Risaralda', '526845', 'prueba (prueba)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificados`
--

CREATE TABLE `certificados` (
  `id` int(255) NOT NULL,
  `tipo` varchar(56) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` varchar(32) DEFAULT NULL,
  `url_certificado` varchar(256) DEFAULT NULL,
  `fk_usuario` int(255) NOT NULL COMMENT 'Docente',
  `fk_persona` int(255) NOT NULL COMMENT 'Estudiante',
  `motivo` varchar(255) DEFAULT NULL,
  `firmado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `certificados`
--

INSERT INTO `certificados` (`id`, `tipo`, `descripcion`, `estatus`, `url_certificado`, `fk_usuario`, `fk_persona`, `motivo`, `firmado`) VALUES
(20, 'notas_full', 'Solicitud de notas completas', 'ENVIADO', 'http://127.0.0.1:8000/storage/usuarios/documentos//agvozbvmAEeTqTS4SObT7Gg89rpq5CYg3qLnJXoH.pdf', 24, 120, NULL, 0),
(21, 'estudio', 'jhkj', 'APROBADO', NULL, 24, 120, NULL, 1),
(22, 'notas_full', 'Solicitud de notas completas', 'ENVIADO', 'http://127.0.0.1:8000/storage/usuarios/documentos//lM0KLZtLbuByyi4KbfeFtTDdk2BKCmg7q9QumcSZ.pdf', 24, 130, NULL, 0),
(23, 'paz y salvo', 'lo necesito', 'RECHAZADO', NULL, 33, 130, 'no', 0),
(24, 'estudio', 'LKL', 'APROBADO', NULL, 24, 125, 'no quiero', 1),
(25, 'notas_full', 'Solicitud de notas completas', 'ENVIADO', 'http://127.0.0.1:8000/storage/usuarios/documentos//Hkem1eDMZzE9tEbBCBzawo2X7aFVplWpTLdk0JM2.pdf', 37, 137, NULL, 0),
(26, 'paz y salvo', 'jhkj', 'PENDIENTE', NULL, 26, 121, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos_electronicos`
--

CREATE TABLE `correos_electronicos` (
  `id` int(255) NOT NULL,
  `direccion_correo` varchar(128) NOT NULL,
  `fk_persona` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `correos_electronicos`
--

INSERT INTO `correos_electronicos` (`id`, `direccion_correo`, `fk_persona`) VALUES
(1, 'diana.molina@ucp.edu.co', 78),
(35, 'RESPONSABLE_sede_2@gmail.com', 92),
(47, 'tttt@gmail.com', 116),
(48, 'li@gmail.com', 117),
(49, 'ramirezanyi.216@gmail.com', 118),
(51, 'ss@gmail.com', 122),
(52, 'ab@gmai.com', 123),
(56, '455@gmail.comr', 131),
(57, '2356@gmail.com', 132),
(58, 'bb@gmai.lcom', 133),
(60, 'rjhj@gmail.com', 135),
(61, 'frsi@gmail.com', 136);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiantes_areas`
--

CREATE TABLE `estudiantes_areas` (
  `id` int(255) NOT NULL,
  `nota_parcial` decimal(3,1) DEFAULT NULL,
  `fk_estudiante` int(255) NOT NULL,
  `fk_areas` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiantes_areas`
--

INSERT INTO `estudiantes_areas` (`id`, `nota_parcial`, `fk_estudiante`, `fk_areas`) VALUES
(5, '3.5', 120, 22),
(6, '3.5', 121, 22),
(7, '3.2', 130, 23),
(8, '3.5', 137, 24),
(9, '1.0', 137, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiantes_grados`
--

CREATE TABLE `estudiantes_grados` (
  `id` int(255) NOT NULL,
  `fk_estudiante` int(11) NOT NULL,
  `fk_grado` int(11) NOT NULL,
  `estatus` varchar(25) NOT NULL DEFAULT 'cursando'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiantes_grados`
--

INSERT INTO `estudiantes_grados` (`id`, `fk_estudiante`, `fk_grado`, `estatus`) VALUES
(24, 120, 29, 'cursando'),
(25, 121, 29, 'cursando'),
(26, 125, 31, 'cursando'),
(27, 130, 30, 'cursando'),
(28, 137, 33, 'cursando'),
(29, 139, 30, 'cursando');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familiares`
--

CREATE TABLE `familiares` (
  `id` int(255) NOT NULL,
  `fk_estudiante` int(11) NOT NULL,
  `fk_parentesco` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `familiares`
--

INSERT INTO `familiares` (`id`, `fk_estudiante`, `fk_parentesco`) VALUES
(15, 120, 118),
(16, 121, 118),
(17, 125, 118),
(18, 130, 131),
(19, 137, 136),
(20, 139, 136);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros`
--

CREATE TABLE `foros` (
  `id` int(255) NOT NULL,
  `tipo` varchar(56) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `estatus` varchar(32) DEFAULT NULL,
  `fk_usuario_comenta` int(255) NOT NULL,
  `fk_usuario_notifica` int(255) DEFAULT NULL,
  `fk_estudiante` int(255) DEFAULT NULL,
  `fk_cancela_matricula` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `foros`
--

INSERT INTO `foros` (`id`, `tipo`, `descripcion`, `fecha_creacion`, `estatus`, `fk_usuario_comenta`, `fk_usuario_notifica`, `fk_estudiante`, `fk_cancela_matricula`) VALUES
(7, 'FORO', 'hola', '2021-03-31 23:41:46', 'VISTO', 24, 33, NULL, NULL),
(8, 'FORO', 'hola', '2021-04-04 21:46:58', 'VISTO', 37, 24, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grados`
--

CREATE TABLE `grados` (
  `id` int(255) NOT NULL,
  `nombre` varchar(24) NOT NULL,
  `fk_periodo_academico` int(255) NOT NULL,
  `fk_persona` int(255) NOT NULL COMMENT 'esto corresponde al docente'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grados`
--

INSERT INTO `grados` (`id`, `nombre`, `fk_periodo_academico`, `fk_persona`) VALUES
(29, 'preescolar', 1, 116),
(30, 'segundo', 1, 116),
(31, 'primero', 1, 117),
(32, 'preescolar', 1, 117),
(33, 'primero', 1, 135),
(34, 'preescolar', 1, 135);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informaciones_adicionales`
--

CREATE TABLE `informaciones_adicionales` (
  `id` int(255) NOT NULL,
  `tipo` varchar(64) NOT NULL,
  `enfermedades_sufridas` text DEFAULT NULL,
  `sisben` text DEFAULT NULL,
  `estrato_socioeconimico` varchar(64) DEFAULT NULL,
  `nro_hermanos` tinyint(4) DEFAULT NULL,
  `posicion_hermanos` varchar(24) DEFAULT NULL,
  `procedencia` varchar(156) DEFAULT NULL,
  `nuevo` tinyint(1) DEFAULT NULL,
  `repitiente` tinyint(1) DEFAULT NULL,
  `fk_matricula` int(255) NOT NULL,
  `fk_estudiante` int(255) NOT NULL,
  `p_nombre_mama` varchar(255) DEFAULT NULL,
  `s_nombre_mama` varchar(255) DEFAULT NULL,
  `p_apellido_mama` varchar(255) DEFAULT NULL,
  `s_apellido_mama` varchar(255) DEFAULT NULL,
  `dni_madre` int(10) DEFAULT NULL,
  `p_nombre_padre` varchar(255) DEFAULT NULL,
  `s_nombre_padre` varchar(255) DEFAULT NULL,
  `p_apellido_padre` varchar(255) DEFAULT NULL,
  `s_apellido_padre` varchar(255) DEFAULT NULL,
  `lugar_expedicion` varchar(255) NOT NULL,
  `lugar_nacimiento` varchar(255) NOT NULL,
  `rh` varchar(20) NOT NULL,
  `dni_padre` int(10) DEFAULT NULL,
  `fk_grado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informaciones_adicionales`
--

INSERT INTO `informaciones_adicionales` (`id`, `tipo`, `enfermedades_sufridas`, `sisben`, `estrato_socioeconimico`, `nro_hermanos`, `posicion_hermanos`, `procedencia`, `nuevo`, `repitiente`, `fk_matricula`, `fk_estudiante`, `p_nombre_mama`, `s_nombre_mama`, `p_apellido_mama`, `s_apellido_mama`, `dni_madre`, `p_nombre_padre`, `s_nombre_padre`, `p_apellido_padre`, `s_apellido_padre`, `lugar_expedicion`, `lugar_nacimiento`, `rh`, `dni_padre`, `fk_grado`) VALUES
(12, 'registro_civil', 'nb', '23.4', 'hgj', 1, 'mayor', 'jhkj', 1, 1, 15, 120, 'hgjg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Santa Emilia', 'jhk', 'a+', NULL, 29),
(13, 'tarjeta_identidad', 'jkj', '1', 'jhj', 0, 'ninguno', 'hjk', 1, 1, 16, 121, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Santa Emilia', 'jhkh', 'a+', NULL, 29),
(14, 'tarjeta_identidad', 'jhjk', '23.4', 'uno', 0, 'ninguna', 'kjl', 1, 1, 17, 125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'El progreso', 'gjghj', 'a+', NULL, 31),
(15, 'registro_civil', 'ytghg', 'kjlk', '25', 0, 'ninguni', 'jhkj', 1, 0, 18, 130, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Santa Emilia', 'hjk', 'a+', NULL, 30),
(16, 'registro_civil', 'ninguna', 'D21', '23.5', 1, 'MAYOR', 'kjhkjh', 1, 0, 19, 137, 'hh', 'hgjhgj', NULL, NULL, NULL, 'jj', NULL, NULL, NULL, 'Frisolera Baja', 'nn', 'a+', NULL, 33),
(17, 'registro_civil', 'jhj', 'kjkl', '23.5', 1, 'mayor', 'kjlkjl', 1, 1, 20, 139, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Santa Emilia', 'jhk', 'b+', NULL, 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugares`
--

CREATE TABLE `lugares` (
  `id` int(255) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `tipo` varchar(32) NOT NULL,
  `fk_lugar` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lugares`
--

INSERT INTO `lugares` (`id`, `nombre`, `tipo`, `fk_lugar`) VALUES
(1, 'Colombia', 'Pais', NULL),
(3, 'Belén de Umbría, Risaralda', 'Municipio y Departamento', 1),
(4, 'Abejero', 'Vereda', 3),
(5, 'Alturas', 'Vereda', 3),
(6, 'El Progreso', 'Vereda', 3),
(7, 'Santa Emilia', 'Vereda', 3),
(8, 'La Tribuna', 'Vereda', 3),
(9, 'Vista Hermosa', 'Vereda', 3),
(10, 'Selva Baja', 'Vereda', 3),
(11, 'Selva Alta', 'Vereda', 3),
(12, 'Guayabal', 'Vereda', 3),
(13, 'El Tigre', 'Vereda', 3),
(14, 'Frisolera Baja', 'Vereda', 3),
(15, 'Frisolera Alta', 'Vereda', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculas`
--

CREATE TABLE `matriculas` (
  `id` int(255) NOT NULL,
  `fecha_matricula` date NOT NULL,
  `img_firma_acudiente` varchar(256) DEFAULT NULL,
  `img_firma_estudiante` varchar(256) DEFAULT NULL,
  `fk_estudiante` int(255) NOT NULL,
  `estatus` varchar(56) DEFAULT NULL,
  `fecha_renovacion` date DEFAULT NULL,
  `religion` tinyint(1) DEFAULT NULL,
  `img_cedula_papa` varchar(256) DEFAULT NULL,
  `img_cedula_mama` varchar(256) DEFAULT NULL,
  `img_firma_familiar` varchar(256) DEFAULT NULL,
  `img_cedula_acudiente` varchar(256) DEFAULT NULL,
  `img_cedula_estudiante` varchar(256) DEFAULT NULL,
  `notas` varchar(256) NOT NULL,
  `tipo_estudiante` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `matriculas`
--

INSERT INTO `matriculas` (`id`, `fecha_matricula`, `img_firma_acudiente`, `img_firma_estudiante`, `fk_estudiante`, `estatus`, `fecha_renovacion`, `religion`, `img_cedula_papa`, `img_cedula_mama`, `img_firma_familiar`, `img_cedula_acudiente`, `img_cedula_estudiante`, `notas`, `tipo_estudiante`) VALUES
(15, '2021-03-24', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 120, 'Aprobada', '2021-04-04', 1, 'http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/madres/NdtGr6bQY1TPttX7GFYVrFCqk7e12LR7JXxdb5un.jpg', 'http://127.0.0.1:8000/storage/matriculas/firmas/familiares/BdUZelKfKlTkHmSTAkNYtjlmEPdxxdhWciSEljxH.pdf', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/notas/pdiiiy7cehtOPN3Fb396wwl5zZ7WrjFnpIIEYmcx.pdf', NULL),
(16, '2021-03-11', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 121, 'Aprobada', NULL, 1, 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', NULL),
(17, '2021-03-11', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 125, 'CANCELANDO', '2021-03-31', 1, 'http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', NULL),
(18, '2021-03-12', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 130, 'CANCELADO', '2021-04-04', 1, 'http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage/matriculas/firmas/familiares/asBm3g1LxiLjww5ki2hTrQ3so4w9io4G1xTzZ9v6.jpg', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', NULL),
(19, '2021-04-04', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 137, 'Aprobada', '2021-04-04', 1, 'http://127.0.0.1:8000/storage/http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage/matriculas/identificaciones/madres/BT5p1M27HTx42LtdTFGjlXo2bYLcr51hskODbxDT.jpg', 'http://127.0.0.1:8000/storage/matriculas/firmas/familiares/aDKT6Humm3Uh6gqtYFFrnrMv4YBDoDt26R8SdU2k.jpg', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage/matriculas/notas/2hYr4Tn6twXMQraBOBDgGankfPOWZGw8tbZau3Bg.pdf', NULL),
(20, '2021-04-04', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 139, 'Pendiente', NULL, 1, 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage/matriculas/firmas/familiares/9JzcHCXq82jhXD1MLmQ99Hs1Zfz3hiCrq52PyKA1.jpg', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', 'http://127.0.0.1:8000/storage', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriculas_canceladas`
--

CREATE TABLE `matriculas_canceladas` (
  `id` int(255) NOT NULL,
  `motivo_cancelacion` text NOT NULL,
  `destino_estudiante` text NOT NULL,
  `fecha_cancelacion` date DEFAULT NULL,
  `fk_matricula` int(255) NOT NULL,
  `fk_estudiante` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `matriculas_canceladas`
--

INSERT INTO `matriculas_canceladas` (`id`, `motivo_cancelacion`, `destino_estudiante`, `fecha_cancelacion`, `fk_matricula`, `fk_estudiante`) VALUES
(2, 'jh', 'h', '2021-04-01', 16, 121),
(3, 'hgj', 'hjk', '2021-04-04', 18, 130),
(4, 'jhkj', 'jhjk', NULL, 17, 125);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_academicos`
--

CREATE TABLE `periodos_academicos` (
  `id` int(255) NOT NULL,
  `nombre` varchar(32) DEFAULT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `periodos_academicos`
--

INSERT INTO `periodos_academicos` (`id`, `nombre`, `fecha_inicio`, `fecha_fin`) VALUES
(1, 'Primero', '2020-12-25', '2021-06-08'),
(2, 'Segundo', '2020-12-25', '2020-12-25'),
(3, 'Tercero', '2021-10-12', '2021-02-26'),
(4, 'Cuarto', '2020-12-25', '2020-12-25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(255) NOT NULL,
  `p_nombre` varchar(64) NOT NULL,
  `s_nombre` varchar(64) DEFAULT NULL,
  `p_apellido` varchar(64) NOT NULL,
  `s_apellido` varchar(64) DEFAULT NULL,
  `tipo` varchar(56) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `img_foto` varchar(256) DEFAULT NULL,
  `dni` int(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `fk_sede` int(255) NOT NULL,
  `sexo` varchar(25) NOT NULL DEFAULT 'masculino'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `p_nombre`, `s_nombre`, `p_apellido`, `s_apellido`, `tipo`, `fecha_nacimiento`, `img_foto`, `dni`, `direccion`, `fk_sede`, `sexo`) VALUES
(78, 'Administrador', NULL, 'Administrador', NULL, 'Administrador', '2021-03-02', NULL, 2023, 'prueba', 4, 'femenino'),
(92, 'RESPONSABLE', 'Responsable', 'RESPONSABLE', NULL, 'DOCENTE ADMINISTRATIVO', '2016-12-09', 'http://127.0.0.1:8000/storage/usuarios/fotos//2Oj1xIh4BKf07fEvfcl3xgt1DQ33R5GAmF3HtLHZ.png', 25252525, 'RESPONSABLE', 2, 'masculino'),
(116, 'diana', NULL, 'molina', NULL, 'DOCENTE ADMINISTRATIVO', '2016-12-24', NULL, 16584, 'belen', 4, 'masculino'),
(117, 'juliana', NULL, 'castañeda', NULL, 'DOCENTE', '2016-12-16', 'http://127.0.0.1:8000/storage/usuarios/fotos//n5A6Qv1bLDdL124LziU8xvbAMup3oU7Z9WGB80xU.jpg', 45862, 'hjgj', 3, 'masculino'),
(118, 'acudiente', 'acudiente', 'acudiente', 'null', 'ACUDIENTE', '2021-03-03', NULL, 256842, 'santa emilia', 4, 'masculino'),
(120, 'estudiante', NULL, 'estudiante', NULL, 'estudiante', '2021-03-24', NULL, 12354, 'jhgj', 4, 'masculino'),
(121, 'sofia', NULL, 'sofia', NULL, 'estudiante', '2021-03-24', NULL, 54654, 'jhgj', 4, 'masculino'),
(122, 'acudientepu', NULL, 'acudiente', NULL, 'ACUDIENTE', '2021-03-03', NULL, 12358, 'jhhj', 3, 'femenino'),
(123, 'gh', NULL, 'jgj', NULL, 'ACUDIENTE', '2021-03-02', NULL, 685421, 'jkhkj', 2, 'femenino'),
(125, 'prueba', NULL, 'prueba', NULL, 'estudiante', '2021-03-02', NULL, 45523, 'hjkjh', 3, 'masculino'),
(130, 'jhkj', NULL, 'jhhkjh', NULL, 'estudiante', '2021-03-23', NULL, 25698, 'jhgjh', 4, 'masculino'),
(131, 'gh', 'null', 'jkj', 'null', 'acudiente', '2021-03-10', NULL, 563, 'jhk', 4, 'masculino'),
(132, 'res', NULL, 'res', NULL, 'DOCENTE', '2013-01-07', NULL, 25684, 'hjjk', 4, 'masculino'),
(133, 'kjl', NULL, 'hgj', NULL, 'ACUDIENTE', '2021-04-02', NULL, 333333, 'hghj', 2, 'masculino'),
(135, 'frisolera', NULL, 'frisolera', 'frisolera', 'DOCENTE', '2013-06-11', 'http://127.0.0.1:8000/storage/usuarios/fotos//jOUCG1hKMvZkaH9fNnK1hGNRcVCImU0bmBMtfvBc.jpg', 2365894, 'belen', 11, 'masculino'),
(136, 'frisolera', 'null', 'sotp', 'null', 'ACUDIENTE', '2010-02-04', 'http://127.0.0.1:8000/storage/usuarios/fotos//6qkAJIkcxmd3mfM0omtQnzt7CldV5956A3hHonbZ.jpg', 123548, 'bbh', 11, 'masculino'),
(137, 'sara', NULL, 'gomez', NULL, 'estudiante', '2021-04-05', NULL, 123568, 'hgjgj #3-12', 11, 'masculino'),
(139, 'hkj', NULL, 'gg', NULL, 'estudiante', '2021-05-04', NULL, 5698742, 'hgj', 4, 'masculino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reset_pass`
--

CREATE TABLE `reset_pass` (
  `id` int(255) NOT NULL,
  `token_recuperacion` varchar(128) NOT NULL,
  `fecha_solicitud` datetime NOT NULL,
  `fk_usuario` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reset_pass`
--

INSERT INTO `reset_pass` (`id`, `token_recuperacion`, `fecha_solicitud`, `fk_usuario`) VALUES
(1, '352fd6f6aa5db413c81d7644566eea8ddfe12b4f9b2d6f4905f2c92708fa0da50f35e70decb6e51995246d139dc161b6f770cf7fefb07f9f443d3e12f1bf3bf7', '2021-03-17 21:06:03', 1),
(2, '13cc10a7848e77828c5a83cdb8f277adb72717c4ca4ce4ab0b1e45ad66324c62c7c9086fc65e435753a81e4379ecf01a753d69d080dd9d6bcde5a000f84fbeb8', '2021-03-17 21:07:21', 1),
(4, 'b2c6b9c0db8c6b7bfa86df8a42c31320274079673c6e089a26ea41cf4e40ccc57c085af445370ae2633b0730c4cff678e228e00838958077d838f1fa4a6962a0', '2021-03-24 15:01:35', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(255) NOT NULL,
  `nombre` varchar(56) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'ADMINISTRADOR'),
(2, 'DOCENTE ADMINISTRATIVO'),
(3, 'DOCENTE'),
(4, 'ACUDIENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sedes`
--

CREATE TABLE `sedes` (
  `id` int(255) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `dane` bigint(20) NOT NULL,
  `consecutivo_sede` bigint(20) NOT NULL,
  `fk_centro_educativo` int(255) NOT NULL,
  `fk_lugar` int(255) NOT NULL,
  `fk_responsable` int(11) DEFAULT NULL COMMENT 'Persona responsable de la sede'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sedes`
--

INSERT INTO `sedes` (`id`, `nombre`, `dane`, `consecutivo_sede`, `fk_centro_educativo`, `fk_lugar`, `fk_responsable`) VALUES
(1, 'Alturas', 266088000480, 26608800019613, 1, 5, 92),
(2, 'Abejero', 266088000315, 26608800019612, 1, 4, 92),
(3, 'El progreso', 266088000285, 26608800019611, 1, 6, 117),
(4, 'Santa Emilia', 266088000196, 26608800019601, 1, 7, 132),
(5, 'La tribuna', 266088000099, 26608800019609, 1, 8, 92),
(6, 'Vista Hermosa', 266088000668, 26608800019610, 1, 9, 92),
(7, 'Selva Baja', 266088000331, 26608800019608, 1, 10, 92),
(8, 'Selva Alta', 266088000706, 26608800019607, 1, 11, 92),
(9, 'Guayabal', 266088000129, 26608800019606, 1, 12, 92),
(10, 'El Tigre', 266088000366, 26608800019605, 1, 13, 92),
(11, 'Frisolera Baja', 266088000251, 26608800019604, 1, 14, 135),
(12, 'Frisolera Alta', 266088000633, 26608800019603, 1, 15, 92);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

CREATE TABLE `telefonos` (
  `id` int(255) NOT NULL,
  `cod_area` smallint(6) NOT NULL,
  `numero_telf` int(11) NOT NULL,
  `fk_persona` int(255) DEFAULT NULL,
  `fk_sede` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`id`, `cod_area`, `numero_telf`, `fk_persona`, `fk_sede`) VALUES
(4, 235, 1234568, 92, NULL),
(19, 57, 123584, 116, NULL),
(20, 57, 5861, 117, NULL),
(21, 57, 3251, 118, NULL),
(23, 57, 335, 122, NULL),
(24, 57, 3256, 123, NULL),
(26, 57, 54, 132, NULL),
(27, 57, 54654, 133, NULL),
(29, 57, 36584212, 135, NULL),
(30, 57, 526842, 136, NULL),
(32, 57, 563218, 131, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(255) NOT NULL,
  `nombre_usuario` varchar(64) NOT NULL,
  `clave_usuario` varchar(128) NOT NULL,
  `politicas` tinyint(1) NOT NULL,
  `fecha_registro` datetime NOT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `fecha_politicas` datetime DEFAULT NULL,
  `descripcion_modif` text DEFAULT NULL,
  `estado` varchar(10) DEFAULT NULL,
  `fk_correo` int(255) NOT NULL,
  `fk_rol` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre_usuario`, `clave_usuario`, `politicas`, `fecha_registro`, `fecha_modificacion`, `fecha_politicas`, `descripcion_modif`, `estado`, `fk_correo`, `fk_rol`) VALUES
(1, 'admin', '$2y$10$Kp8zS18nPfhT.shU01PBjOiCFVEQeFGYhey6OPX.Zc2DStLj6J2lC', 1, '2021-03-11 17:03:59', '2021-04-04 21:23:38', NULL, 'CAMBIO DE CONTRASEÑA', 'Activo', 1, 1),
(24, 'diana', '$2y$10$1LYvJB8H8cu6Mg.xkVbUy.gU.nc3.nsZ699LcECR2QJL613.9GtG.', 1, '2021-03-31 22:59:23', '2021-04-04 21:50:24', NULL, 'CAMBIO DE CONTRASEÑA', 'ACTIVO', 47, 2),
(25, 'juliana', '$2y$10$8RGmrjLr3U3ftch18wbSautKf.ZJlsFFIi/GLqc37h.LTdNut8oqi', 1, '2021-03-31 23:00:54', '2021-04-04 22:09:45', NULL, 'CAMBIO DE CONTRASEÑA', 'ACTIVO', 48, 3),
(26, 'acudiente', '$2y$10$1HZFQSkw8yQ3nkT.U9WvbuMwoPeARImTC0QIk.wJ6Y1UT2JwVwV3m', 1, '2021-03-31 23:06:51', '2021-04-04 22:15:55', NULL, 'CAMBIO DE CONTRASEÑA', 'ACTIVO', 49, 4),
(28, 'acudiente2', '$2y$10$whas0GaeuElp7HGzN64ImOdQ564s.ekMj.Ht3zMCW9RPAL2/gENXy', 1, '2021-03-31 23:25:17', NULL, NULL, NULL, 'ACTIVO', 51, 4),
(29, 'prueba', '$2y$10$ZLo9tbqiUU0O/RJgAEgMbOgJl6NZcTDIJ0ZizOxdThDqsvG1VWaV6', 0, '2021-03-31 23:26:12', NULL, NULL, NULL, 'INACTIVO', 52, 4),
(33, 'prueba7', '$2y$10$yVQUK1ORVKKQVxJpxFpyHeHG5Ns3G1ZCRnsUboR7IiqhwoTBx1dH2', 1, '2021-03-31 23:40:46', '2021-04-04 21:55:55', NULL, NULL, 'INACTIVO', 56, 4),
(34, 'red', '$2y$10$V5i/3n7fI1pWKEOt8q56T.a017afO6LthW.sqFA0dao6YxQxSq4Ka', 1, '2021-04-02 15:45:53', '2021-04-02 15:46:16', NULL, 'hjk', 'ACTIVO', 57, 3),
(35, 'hh', '$2y$10$YZTvE1xUKx7P/3EwjkDY1uGJKP.ou7Ov1SOzxCLSfYM6hnQsQgvoq', 0, '2021-04-02 16:04:25', NULL, NULL, NULL, 'ACTIVO', 58, 4),
(37, 'frisolera', '$2y$10$dIM/1ZwxpCg7wjlMsOwYOe4oyIM6I0f3Aovbo2o.yKmx3ODgAg.sW', 1, '2021-04-04 21:29:49', '2021-04-04 21:30:42', NULL, 'falto segundo nombre', 'ACTIVO', 60, 3),
(38, 'frisolera1', '$2y$10$gXP9xZWEwnl6AiCOj4W3BOcRjE2DFqmDyxTQrbMgJVJfO1SFTI5oy', 0, '2021-04-04 21:35:24', '2021-04-04 21:35:40', NULL, NULL, 'ACTIVO', 61, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grado_areas` (`fk_grado`,`fk_periodo_academico`),
  ADD KEY `fk_periodo_academico` (`fk_periodo_academico`);

--
-- Indices de la tabla `centros_educativos`
--
ALTER TABLE `centros_educativos`
  ADD PRIMARY KEY (`identificacion`),
  ADD UNIQUE KEY `identificacion` (`identificacion`),
  ADD KEY `fk_direccion` (`direccion`);

--
-- Indices de la tabla `certificados`
--
ALTER TABLE `certificados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_certiicados` (`fk_usuario`),
  ADD KEY `fk_persona_certiicados` (`fk_persona`);

--
-- Indices de la tabla `correos_electronicos`
--
ALTER TABLE `correos_electronicos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `direccion_correo` (`direccion_correo`),
  ADD KEY `fk_persona_correos_electronicos` (`fk_persona`);

--
-- Indices de la tabla `estudiantes_areas`
--
ALTER TABLE `estudiantes_areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_estudiante` (`fk_estudiante`),
  ADD KEY `fk_area` (`fk_areas`);

--
-- Indices de la tabla `estudiantes_grados`
--
ALTER TABLE `estudiantes_grados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_estudiante` (`fk_estudiante`),
  ADD KEY `fk_grado` (`fk_grado`);

--
-- Indices de la tabla `familiares`
--
ALTER TABLE `familiares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_estudiante` (`fk_estudiante`),
  ADD KEY `fk_parentesco` (`fk_parentesco`);

--
-- Indices de la tabla `foros`
--
ALTER TABLE `foros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_comenta_foros` (`fk_usuario_comenta`),
  ADD KEY `fk_usuario_notifica_foros` (`fk_usuario_notifica`),
  ADD KEY `fk_estudiante_foros` (`fk_estudiante`),
  ADD KEY `fk_cancela_matricula_foros` (`fk_cancela_matricula`);

--
-- Indices de la tabla `grados`
--
ALTER TABLE `grados`
  ADD PRIMARY KEY (`id`,`fk_periodo_academico`),
  ADD KEY `fk_periodo_academico_grados` (`fk_periodo_academico`),
  ADD KEY `fk_persona` (`fk_persona`);

--
-- Indices de la tabla `informaciones_adicionales`
--
ALTER TABLE `informaciones_adicionales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_matricula_estudiantes_informaciones_adicionales` (`fk_matricula`,`fk_estudiante`),
  ADD KEY `fk_grado` (`fk_grado`),
  ADD KEY `informaciones_adicionales_ibfk_2` (`fk_estudiante`);

--
-- Indices de la tabla `lugares`
--
ALTER TABLE `lugares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lugar_lugar` (`fk_lugar`);

--
-- Indices de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD PRIMARY KEY (`id`,`fk_estudiante`),
  ADD KEY `fk_estudiante_matriculas` (`fk_estudiante`),
  ADD KEY `id` (`id`,`img_cedula_mama`);

--
-- Indices de la tabla `matriculas_canceladas`
--
ALTER TABLE `matriculas_canceladas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_estudiante_matricula_matriculas_canceladas` (`fk_matricula`,`fk_estudiante`);

--
-- Indices de la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni` (`dni`),
  ADD KEY `fk_lugar_personas` (`direccion`),
  ADD KEY `fk_sede_personas` (`fk_sede`);

--
-- Indices de la tabla `reset_pass`
--
ALTER TABLE `reset_pass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_reset_pass` (`fk_usuario`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sedes`
--
ALTER TABLE `sedes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_centro_educativo_sedes` (`fk_centro_educativo`),
  ADD KEY `fk_lugar_sedes` (`fk_lugar`),
  ADD KEY `fk_responsable` (`fk_responsable`);

--
-- Indices de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_persona_telefonos` (`fk_persona`),
  ADD KEY `fk_sede_telefonos` (`fk_sede`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_usuario` (`nombre_usuario`),
  ADD KEY `fk_correo_usuarios` (`fk_correo`),
  ADD KEY `fk_rol_usuarios` (`fk_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `certificados`
--
ALTER TABLE `certificados`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `correos_electronicos`
--
ALTER TABLE `correos_electronicos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `estudiantes_areas`
--
ALTER TABLE `estudiantes_areas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `estudiantes_grados`
--
ALTER TABLE `estudiantes_grados`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `familiares`
--
ALTER TABLE `familiares`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `foros`
--
ALTER TABLE `foros`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `grados`
--
ALTER TABLE `grados`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `informaciones_adicionales`
--
ALTER TABLE `informaciones_adicionales`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `lugares`
--
ALTER TABLE `lugares`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `matriculas`
--
ALTER TABLE `matriculas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `matriculas_canceladas`
--
ALTER TABLE `matriculas_canceladas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `periodos_academicos`
--
ALTER TABLE `periodos_academicos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT de la tabla `reset_pass`
--
ALTER TABLE `reset_pass`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sedes`
--
ALTER TABLE `sedes`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`fk_grado`) REFERENCES `grados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `areas_ibfk_2` FOREIGN KEY (`fk_periodo_academico`) REFERENCES `periodos_academicos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `certificados`
--
ALTER TABLE `certificados`
  ADD CONSTRAINT `fk_persona_certiicados` FOREIGN KEY (`fk_persona`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_usuario_certiicados` FOREIGN KEY (`fk_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `correos_electronicos`
--
ALTER TABLE `correos_electronicos`
  ADD CONSTRAINT `fk_persona_correos_electronicos` FOREIGN KEY (`fk_persona`) REFERENCES `personas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `estudiantes_areas`
--
ALTER TABLE `estudiantes_areas`
  ADD CONSTRAINT `estudiantes_areas_ibfk_1` FOREIGN KEY (`fk_areas`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `estudiantes_areas_ibfk_2` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `estudiantes_grados`
--
ALTER TABLE `estudiantes_grados`
  ADD CONSTRAINT `estudiantes_grados_ibfk_1` FOREIGN KEY (`fk_grado`) REFERENCES `grados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `estudiantes_grados_ibfk_2` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `familiares`
--
ALTER TABLE `familiares`
  ADD CONSTRAINT `familiares_ibfk_1` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `familiares_ibfk_2` FOREIGN KEY (`fk_parentesco`) REFERENCES `personas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `foros`
--
ALTER TABLE `foros`
  ADD CONSTRAINT `fk_cancela_matricula_foros` FOREIGN KEY (`fk_cancela_matricula`) REFERENCES `matriculas_canceladas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_estudiante_foros` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_usuario_comenta_foros` FOREIGN KEY (`fk_usuario_comenta`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_usuario_notifica_foros` FOREIGN KEY (`fk_usuario_notifica`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `grados`
--
ALTER TABLE `grados`
  ADD CONSTRAINT `fk_periodo_academico_grados` FOREIGN KEY (`fk_periodo_academico`) REFERENCES `periodos_academicos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `grados_ibfk_1` FOREIGN KEY (`fk_persona`) REFERENCES `personas` (`id`);

--
-- Filtros para la tabla `informaciones_adicionales`
--
ALTER TABLE `informaciones_adicionales`
  ADD CONSTRAINT `informaciones_adicionales_ibfk_1` FOREIGN KEY (`fk_grado`) REFERENCES `grados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `informaciones_adicionales_ibfk_2` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `informaciones_adicionales_ibfk_3` FOREIGN KEY (`fk_matricula`) REFERENCES `matriculas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `lugares`
--
ALTER TABLE `lugares`
  ADD CONSTRAINT `fk_lugar_lugar` FOREIGN KEY (`fk_lugar`) REFERENCES `lugares` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `matriculas`
--
ALTER TABLE `matriculas`
  ADD CONSTRAINT `fk_estudiante_matriculas` FOREIGN KEY (`fk_estudiante`) REFERENCES `personas` (`id`);

--
-- Filtros para la tabla `matriculas_canceladas`
--
ALTER TABLE `matriculas_canceladas`
  ADD CONSTRAINT `fk_estudiante_matricula_matriculas_canceladas` FOREIGN KEY (`fk_matricula`,`fk_estudiante`) REFERENCES `matriculas` (`id`, `fk_estudiante`) ON DELETE CASCADE;

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `personas_ibfk_2` FOREIGN KEY (`fk_sede`) REFERENCES `sedes` (`id`);

--
-- Filtros para la tabla `reset_pass`
--
ALTER TABLE `reset_pass`
  ADD CONSTRAINT `fk_usuario_reset_pass` FOREIGN KEY (`fk_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `sedes`
--
ALTER TABLE `sedes`
  ADD CONSTRAINT `fk_centro_educativo_sedes` FOREIGN KEY (`fk_centro_educativo`) REFERENCES `centros_educativos` (`identificacion`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_lugar_sedes` FOREIGN KEY (`fk_lugar`) REFERENCES `lugares` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sedes_ibfk_1` FOREIGN KEY (`fk_responsable`) REFERENCES `personas` (`id`);

--
-- Filtros para la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD CONSTRAINT `fk_persona_telefonos` FOREIGN KEY (`fk_persona`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_sede_telefonos` FOREIGN KEY (`fk_sede`) REFERENCES `sedes` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_correo_usuarios` FOREIGN KEY (`fk_correo`) REFERENCES `correos_electronicos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_rol_usuarios` FOREIGN KEY (`fk_rol`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
