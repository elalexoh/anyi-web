<?php

namespace App\Http\Controllers;

use App\Models\Lugares;
use Illuminate\Http\Request;

class LugarController extends Controller
{
    public function indexPaises()
    {
        $paises = Lugares::where('fk_lugar', null)->get();

        if (!!count($paises)) {
            $data = response()->json(array(
                'status'    =>  'success',
                'data'   =>  $paises
            ), 200);
        } else {
            $data = response()->json(array(
                'status'    =>  'not found',
                'message'   =>  'No hay paises registrados en la plataforma'
            ), 404);
        }
        return $data;
    }

    public function indexMunicipios($id)
    {
        $municipios = Lugares::where('fk_lugar', $id)->get();
        if (!!count($municipios)) {
            $data = response()->json(array(
                'status'    =>  'success',
                'data'   =>  $municipios
            ), 200);
        } else {
            $data = response()->json(array(
                'status'    =>  'not found',
                'message'   =>  'No hay municipios registrados en la plataforma'
            ), 404);
        }
        return $data;
    }

    public function indexEstados($id)
    {
        $estados = Lugares::where('fk_lugar', $id)->get();
        if (!!count($estados)) {
            $data = response()->json(array(
                'status'    =>  'success',
                'data'   =>  $estados
            ), 200);
        } else {
            $data = response()->json(array(
                'status'    =>  'not found',
                'message'   =>  'No hay estados registrados en la plataforma'
            ), 404);
        }
        return $data;
    }
}
