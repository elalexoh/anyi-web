<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use App\Models\Foros;
use App\Models\Usuarios;
use App\Models\Correos_Electronicos;
use App\Models\Personas;


class ForoController extends Controller
{
  public function index($id)
  {
    // solo debo ver los mensajes que me han enviado y los que he enviado
    $comentarios = Foros::where('fk_usuario_comenta', $id)->orWhere('fk_usuario_notifica', $id)->get();
    // return $comentarios;
    $comments = [];

    if ($comentarios) {
      foreach ($comentarios as $item) {
        $acudiente = Usuarios::find($item->fk_usuario_comenta);
        $correo = Correos_Electronicos::find($acudiente->fk_correo);
        $persona = Personas::find($correo->fk_persona);

        $comments[] = [
          'id'                =>  $item->id,
          'tipo'              =>  $item->tipo,
          'descripcion'       =>  $item->descripcion,
          'estatus'           =>  $item->estatus,
          'fecha_creacion'    =>  $item->fecha_creacion,
          'acudiente'         =>  $acudiente->nombre_usuario
        ];
      }
      $response = response()->json(
        array(
          'status'    => 'Success',
          'data'   =>  $comments,
        ),
        200
      );
    } else {
      $response = response()->json(
        array(
          'status'    => 'Resources not found',
          'message'   =>  'No hay comentarios en la plataforma'
        ),
        404
      );
    }
    return $response;
  }

  public function store(Request $request)
  {

    $token = $request->header('Authorization');
    $jwtAuth = new \JwtAuth();
    $checkToken = $jwtAuth->checkToken($token);

    if ($checkToken) {
      $identity = $jwtAuth->checkToken($token, true);

      // Recibir datos del post
      $params_array = array(
        'comentario'      =>  $request->input('comentario'),
        'fk_usuario_notifica'      =>  $request->input('fk_usuario_notifica')
      );
      $params = (object) $params_array;

      if (!empty($params) && !empty($params_array)) {

        // Validamos los datos
        $validate = \Validator::make($params_array, [
          'comentario'    => 'required'
        ]);

        if ($validate->fails()) {
          // Validaciones fallan
          $data = array(
            'status'    => 'error',
            'code'      => 404,
            'message'   => 'Ha ocurrido un problema con la validación de los datos',
            'errors'    => $validate->errors()
          );
        } else {
          // Los datos fueron validados correctamente
          $today = new DateTime();

          $comentario = new Foros();
          $comentario->tipo               = 'FORO';
          $comentario->descripcion        = $params->comentario;
          $comentario->fecha_creacion     = $today->format('Y-m-d H:i:s');
          $comentario->estatus            = 'NO VISTO';
          $comentario->fk_usuario_comenta = $identity->id_user;
          $comentario->fk_usuario_notifica = $params->fk_usuario_notifica;
          $comentario->save();

          $data = array(
            'status'    => 'success',
            'code'      => 200,
            'message'   => 'Comentario registrado satisfactoriamente'
          );
        }
      } else {
        // Si los datos están vacíos.
        $data = array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'No se han recibido los datos',
          'datos'     => $request->input()
        );
      }
    } else {
      $data = array(
        'status'    => 'error',
        'code'      =>  400,
        'message'   => 'Login incorrecto'
      );
    }

    return response()->json($data, 200);
  }

  public function updateComment($id)
  {
    $comentario = Foros::where('id', $id)->first();
    if (is_object($comentario)) {
      $comentario->estatus = 'VISTO';
      $comentario->update();

      $response = response()->json(
        array(
          'status'    => 'success',
          'message'   =>  'Actualizado correctamente'
        ),
        200
      );
    } else {
      $response = response()->json(
        array(
          'status'    => 'not found',
          'message'   =>  'No se ha encontrado el registro'
        ),
        404
      );
    }

    return $response;
  }
}
