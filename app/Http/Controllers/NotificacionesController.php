<?php

namespace App\Http\Controllers;

use App\Models\Certificados;
use App\Models\Personas;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Validator;

class NotificacionesController extends Controller
{
  public function index($id)
  {
    $estudiante_notificacion = [];
    $notas = Certificados::where('fk_usuario', $id)->where('tipo', 'notas_full')->get();
    foreach ($notas as $nota) {
      $estudiante_notificacion[] = [
        'estudiante' => Personas::where('id', $nota->fk_persona)->first(),
        'nota'       => $nota
      ];
    }
    if (count($notas)) {
      $data = response()->json(array(
        'status'    =>  'Success',
        'data'      =>  $estudiante_notificacion
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not resources',
        'message'   =>  'No se han encontrado registros'
      ), 404);
    }
    return $data;
  }
  public function create(Request $request)
  {
    $params_array = array(
      'tipo'               => $request->input('tipo'),
      'descripcion'        => $request->input('descripcion'),
      'estatus'            => $request->input('estatus'),
      'url_certificado'    => $request->input('url_certificado'),
      'fk_usuario'         => $request->input('fk_usuario'),
      'fk_persona'         => $request->input('fk_persona')
    );

    $params = (object) $params_array;
    if (!empty($params) && !empty($params_array)) {
      $validate = Validator::make($params_array, [
        'tipo'              => 'nullable|string',
        'descripcion'       => 'nullable|string',
        'estatus'           => 'nullable|string',
        'fk_usuario'        => 'required|integer',
        'fk_persona'        => 'required|integer',
      ]);

      if (!$validate->fails()) {

        $certificado                    = new Certificados();
        $certificado->tipo              = $params->tipo;
        $certificado->descripcion       = $params->descripcion;
        $certificado->estatus           = $params->estatus;
        $certificado->url_certificado   = '';
        $certificado->fk_usuario        = $params->fk_usuario;
        $certificado->fk_persona        = $params->fk_persona;
        $certificado->save();

        $data = response()->json(array(
          'status'        => 'Success',
          'message'       => 'Certificado registrado satisfactoriamente',
          'certificado'   => $certificado,
        ), 200);
      } else {
        $data = response()->json(array(
          'status'    => 'Error',
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        ), 404);
      }
    }
    return $data;
  }
  public function load(Request $request, $id)
  {
    // return $request;
    $certificado = Certificados::find($id);

    if (is_object($certificado)) {
      $notas_pdf                      = $request->file('notas_pdf')->store('usuarios/documentos/', 'public');
      $url_notas_pdf                  = asset('storage/' . '' . $notas_pdf);
      $certificado->url_certificado   = $url_notas_pdf;
      $certificado->estatus           = 'ENVIADO';
      $certificado->update();
      $data = response()->json(array(
        'status'        =>  'Success',
        'message'       =>  'Se ha actualizado correctamente el certificado'
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not Resources',
        'message'   =>  'No se ha encontrado el registro'
      ), 404);
    }
    return $data;
  }
  public function checkFullNotas(
    $id_docente,
    $id_estudiante
  ) {
    $certificado = Certificados::where('fk_usuario', $id_docente)->where('fk_persona', $id_estudiante)->first();
    if (is_object($certificado)) {
      $info[] = [
        'certificado' => $certificado->estatus
      ];
      $data = response()->json(array(
        'status'        => 'Success',
        'data'          => $certificado,
        'info'          => $info,
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    => 'Error',
        'message'   => 'No se han encontrado registros',
      ), 404);
    }
    return $data;
  }
  public function NotificationsUnread($id)
  {
    $notifications = $this->index($id);
    return $notifications;
  }
}
