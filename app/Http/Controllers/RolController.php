<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Roles;

class RolController extends Controller
{
    // Listar roles
    public function index()
    {
        $roles = Roles::all()->toArray();
        return $roles;
    }

    // Borrar rol
    public function delete($id)
    {
        $rol = Roles::find($id);
        $rol->delete();
 
        return response()->json('El rol se ha borrado exitosamente');
    }

    // Crear rol
    public function create(Request $request)
    {
        $rol = new Roles([
            'nombre' => $request->input('nombre')
        ]);
        $rol->save();
 
        return response()->json('El rol se ha añadido correctamente');
    }

    // Editar rol
    public function edit($id)
    {
        $rol = Roles::find($id);
        return response()->json($rol);
    }
 
    // Actualizar rol
    public function update(Request $request, $id)
    {
        $rol = Roles::find($id);
        $rol->update($request->all());
 
        return response()->json('El rol se ha actualizado correctamente');
    }

}
