<?php

namespace App\Http\Controllers;

use App\Estudiantes_Grados;
use App\Models\Correos_Electronicos;
use Illuminate\Http\Request;
use DateTime;
use App\Models\Grados;
use App\Models\Periodos_Academicos;
use App\Models\Personas;
use App\Models\Sedes;
use App\Models\Usuarios;
use App\Models\Areas;
use App\Models\Estudiantes_Areas;
use Illuminate\Support\Facades\Validator;

class GradoController extends Controller
{
  // grados por usuario
  public function indexGrados(Request $request)
  {

    $token = $request->header('Authorization');
    $jwtAuth = new \JwtAuth();
    $checkToken = $jwtAuth->checkToken($token);
    $periodos_academicos = [];

    if ($checkToken) {
      $identity = $jwtAuth->checkToken($token, true);
      $persona = Usuarios::find($identity->id_user)->correos_electronicos->personas;
      $grados = Grados::where('fk_persona', $persona->id)->get();
      if (count($grados)) {
        foreach ($grados as $grado) {
          $periodos_academicos[] = [
            'grado' => $grado,
            'periodo_academico' => Periodos_Academicos::where('id', $grado->fk_periodo_academico)->first()
          ];
        }

        $data = response()->json(array(
          'status'    =>  'Success',
          'data'      =>  $periodos_academicos
        ), 200);
      } else {

        $data = response()->json(array(
          'status'    =>  'Resources not found',
          'message'      => 'no hay grados registrados para este docente'
        ), 404);
      }
    } else {
      $data = response()->json(array(
        'status'    =>  'Access Forbiden',
        'message'      =>  'Debe estar logeado para continuar'
      ), 403);
    }
    return $data;
  }
  // todos los grados
  public function index()
  {
    $grados = Grados::all();
    if (!!count($grados)) {
      $grado_data = [];
      foreach ($grados as $grado) {
        $persona = Personas::where('id', $grado->fk_persona)->first();
        $sede = Sedes::where('id', $persona->fk_sede)->first();
        $grado_data[] = [
          "id"                    => $grado->id,
          "nombre"                => $grado->nombre,
          "fk_periodo_academico"  => $grado->fk_periodo_academico,
          "fk_persona"            => $grado->fk_persona,
          "persona"               => $persona,
          "periodo"               => Periodos_Academicos::where('id', $grado->fk_periodo_academico)->first(),
          "sede"                  => $sede,
        ];
      }
      $data = response()->json(array(
        'status'    =>  'success',
        'data'      =>  $grado_data
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not found',
        'message'   =>  'No se encontraron registros',
      ), 404);
    }
    return $data;
  }

  // Grados por sede
  public function gradosBySede($id)
  {
    $grados = Grados::all();
    if (!!count($grados)) {
      $grado_data = [];
      foreach ($grados as $grado) {
        $persona = Personas::where('id', $grado->fk_persona)->where('fk_sede', $id)->first();
        if (is_object($persona)) {
          $grado_data[] = [
            "id"                    => $grado->id,
            "nombre"                => $grado->nombre,
            "fk_periodo_academico"  => $grado->fk_periodo_academico,
            "fk_persona"            => $grado->fk_persona,
            "persona"               => $persona,
            "periodo"               => Periodos_Academicos::where('id', $grado->fk_periodo_academico)->first(),
          ];
        }
      }
      $data = response()->json(array(
        'status'    =>  'success',
        'data'      =>  $grado_data
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not found',
        'message'   =>  'No se encontraron registros',
      ), 404);
    }
    return $data;
  }

  public function getGrado($id)
  {
    $grado = Grados::find($id);
    $responsable = $grado->personas;
    $periodos_academicos = Periodos_Academicos::where('id', $grado->fk_periodo_academico)->get();
    $grado_detail = [
      'grado' => $grado,
      'responsable' => $responsable,
      'periodos_academicos' => $periodos_academicos,
    ];
    if (is_object($grado)) {
      $data = response()->json(array(
        'status'    =>  'success',
        'data'      =>  $grado_detail
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not found',
        'message'   =>  'Este registro no existe',
      ), 404);
    }
    return $data;
  }

  public function asignarGrados(Request $request)
  {
    $params_array = array(
      'nombre'                     =>  $request->input('grado.nombre'),
      'id_docente'                 =>  $request->input('grado.docente'),
      'id_periodo_academico'       =>  $request->input('grado.periodo_academico'),
      'estudiantes'                =>  $request->input('estudiantes'),
    );
    if (!empty($params_array)) {

      // validamos los datos
      $validate = Validator::make($params_array, [
        'nombre'                  => 'required',
        'id_docente'              => 'required|integer',
        'id_periodo_academico'    => 'required|integer',
      ]);

      if (!$validate->fails()) {
        // save grado
        $grado                         = new Grados();
        $grado->nombre                 = $request->input('grado.nombre');
        $grado->fk_persona             = $request->input('grado.docente');
        $grado->fk_periodo_academico   = $request->input('grado.periodo_academico');
        $grado->save();

        // assign students
        if (!!count($request->input('estudiantes')) && $request->input('estudiantes')[0]['estudiante'] !== null) {
          foreach ($request->input('estudiantes') as $estudiante) {
            $estudiante_bd                = new Estudiantes_Grados();
            $estudiante_bd->fk_estudiante = $estudiante['estudiante'];
            $estudiante_bd->fk_grado      = $grado->id;
            $estudiante_bd->save();
          }
        }

        $data = array(
          'status'    => 'success',
          'code'      => 200,
          'message'   => 'Registro exitoso',
        );
      } else {
        $data = array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        );
      }
    } else {
      $data = array(
        'status'    => 'error',
        'code'      => 404,
        'message'   => 'No se han recibido los datos',
        'datos'     => $request->input()
      );
    }
    return $data;
  }

  public function getAreasbyGrado($id)
  {
    $areas = Areas::where('fk_grado', $id)->get();
    if (count($areas)) {
      $data = response()->json(array(
        'status'    =>  'Success',
        'data'      =>  $areas,
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Resources not found',
        'message'    =>  'No hay areas asignadas a este grado.',
      ), 404);
    }
    return $data;
  }

  public function deleteGrado($id)
  {
    $grado = Grados::where('id', $id)->first();
    if (is_object($grado)) {
      $grado->delete();
      $data = response()->json(array(
        'status'    =>  'Success',
        'message'   =>  'Registro eliminado exitosamente',
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not Found Resources',
        'message'   => 'No se ha encontrado el registro',
      ), 404);
    }
    return $data;
  }

  public function editGrado(Request $request, $id)
  {
    $grado = Grados::where('id', $id)->first();
    $__issues = [];
    if (is_object($grado)) {

      $grado->nombre                  = $request->input('nombre');
      $grado->fk_periodo_academico    = $request->input('fk_periodo_academico');
      $grado->fk_persona              = $request->input('fk_persona');
      if (!!count($request->input('estudiantes')) && $request->input('estudiantes')[0]['estudiante'] !== null) {
        foreach ($request->input('estudiantes') as $estudiante) {

          $__estudiante = Estudiantes_Grados::where('fk_grado', $grado->id)
            ->where('fk_estudiante', $estudiante['estudiante'])
            ->first();
          if (!is_object($__estudiante)) {
            $estudiante_bd                = new Estudiantes_Grados();
            $estudiante_bd->fk_estudiante = $estudiante['estudiante'];
            $estudiante_bd->fk_grado      = $grado->id;
            $estudiante_bd->save();
          } else {
            $__issues[] = 'Este estudiante ya fue asignado con anterioridad';
          }
        }
      }
      $grado->update();

      $data = response()->json(array(
        'status'    =>  'Success',
        'message'   =>  'Registro actualizado exitosamente',
        'issues'    =>  $__issues

      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not resources',
        'message'   =>  'No se han encontrado registros'
      ), 404);
    }
    return $data;
  }
}
