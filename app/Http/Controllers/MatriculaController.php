<?php

namespace App\Http\Controllers;

use App\Estudiantes_Grados;
use App\Models\Correos_Electronicos;
use App\Models\Familiares;
use App\Models\Grados;
use App\Models\Informaciones_Adicionales;
use Illuminate\Http\Request;
use App\Models\Matriculas;
use App\Models\Matriculas_Canceladas;
use App\Models\Personas;
use App\Models\Sedes;
use App\Models\Usuarios;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class MatriculaController extends Controller
{

  public function indexMatriculas()
  {
    $matriculados = Matriculas::all();
    $matriculas_data = [];

    if (count($matriculados)) {
      foreach ($matriculados as $matricula) {
        $estudiante = Personas::where('id', $matricula->fk_estudiante)->first();
        $familiar = Familiares::where('fk_estudiante', $estudiante->id)->first();
        $acudiente = Personas::where('id', $familiar->fk_parentesco)->first();
        $sede = Sedes::where('id', $estudiante->fk_sede)->first();
        $extra_info = Informaciones_Adicionales::where('fk_matricula', $matricula->id)->first();
        $grado = Grados::where('id', $extra_info->fk_grado)->first();
        $matriculas_data[] = [
          "id"                  => $matricula->id,
          "estudiante"          => $estudiante->dni,
          "acudiente"           => $acudiente->p_nombre . ' ' . $acudiente->p_apellido,
          "grado"               => $grado->nombre,
          "estatus"             => $matricula->estatus,
          "fecha_matricula"     => $matricula->fecha_matricula,
          "sede"                => $sede->nombre,
          "firma_acudiente"     => $matricula->img_firma_acudiente,
          "firma_estudiante"    => $matricula->img_firma_estudiante,
        ];
      }
      $data = response()->json(array(
        'status'    =>  'success',
        'data'      =>  $matriculas_data
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not resources',
        'message'      => 'No se han registrado matriculas'
      ), 204);
    }
    return $data;
  }

  public function matriculasBySede($id)
  {
    $estudiantes = Personas::where('fk_sede', $id)->where('tipo', 'estudiante')->get();
    $matriculas_data = [];

    if (count($estudiantes)) {
      foreach ($estudiantes as $estudiante) {
        $matricula = Matriculas::where('fk_estudiante', $estudiante->id)->first();
        $familiar = Familiares::where('fk_estudiante', $estudiante->id)->first();
        // $persona = Personas::where('id', $familiar->fk_parentesco)->first();
        if (is_object($matricula)) {
          $persona = Personas::where('id', $familiar->fk_parentesco)->first();
          $sede = Sedes::where('id', $estudiante->fk_sede)->first();
          $extra_info = Informaciones_Adicionales::where('fk_matricula', $matricula->id)->first();
          $grado = Grados::where('id', $extra_info->fk_grado)->first();

          $matriculas_data[] = [
            "id"                  => $matricula->id,
            "estudiante"          => $estudiante->dni,
            "acudiente"           => $persona->p_nombre . ' ' . $persona->p_apellido,
            "grado"               => $grado->nombre,
            "estatus"             => $matricula->estatus,
            "fecha_matricula"     => $matricula->fecha_matricula,
            "sede"                => $sede->nombre,
            "firma_acudiente"     => $matricula->img_firma_acudiente,
            "firma_estudiante"    => $matricula->img_firma_estudiante,
            //
            // "matricula"       => $matricula,
            // "estudiante_dni"  => $estudiante->dni,
            // "persona"         => $persona,
            // "grado"           => $grado,
            // "sede"            => $sede,
          ];
        }
      }
      $data = response()->json(array(
        'status'    =>  'success',
        'data'      =>  $matriculas_data
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not resources',
        'message'      => 'No se han registrado matriculas'
      ), 204);
    }
    return $data;
  }

  public function matriculasByAcudiente($id)
  {
    $estudiantes = Familiares::where('fk_parentesco', $id)->get();
    $matriculas_data = [];

    if (count($estudiantes)) {
      foreach ($estudiantes as $estudiante) {
        $matricula = Matriculas::where('fk_estudiante', $estudiante->fk_estudiante)->first();
        $estudiante_data = Personas::where('id', $estudiante->fk_estudiante)->first();
        $sede = Sedes::where('id', $estudiante_data->fk_sede)->first();
        $familiar = Familiares::where('fk_estudiante', $estudiante_data->id)->first();
        $persona = Personas::where('id', $familiar->fk_parentesco)->first();
        $extra_info = Informaciones_Adicionales::where('fk_matricula', $matricula->id)->first();
        $grado = Grados::where('id', $extra_info->fk_grado)->first();
        $matriculas_data[] = [
          "id"                  => $matricula->id,
          "estudiante"          => $estudiante_data->dni,
          "acudiente"           => $persona->p_nombre . ' ' . $persona->p_apellido,
          "grado"               => $grado->nombre,
          "estatus"             => $matricula->estatus,
          "fecha_matricula"     => $matricula->fecha_matricula,
          "sede"                => $sede->nombre,
          "firma_acudiente"     => $matricula->img_firma_acudiente,
          "firma_estudiante"    => $matricula->img_firma_estudiante,
        ];
      }
      $data = response()->json(array(
        'status'    =>  'success',
        'data'      =>  $matriculas_data
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not resources',
        'message'      => 'No se han registrado matriculas'
      ), 204);
    }
    return $data;
  }

  public function getMatricula($id)
  {
    $matricula = Matriculas::find($id);
    if (is_object($matricula)) {
      $familiares = Familiares::where('fk_estudiante', $matricula->fk_estudiante)->first();
      $acudiente = Personas::where('id', $familiares->fk_parentesco)->first();
      $estudiante = Personas::where('id', $matricula->fk_estudiante)->first();
      $extra_info = Informaciones_Adicionales::where('fk_matricula', $matricula->id)->first();
      $info_cancelacion = Matriculas_Canceladas::where('fk_matricula', $matricula->id)->first();
      $data = response()->json(array(
        'status'          =>  'success',
        'matricula'       => $matricula,
        'estudiante'      => $estudiante,
        'acudiente'       => $acudiente,
        'extra_info'      => $extra_info,
        'cancelacion'     => $info_cancelacion
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not resources',
        'message'      => 'No se han encontrado registros en la plataforma'
      ), 404);
    }

    return $data;
  }

  public function createMatricula(Request $request)
  {
    try {
      DB::beginTransaction();
      $params_array = array(
        "fecha_matricula"               =>  $request->input('fecha_matricula'),
        "estatus"                       =>  $request->input('status'),
        "img_cedula_mama"               =>  $request->file('img_cedula_mama'),
        "img_cedula_papa"               =>  $request->file('img_cedula_papa'),
        "img_cedula_acudiente"          =>  $request->file('img_cedula_acudiente'),
        "img_cedula_estudiante"         =>  $request->file('img_cedula_estudiante'),
        "notas"                         =>  $request->file('notas'),
        "img_firma_acudiente"           =>  $request->file('img_firma_acudiente'),
        "img_firma_familiar"            =>  $request->file('img_firma_familiar'),
        "img_firma_estudiante"          =>  $request->file('img_firma_estudiante'),
        "religion"                      =>  $request->input('religion'),
        "sede"                          =>  $request->input('sede'),
        "lugar_expedicion"              =>  $request->input('lugar_expedicion'),


        //estudiante
        "p_nombre_estudiante"           => $request->input('p_nombre_estudiante'),
        "s_nombre_estudiante"           => $request->input('s_nombre_estudiante'),
        "p_apellido_estudiante"         => $request->input('p_apellido_estudiante'),
        "s_apellido_estudiante"         => $request->input('s_apellido_estudiante'),
        "dni_estudiante"                => $request->input('dni_estudiante'),
        "fecha_nacimiento_estudiante"   => $request->input('fecha_nacimiento_estudiante'),
        "direccion_estudiante"          => $request->input('direccion_estudiante'),
        "rh"                            => $request->input('rh'),
        "lugar_nacimiento"              => $request->input('lugar_nacimiento'),

        // acudiente
        "id_acudiente"                  => $request->input('id_acudiente'),
        "p_nombre_acudiente"            => $request->input('p_nombre_acudiente'),
        "s_nombre_acudiente"            => $request->input('s_nombre_acudiente'),
        "p_apellido_acudiente"          => $request->input('p_apellido_acudiente'),
        "s_apellido_acudiente"          => $request->input('s_apellido_acudiente'),
        "dni_acudiente"                 => $request->input('dni_acudiente'),
        "fecha_nacimiento_acudiente"    => $request->input('fecha_nacimiento_acudiente'),
        "direccion_acudiente"           => $request->input('direccion_acudiente'),
        "clave_usuario"                 => $request->input('clave_usuario'),
        "nombre_usuario"                => $request->input('nombre_usuario'),
        "direccion_correo"              => $request->input('direccion_correo'),

        //informacion extra
        "tipo"                          => $request->input('tipo'),
        "enfermedades_sufridas"         => $request->input('enfermedades_sufridas'),
        "sisben"                        => $request->input('sisben'),
        "strato"                        => $request->input('strato'),
        "nro_hermanos"                  => $request->input('nro_hermanos'),
        "posicion_hermanos"             => $request->input('posicion_hermanos'),
        "procedencia"                   => $request->input('procedencia'),
        "nuevo"                         => $request->input('nuevo'),
        "repitiente"                    => $request->input('repitiente'),
        "grado"                         => $request->input('grado'),

        //mama
        "p_nombre_mama"                 => $request->input('p_nombre_mama'),
        "s_nombre_mama"                 => $request->input('s_nombre_mama'),
        "p_apellido_mama"               => $request->input('p_apellido_mama'),
        "s_apellido_mama"               => $request->input('s_apellido_mama'),
        "dni_madre"                     => $request->input('dni_madre'),

        //padre
        "p_nombre_padre"                => $request->input('p_nombre_padre'),
        "s_nombre_padre"                => $request->input('s_nombre_padre'),
        "p_apellido_padre"              => $request->input('p_apellido_padre'),
        "s_apellido_padre"              => $request->input('s_apellido_padre'),
        "dni_padre"                     => $request->input('dni_padre'),
      );
      $params = (object) $params_array;
      if (!empty($params) && !empty($params_array)) {

        // validamos los datos
        $validate = Validator::make($params_array, [
          "img_cedula_mama"             => 'nullable|mimes:pdf,jpg,bmp,png',
          "img_cedula_papa"             => 'nullable|mimes:pdf,jpg,bmp,png',
          "img_cedula_acudiente"        => 'nullable|mimes:pdf,jpg,bmp,png',
          "img_cedula_estudiante"       => 'nullable|mimes:pdf,jpg,bmp,png',
          "notas"                       => 'nullable|mimes:pdf,jpg,bmp,png',
          "img_firma_acudiente"         => 'nullable|mimes:pdf,jpg,bmp,png',
          "img_firma_familiar"          => 'nullable|mimes:pdf,jpg,bmp,png',
          "img_firma_estudiante"        => 'nullable|mimes:pdf,jpg,bmp,png',
          "fecha_matricula"             => 'required',
          "religion"                    => 'required',
          "sede"                        => 'required',
          "p_nombre_estudiante"         => 'required',
          "p_apellido_estudiante"       => 'required',
          "dni_estudiante"              => 'required|integer',
          "fecha_nacimiento_estudiante" => 'required',
          "direccion_estudiante"        => 'required',
          "p_nombre_acudiente"          => 'nullable',
          "p_apellido_acudiente"        => 'nullable',
          "dni_acudiente"               => 'nullable|integer',
          "fecha_nacimiento_acudiente"  => 'nullable',
          "direccion_acudiente"         => 'nullable',
          "grado"                       => 'required',

          "lugar_expedicion"            => 'required',
          "lugar_nacimiento"            => 'required',
          "rh"                          => 'required',
        ]);

        if (!$validate->fails()) {

          $today = new DateTime();

          //guardar imagenes
          $img_cedula_mama        = $request->file('img_cedula_mama') == null ? null : $request->file('img_cedula_mama')->store('matriculas/identificaciones/madres', 'public');
          $img_cedula_papa        = $request->file('img_cedula_papa') == null ? null : $request->file('img_cedula_papa')->store('matriculas/identificaciones/padres', 'public');
          $img_cedula_acudiente   = $request->file('img_cedula_acudiente') == null ? null : $request->file('img_cedula_acudiente')->store('matriculas/identificaciones/acudientes', 'public');
          $img_cedula_estudiante  = $request->file('img_cedula_estudiante') == null ? null : $request->file('img_cedula_estudiante')->store('matriculas/identificaciones/estudiantes', 'public');
          $notas                  = $request->file('notas') == null ? null : $request->file('notas')->store('matriculas/notas', 'public');
          $img_firma_acudiente    = $request->file('img_firma_acudiente') == null ? null : $request->file('img_firma_acudiente')->store('matriculas/firmas/acudientes', 'public');
          $img_firma_familiar     = $request->file('img_firma_familiar') == null ? null : $request->file('img_firma_familiar')->store('matriculas/firmas/familiares', 'public');
          $img_firma_estudiante   = $request->file('img_firma_estudiante') == null ? null : $request->file('img_firma_estudiante')->store('matriculas/firmas/estudiantes', 'public');

          //obtener las urls de las imagenes
          $url_img_cedula_mama       = asset('storage/' . '' . $img_cedula_mama);
          $url_img_cedula_papa       = asset('storage/' . '' . $img_cedula_papa);
          $url_img_cedula_acudiente  = asset('storage/' . '' . $img_cedula_acudiente);
          $url_img_cedula_estudiante = asset('storage/' . '' . $img_cedula_estudiante);
          $url_notas                 = asset('storage/' . '' . $notas);
          $url_img_firma_acudiente   = asset('storage/' . '' . $img_firma_acudiente);
          $url_img_firma_familiar    = asset('storage/' . '' . $img_firma_familiar);
          $url_img_firma_estudiante  = asset('storage/' . '' . $img_firma_estudiante);


          // crear el estudiante
          $estudiante = new Personas();
          $estudiante->p_nombre = $params->p_nombre_estudiante;
          $estudiante->s_nombre = $params->s_nombre_estudiante;
          $estudiante->p_apellido = $params->p_apellido_estudiante;
          $estudiante->s_apellido = $params->s_apellido_estudiante;
          $estudiante->dni = $params->dni_estudiante;
          $estudiante->fecha_nacimiento = $params->fecha_nacimiento_estudiante;
          $estudiante->direccion = $params->direccion_estudiante;
          $estudiante->tipo = 'estudiante'; //esto debe ser una tabla
          $estudiante->fk_sede = $params->sede;
          $estudiante->save();

          if ($params->id_acudiente === null) {
            // crear el acudiente
            $acudiente = new Personas();
            $acudiente->p_nombre = $params->p_nombre_acudiente;
            $acudiente->s_nombre = $params->s_nombre_acudiente;
            $acudiente->p_apellido = $params->p_apellido_acudiente;
            $acudiente->s_apellido = $params->s_apellido_acudiente;
            $acudiente->dni = $params->dni_acudiente;
            $acudiente->fecha_nacimiento = $params->fecha_nacimiento_acudiente;
            $acudiente->direccion = $params->direccion_acudiente;
            $acudiente->tipo = 'acudiente'; //esto debe ser una tabla
            $acudiente->fk_sede =  $params->sede;
            $acudiente->save();

            // crear el correo del acudiente
            $correo_acudiente = new Correos_Electronicos();
            $correo_acudiente->direccion_correo = $params->direccion_correo;
            $correo_acudiente->fk_persona = $acudiente->id;
            $correo_acudiente->save();

            // crear el usuario del acudiente con el correo
            $usuario = new Usuarios();
            $usuario->clave_usuario = Hash::make($params->clave_usuario);
            $usuario->nombre_usuario = $params->nombre_usuario;
            $usuario->fk_correo = $correo_acudiente->id;
            $usuario->fk_rol = 4; //acudiente
            $usuario->estado = 'ACTIVO'; //acudiente
            $usuario->politicas = 0; //!nose que es esto
            $usuario->fecha_registro    = $today->format('Y-m-d H:i:s');
            $usuario->save();
          }

          // crear parentezco
          $parentesco = new Familiares();
          $parentesco->fk_estudiante = $estudiante->id;
          $parentesco->fk_parentesco = $params->id_acudiente === null ? $acudiente->id : $params->id_acudiente;
          $parentesco->save();

          // crear la matricula
          $matricula = new Matriculas();
          $matricula->fecha_matricula       =  $params->fecha_matricula;
          $matricula->estatus               =  'Pendiente';
          $matricula->img_firma_acudiente   =  $url_img_firma_acudiente;
          $matricula->img_firma_familiar    =  $url_img_firma_familiar;
          $matricula->img_firma_estudiante  =  $url_img_firma_estudiante;
          $matricula->religion              =  $params->religion === 'true' ? 1 : 0;
          $matricula->img_cedula_mama       =  $url_img_cedula_mama;
          $matricula->img_cedula_papa       =  $url_img_cedula_papa;
          $matricula->img_cedula_acudiente  =  $url_img_cedula_acudiente;
          $matricula->img_cedula_estudiante =  $url_img_cedula_estudiante;
          $matricula->notas                 =  $url_notas;
          $matricula->fk_estudiante         =  $estudiante->id;
          $matricula->save();

          // añadir la información extra
          $extra_info                             = new Informaciones_Adicionales();
          $extra_info->tipo                       = $params->tipo;
          $extra_info->enfermedades_sufridas      = $params->enfermedades_sufridas;
          $extra_info->sisben                     = $params->sisben;
          $extra_info->estrato_socioeconimico     = $params->strato;
          $extra_info->nro_hermanos               = $params->nro_hermanos;
          $extra_info->posicion_hermanos          = $params->posicion_hermanos;
          $extra_info->procedencia                = $params->procedencia;
          $extra_info->nuevo                      = $params->nuevo ? 1 : 0;
          $extra_info->repitiente                 = $params->repitiente ? 1 : 0;
          $extra_info->fk_grado                   = $params->grado;
          $extra_info->fk_matricula               = $matricula->id;
          $extra_info->fk_estudiante              = $estudiante->id;
          // nueva data
          $extra_info->p_nombre_mama              = $params->p_nombre_mama;
          $extra_info->s_nombre_mama              = $params->s_nombre_mama;
          $extra_info->p_apellido_mama            = $params->p_apellido_mama;
          $extra_info->s_apellido_mama            = $params->s_apellido_mama;
          $extra_info->dni_madre                  = $params->dni_madre;
          $extra_info->p_nombre_padre             = $params->p_nombre_padre;
          $extra_info->s_nombre_padre             = $params->s_nombre_padre;
          $extra_info->p_apellido_padre           = $params->p_apellido_padre;
          $extra_info->s_apellido_padre           = $params->s_apellido_padre;
          $extra_info->dni_padre                  = $params->dni_padre;

          $extra_info->lugar_expedicion           = $params->lugar_expedicion;
          $extra_info->lugar_nacimiento           = $params->lugar_nacimiento;
          $extra_info->rh                         = $params->rh;
          $extra_info->save();

          $estudiante_grado                       = new Estudiantes_Grados();
          $estudiante_grado->fk_estudiante        = $estudiante->id;
          $estudiante_grado->fk_grado             = $params->grado;
          $estudiante_grado->save();
        }
      }
      DB::commit();
      $data = response()->json(array(
        'status'        => 'Success',
        'message'       => 'Matricula registrada satisfactoriamente',
      ), 200);
    } catch (Exception $ex) {
      DB::rollback();
      $data = response()->json(array(
        'status'        => 'Error',
        'message'       => $ex->getMessage(),
      ), 500);
    }
    return $data;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function updateMatricula(Request $request, $id)
  {
    $today = new DateTime();
    $matricula = Matriculas::find($id);
    $familiares = Familiares::where('fk_estudiante', $matricula->fk_estudiante)->first();
    $acudiente = Personas::where('id', $familiares->fk_parentesco)->first();
    $estudiante = Personas::where('id', $matricula->fk_estudiante)->first();
    $extra_info = Informaciones_Adicionales::where('fk_matricula', $matricula->id)->first();
    $estudiante_grado = Estudiantes_Grados::where([
      ['fk_grado', "=", $extra_info->fk_grado],
      ['fk_estudiante', "=", $estudiante->id]
    ])->first();
    // $estudiante_grado = Estudiantes_Grados::where('fk_grado', $extra_info->fk_grado)->first();

    try {

      //guardar imagenes
      $img_cedula_mama           = is_null($request->file('img_cedula_mama')) ? $matricula->img_cedula_mama : $request->file('img_cedula_mama')->store('matriculas/identificaciones/madres', 'public');

      $img_cedula_papa           = is_null($request->file('img_cedula_papa')) ? $matricula->img_cedula_papa : $request->file('img_cedula_papa')->store('matriculas/identificaciones/padres', 'public');

      $img_cedula_acudiente      = is_null($request->file('img_cedula_acudiente')) ? $matricula->img_cedula_acudiente : $request->file('img_cedula_acudiente')->store('matriculas/identificaciones/acudientes', 'public');

      $img_cedula_estudiante     = is_null($request->file('img_cedula_estudiante')) ? $matricula->img_cedula_estudiante : $request->file('img_cedula_estudiante')->store('matriculas/identificaciones/estudiantes', 'public');

      $notas                     = is_null($request->file('notas')) ? $matricula->notas : $request->file('notas')->store('matriculas/identificaciones/notas', 'public');

      $img_firma_acudiente       = is_null($request->file('img_firma_acudiente')) ? $matricula->img_firma_acudiente : $request->file('img_firma_acudiente')->store('matriculas/firmas/acudientes', 'public');

      $img_firma_familiar        = is_null($request->file('img_firma_familiar')) ? $matricula->img_firma_familiar : $request->file('img_firma_familiar')->store('matriculas/firmas/familiares', 'public');

      $img_firma_estudiante      = is_null($request->file('img_firma_estudiante')) ? $matricula->img_firma_estudiante : $request->file('img_firma_estudiante')->store('matriculas/firmas/estudiantes', 'public');




      //obtener las urls de las imagenes
      $url_img_cedula_mama          = is_null($request->file('img_cedula_mama')) ? $matricula->img_cedula_mama : asset('storage/' . '' . $img_cedula_mama);

      $url_img_cedula_papa          = asset('storage/' . '' . $img_cedula_papa);

      $url_img_cedula_acudiente     = is_null($request->file('img_cedula_acudiente')) ? $matricula->img_cedula_acudiente : asset('storage/' . '' . $img_cedula_acudiente);

      $url_img_cedula_estudiante    = is_null($request->file('img_cedula_estudiante')) ? $matricula->img_cedula_estudiante : asset('storage/' . '' . $img_cedula_estudiante);

      $url_notas                    = is_null($request->file('notas')) ? $matricula->notas : asset('storage/' . '' . $notas);

      $url_img_firma_acudiente      = is_null($request->file('img_firma_acudiente')) ? $matricula->img_firma_acudiente : asset('storage/' . '' . $img_firma_acudiente);

      $url_img_firma_familiar       = is_null($request->file('img_firma_familiar')) ? $matricula->img_firma_familiar : asset('storage/' . '' . $img_firma_familiar);

      $url_img_firma_estudiante     = is_null($request->file('img_firma_estudiante')) ? $matricula->img_firma_estudiante : asset('storage/' . '' . $img_firma_estudiante);

      // matricula

      $matricula->fecha_renovacion = $today->format('Y-m-d');
      $matricula->religion = is_null($request->input('religion')) ? $matricula->religion : $request->input('religion');
      $matricula->img_cedula_mama       =  $url_img_cedula_mama;
      $matricula->img_cedula_papa       =  $url_img_cedula_papa;
      $matricula->img_cedula_acudiente  =  $url_img_cedula_acudiente;
      $matricula->img_cedula_estudiante =  $url_img_cedula_estudiante;
      $matricula->notas                 =  $url_notas;
      $matricula->img_firma_acudiente   =  $url_img_firma_acudiente;
      $matricula->img_firma_familiar    =  $url_img_firma_familiar;
      $matricula->img_firma_estudiante  =  $url_img_firma_estudiante;
      $matricula->update();


      // estudiante
      $estudiante->p_nombre = is_null($request->input('p_nombre_estudiante')) ? $estudiante->p_nombre : $request->input('p_nombre_estudiante');
      $estudiante->s_nombre = is_null($request->input('s_nombre_estudiante')) ? $estudiante->s_nombre : $request->input('s_nombre_estudiante');
      $estudiante->p_apellido = is_null($request->input('p_apellido_estudiante')) ? $estudiante->p_apellido : $request->input('p_apellido_estudiante');
      $estudiante->s_apellido = is_null($request->input('s_apellido_estudiante')) ? $estudiante->s_apellido : $request->input('s_apellido_estudiante');
      $estudiante->fecha_nacimiento = is_null($request->input('fecha_nacimiento_estudiante')) ? $estudiante->fecha_nacimiento : $request->input('fecha_nacimiento_estudiante');
      $estudiante->dni = is_null($request->input('dni_estudiante')) ? $estudiante->dni : $request->input('dni_estudiante');
      $estudiante->direccion = is_null($request->input('direccion_estudiante')) ? $estudiante->direccion : $request->input('direccion_estudiante');
      $estudiante->update();

      // acudiente
      $acudiente->p_nombre = is_null($request->input('p_nombre_acudiente')) ? $acudiente->p_nombre : $request->input('p_nombre_acudiente');
      $acudiente->s_nombre = is_null($request->input('s_nombre_acudiente')) ? $acudiente->s_nombre : $request->input('s_nombre_acudiente');
      $acudiente->p_apellido = is_null($request->input('p_apellido_acudiente')) ? $acudiente->p_apellido : $request->input('p_apellido_acudiente');
      $acudiente->s_apellido = is_null($request->input('s_apellido_acudiente')) ? $acudiente->s_apellido : $request->input('s_apellido_acudiente');
      $acudiente->fecha_nacimiento = is_null($request->input('fecha_nacimiento_acudiente')) ? $acudiente->fecha_nacimiento : $request->input('fecha_nacimiento_acudiente');
      $acudiente->dni = is_null($request->input('dni_acudiente')) ? $acudiente->dni : $request->input('dni_acudiente');
      $acudiente->direccion = is_null($request->input('direccion_acudiente')) ? $acudiente->direccion : $request->input('direccion_acudiente');
      $acudiente->update();

      // informacion extra
      $extra_info->tipo                   = is_null($request->input('tipo')) ? $extra_info->tipo : $request->input('tipo');
      $extra_info->enfermedades_sufridas  = is_null($request->input('enfermedades_sufridas')) ? $extra_info->enfermedades_sufridas : $request->input('enfermedades_sufridas');
      $extra_info->sisben                 = is_null($request->input('sisben')) ? $extra_info->sisben : $request->input('sisben');
      $extra_info->estrato_socioeconimico = is_null($request->input('estrato_socioeconimico')) ? $extra_info->estrato_socioeconimico : $request->input('estrato_socioeconimico');
      $extra_info->nro_hermanos           = is_null($request->input('nro_hermanos')) ? $extra_info->nro_hermanos : $request->input('nro_hermanos');
      $extra_info->posicion_hermanos      = is_null($request->input('posicion_hermanos')) ? $extra_info->posicion_hermanos : $request->input('posicion_hermanos');
      $extra_info->procedencia            = is_null($request->input('procedencia')) ? $extra_info->procedencia : $request->input('procedencia');
      $extra_info->nuevo                  = is_null($request->input('nuevo')) ? $extra_info->nuevo : $request->input('nuevo');
      $extra_info->repitiente             = is_null($request->input('repitiente')) ? $extra_info->repitiente : $request->input('repitiente');
      $extra_info->fk_grado               = is_null($request->input('grado')) ? $extra_info->fk_grado : $request->input('grado');

      $extra_info->p_nombre_mama          = is_null($request->input('p_nombre_mama')) ? $extra_info->p_nombre_mama : $request->input('p_nombre_mama');
      $extra_info->s_nombre_mama          = is_null($request->input('s_nombre_mama')) ? $extra_info->s_nombre_mama : $request->input('s_nombre_mama');
      $extra_info->p_apellido_mama        = is_null($request->input('p_apellido_mama')) ? $extra_info->p_apellido_mama : $request->input('p_apellido_mama');
      $extra_info->s_apellido_mama        = is_null($request->input('s_apellido_mama')) ? $extra_info->s_apellido_mama : $request->input('s_apellido_mama');

      $extra_info->dni_madre              = is_null($request->input('dni_madre')) ? $extra_info->dni_madre : $request->input('dni_madre');

      $extra_info->p_nombre_padre         = is_null($request->input('p_nombre_padre')) ? $extra_info->p_nombre_padre : $request->input('p_nombre_padre');
      $extra_info->s_nombre_padre         = is_null($request->input('s_nombre_padre')) ? $extra_info->s_nombre_padre : $request->input('s_nombre_padre');
      $extra_info->p_apellido_padre       = is_null($request->input('p_apellido_padre')) ? $extra_info->p_apellido_padre : $request->input('p_apellido_padre');
      $extra_info->s_apellido_padre       = is_null($request->input('s_apellido_padre')) ? $extra_info->s_apellido_padre : $request->input('s_apellido_padre');

      $extra_info->dni_padre              = is_null($request->input('dni_padre')) ? $extra_info->dni_padre : $request->input('dni_padre');

      $extra_info->lugar_expedicion       = is_null($request->input('lugar_expedicion')) ? $extra_info->lugar_expedicion : $request->input('lugar_expedicion');
      $extra_info->lugar_nacimiento       = is_null($request->input('lugar_nacimiento')) ? $extra_info->lugar_nacimiento : $request->input('lugar_nacimiento');
      $extra_info->rh                     = is_null($request->input('rh')) ? $extra_info->rh : $request->input('rh');
      $extra_info->update();


      $estudiante_grado->fk_grado         = is_null($request->input('grado')) ? $estudiante_grado->fk_grado : $request->input('grado');
      $estudiante_grado->update();

      $data = response()->json(array(
        'status'        =>  'success',
        'code'          =>   200,
        'message'       =>  'Matricula actualizada correctamente',
        // 'asd'           =>  $estudiante_grado,
        // 'grado'         =>  $extra_info->fk_grado,
        // 'estudiante'    =>  $estudiante->id
      ), 200);
    } catch (\Throwable $e) {
      $data = response()->json(array(
        'status'    =>  'error',
        'code'      =>   404,
        'message'   =>  $e,
      ), 404);
    }
    return $data;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function cancelMatricula(Request $request, $id)
  {
    //$today = new DateTime();
    $matricula = Matriculas::find($id);
    $Matricula_Cancelada = "";

    $params_array = array(
      "motivo_cancelacion" => $request->input('motivo_cancelacion'),
      "destino_estudiante" => $request->input('destino_estudiante'),
      "fecha_cancelacion"  => $request->input('fecha_cancelacion'),
      "estatus"            => $request->input('estatus'),
      "fk_estudiante"      => $matricula->fk_estudiante,
      "fk_matricula"       => $id,
    );

    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // validamos los datos
      $validate = Validator::make($params_array, [
        "motivo_cancelacion"    => 'required|string',
        "destino_estudiante"    => 'required|string',
        "fecha_cancelacion"     => 'nullable|string',
        "fk_matricula"          => 'required|integer',
        "fk_estudiante"         => 'required|integer'
      ]);

      if (!$validate->fails()) {

        $matricula->estatus = $params->estatus;
        $matricula->update();

        if ($params->estatus === 'CANCELANDO' || $request->input('id_cancelacion') === null) {
          $Matricula_Cancelada = new Matriculas_Canceladas();
          $Matricula_Cancelada->motivo_cancelacion  = $params->motivo_cancelacion;
          $Matricula_Cancelada->destino_estudiante  = $params->destino_estudiante;
          $Matricula_Cancelada->fecha_cancelacion   = $params->fecha_cancelacion;
          $Matricula_Cancelada->fk_matricula        = $params->fk_matricula;
          $Matricula_Cancelada->fk_estudiante       = $params->fk_estudiante;
          $Matricula_Cancelada->save();
        } else {
          // update
          $matricula = Matriculas_Canceladas::where('id', $request->input('id_cancelacion'))->first();
          $matricula->fecha_cancelacion  = $params->fecha_cancelacion;
          $matricula->update();
        }

        $data = response()->json(array(
          'status'    =>  'success',
          'message'   =>  'Matricula cancelada correctamente',
          'data'      =>  $Matricula_Cancelada
        ), 200);
      } else {
        $data = array(
          'status'    => 'error',
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        );
      }
    }
    return $data;
  }
  public function updateStatus($id, $status)
  {
    try {
      $matricula = Matriculas::findOrFail($id);
      $matricula->estatus = $status;
      $matricula->update();
      $response = response()->json([
        'status' => 'Success',
        'message' => 'Updated!'
      ], 200);
    } catch (Exception $e) {
      $response = response()->json([
        'status' => 'Error',
        'message' => $e->getMessage()
      ], 500);
    }
    return $response;
  }
}
