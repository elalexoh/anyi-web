<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\Usuarios;
use App\Models\Correos_Electronicos;
use App\Models\Reset_Pass;
use Mail;
use DateTime;
use DateInterval;
use App\Mail\ResetPasswordReceived;
use Illuminate\Support\Facades\Validator;

class UsuarioController extends Controller
{

  public function login(Request $request)
  {

    $jwtAuth = new JwtAuth();

    // Recibir datos del post   
    $params_array = array(
      'user'      =>  $request->input('user'),
      'password'  =>  $request->input('password'),
      'gettoken'  =>  $request->input('gettoken')
    );
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // Validamos los datos
      $validate = Validator::make($params_array, [
        'user' => 'required',
        'password' => 'required'
      ]);

      if ($validate->fails()) {
        // Validaciones fallan
        $signup = array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        );
      } else {
        // Los datos fueron validados correctamente                
        $signup = $jwtAuth->signup($params->user, $params->password); // Token

        if (!empty($params->gettoken)) {
          $signup = $jwtAuth->signup($params->user, $params->password, true); // Datos decodificados
        }
      }
    } else {
      // Si los datos están vacíos.
      $signup = array(
        'status'    => 'error',
        'code'      => 404,
        'message'   => 'No se han recibido los datos',
        'datos'     => $request->input()
      );
    }

    // Devolvemos en json
    return response()->json($signup, 200);
  }

  public function update(Request $request)
  {
    $token = $request->header('Authorization');
    $jwtAuth = new \JwtAuth();
    $checkToken = $jwtAuth->checkToken($token);

    if ($checkToken) {
      echo "<h1>Login correcto</h1>";
    } else {
      echo "<h1>Login incorrecto</h1>";
    }

    die();
  }

  public function sendEmailToken(Request $request)
  {

    // Recibir datos del post   
    $params_array = array(
      'user'      =>  $request->input('user'),
      'email'     =>  strtolower($request->input('email'))
    );
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // Validamos los datos
      $validate = Validator::make($params_array, [
        'user' => 'required',
        'email' => 'required|email'
      ]);

      if ($validate->fails()) {
        // Validaciones fallan
        $data = array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        );
      } else {
        // Los datos fueron validados correctamente  

        $usuario = Usuarios::where('nombre_usuario', $params->user)->first();

        if (is_object($usuario)) {
          // Si el usuario existe
          $email = Correos_Electronicos::where('id', $usuario->fk_correo)->first();

          if ($email->direccion_correo == $params->email) {
            // Si el email ingresado coincide con los registros de la BD
            $today = new DateTime();


            // Validar si hay un token vigente (con plazo de 24 horas)
            // Si no está vigente, no se crea el registro en la tabla reset_pass

            //Crear registro en tabla RESET_PASS con un token
            $reset_pass = new Reset_Pass();
            $reset_pass->token_recuperacion = bin2hex(random_bytes(64));
            $reset_pass->fecha_solicitud = $today->format('Y-m-d H:i:s');
            $reset_pass->fk_usuario = $usuario->id;
            $reset_pass->save();


            //Enviar email con el token                       
            Mail::to($params->email)->send(new ResetPasswordReceived($usuario, $reset_pass));


            $data = array(
              'status'    => 'success',
              'code'      =>  200,
              'message'   => 'Email de restablecimiento de contraseña enviado satisfactoriamente',
              'datos'     => $request->input()
            );
          } else {
            $data = array(
              'status'    => 'error',
              'code'      =>  404,
              'message'   => 'Datos ingresados no son correctos'
            );
          }
        } else {

          $data = array(
            'status'    => 'error',
            'code'      =>  404,
            'message'   => 'Datos ingresados no son correctos'
          );
        }
      }
    } else {
      // Si los datos están vacíos.
      $data = array(
        'status'    => 'error',
        'code'      => 404,
        'message'   => 'No se han recibido los datos',
        'datos'     => $request->input()
      );
    }

    return response()->json($data, 200);
  }

  public function checkTokenReset(Request $request)
  {

    // Recibir datos del post   
    $params_array = array(
      'token'      =>  $request->input('token')
    );
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // Validamos los datos
      $validate = Validator::make($params_array, [
        'token' => 'required'
      ]);

      if ($validate->fails()) {
        // Validaciones fallan
        $data = array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        );
      } else {
        // Los datos fueron validados correctamente 

        $reset_pass = Reset_Pass::where('token_recuperacion', $params->token)->first();

        if (is_object($reset_pass)) {
          // Validaciones de vigencia del token
          $today = new DateTime();
          $today = $today->format('Y-m-d H:i:s');

          $fecha_token = new DateTime($reset_pass->fecha_solicitud);
          $fecha_token->add(new DateInterval('P01D')); // Le añadimos un día a la fecha de solicitud del token

          if ($today <= $fecha_token) {
            // El token está vigente
            $data = array(
              'status'    => 'success',
              'code'      => 200,
              'message'   => 'Token validado exitosamente.',
              'token'      => $params->token
            );
          } else {
            // El token está vencido
            $data = array(
              'status'    => 'error',
              'code'      => 400,
              'message'   => 'Token vencido, por favor solicite otro.',
              'data'      => $fecha_token
            );
          }
        } else {
          // Si el token no coincide.
          $data = array(
            'status'    => 'error',
            'code'      => 404,
            'message'   => 'Token no válido'
          );
        }
      }
    } else {
      // Si los datos están vacíos.
      $data = array(
        'status'    => 'error',
        'code'      => 404,
        'message'   => 'No se han recibido los datos',
        'datos'     => $request->input()
      );
    }

    return response()->json($data, 200);
  }
  //metodo para updatear la contraseña
  public function changePassReset(Request $request)
  {
    // Recibir datos del post   
    $params_array = array(
      'newPass'       =>  $request->input('new'),
      'confirmPass'   =>  $request->input('confirm'),
      'token_reset'   =>  $request->input('token_reset')
    );
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // Validamos los datos
      $validate = Validator::make($params_array, [
        'newPass' => 'required',
        'confirmPass' => 'required'
      ]);

      if ($validate->fails()) {
        // Validaciones fallan
        $data = array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        );
      } else {
        // Los datos fueron validados correctamente 
        $reset_pass = Reset_Pass::where('token_recuperacion', $params->token_reset)->first();

        if (is_object($reset_pass)) {

          if ($params->newPass == $params->confirmPass) {
            $usuario = Usuarios::find($reset_pass->fk_usuario);
            $usuario->clave_usuario = Hash::make($params->confirmPass);
            $usuario->update();

            $reset_pass->delete();

            $data = array(
              'status'    => 'success',
              'code'      => 200,
              'message'   => 'Clave de usuario actualizada satisfactoriamente'
            );
          } else {
            $data = array(
              'status'    => 'error',
              'code'      => 404,
              'message'   => 'Las contraseñas deben ser iguales'
            );
          }
        } else {
          $data = array(
            'status'    => 'error',
            'code'      => 404,
            'message'   => 'No se ha podido validar su identidad'
          );
        }
      }
    } else {
      // Si los datos están vacíos.
      $data = array(
        'status'    => 'error',
        'code'      => 404,
        'message'   => 'No se han recibido los datos',
        'datos'     => $request->input()
      );
    }

    return response()->json($data, 200);
  }
  //metodo para recuperar la contraseña
  public function changePass(Request $request)
  {

    $token = $request->header('Authorization');
    $jwtAuth = new \JwtAuth();
    $checkToken = $jwtAuth->checkToken($token);

    if ($checkToken) {

      $identity = $jwtAuth->checkToken($token, true);

      // Recibir datos del post   
      $params_array = array(
        'email'         => strtolower($request->input('email')),
        'oldPass'       => $request->input('oldPass'),
        'newPass'       => $request->input('newPass'),
        'confirmPass'   => $request->input('confirmPass')
      );
      $params = (object) $params_array;

      $usuario = Usuarios::find($identity->id_user);

      if (($params->email == $identity->email) && (Hash::check($params->oldPass, $usuario->clave_usuario))) {
        if ($params->newPass == $params->confirmPass) {

          $today = new DateTime();
          $usuario->clave_usuario         = Hash::make($params->newPass);
          $usuario->fecha_modificacion    = $today->format('Y-m-d H:i:s');
          $usuario->descripcion_modif     = 'CAMBIO DE CONTRASEÑA';
          $usuario->update();


          $data = array(
            'status'    => 'success',
            'code'      =>  200,
            'msg'       => 'Contraseña actualizada exitosamente',
            'params'    => $params->newPass
          );
        } else {
          $data = array(
            'status'    => 'error',
            'code'      =>  400,
            'msg'       => 'Las claves no coinciden'
          );
        }
      } else {
        $data = array(
          'status'    => 'error',
          'code'      =>  400,
          'msg'       => 'Datos ingresados no son correctos'
        );
      }
    } else {
      $data = array(
        'status'    => 'error',
        'code'      =>  400,
        'msg'       => 'Login incorrecto'
      );
    }


    return response()->json($data, 200);
  }

  public function verifyUser(Request $request)
  {
    $token = $request->header('Authorization');
    $jwtAuth = new \JwtAuth();
    $checkToken = $jwtAuth->checkToken($token);

    if ($checkToken) {
      $identity = $jwtAuth->checkToken($token, true);

      if ($identity->rol == $request->input('rol')) {
        $data = array(
          'status'    => 'success',
          'code'      => 200,
          'message'   => 'Usuario verificado satisfactoriamente'
        );
      } else {
        $data = array(
          'status'    => 'error',
          'code'      => 400,
          'message'   => 'Usuario no tiene los permisos indicados'
        );
      }
    } else {
      $data = array(
        'status'    => 'error',
        'code'      => 400,
        'message'   => 'Login incorrecto'
      );
    }

    return response()->json($data, $data['code']);
  }

  public function updatePoliticas(Request $request, $id)
  {
    $usuario = Usuarios::where('id', $id)->first();

    if (is_object($usuario)) {

      $usuario->politicas = true;
      $usuario->update();

      $data = response()->json(array(
        'status'    =>  'Success',
        'message'   =>  'Usuario actualizado correctamente'
      ), 200);
    } else {

      $data = response()->json(array(
        'status'    =>  'Error',
        'message'   =>  'Registro no encontrado'
      ), 404);
    }
    return $data;
  }
}
