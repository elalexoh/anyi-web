<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Certificados;
use App\Models\Personas;
use App\Models\Sedes;
use App\Models\Familiares;
use Hamcrest\Type\IsObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CertificadoController extends Controller
{
  public function indexCertificados()
  {
    $certificados = Certificados::where('tipo', '!=', 'notas_full')->get();

    if (count($certificados)) {
      foreach ($certificados as $item) {
        $persona_info = Personas::find($item->fk_persona);
        $familiar = Familiares::where('fk_estudiante', $item->fk_persona)->first();

        // Datos del docente
        $persona[] = [
          'id'                     =>  $item->id,
          'tipo'                   =>  $item->tipo,
          'descripcion'            =>  $item->descripcion,
          'estatus'                =>  $item->estatus,
          'p_nombre'               =>  $persona_info->p_nombre,
          'p_apellido'             =>  $persona_info->p_apellido,
          'dni'                    =>  $persona_info->dni,
          'id_sede'                =>  $persona_info->fk_sede,
          'familiar'               =>  $familiar
        ];
      }
      $data = response()->json(array(
        'status'    =>  'success',
        'data'      =>  $persona
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Not Resources',
        'data'   =>  'No se encontraron registros'
      ), 404);
    }
    return $data;
  }

  // obtener los certificados por el id de la sede
  public function getCertificados($id)
  {
    // $sede = Sedes::findOrFail($id);
    $sede = Sedes::where('id', $id)->first();
    if (is_object($sede)) {
      $personas = Personas::where('fk_sede', $sede->id)->get();
      $certificados = [];
      $data = '';

      if ($personas) {
        foreach ($personas as $item) {
          $certificado = Certificados::where('fk_persona', $item->id)->first();
          if (is_object($certificado)) {
            $certificados[] = array(
              "id"                     => $certificado->id,
              "tipo"                   => $certificado->tipo,
              "descripcion"            => $certificado->descripcion,
              // "url_certificado"        => $certificado->url_certificado,
              "estatus"                => $certificado->estatus,
              "p_nombre"               => $item->p_nombre,
              "p_apellido"             => $item->p_apellido,
              "dni"                    => $item->dni,
              'id_sede'                => $item->fk_sede
            );
          }
        }
        if ($certificados) {
          $data = response()->json(array(
            'status'    =>  'Success',
            'code'      =>   200,
            'data'      =>  $certificados
          ), 200);
        } else {
          $data = response()->json(array(
            'status'    =>  'Not found',
            'code'      =>   404,
            'data'      =>  'No hay certificados registrados para esta sede'
          ), 404);
        }
      } else {
        $data = response()->json(array(
          'status'    =>  'Not found',
          'code'      =>   404,
          'data'      =>  'No hay personas registradas para esta sede'
        ), 404);
      }
    } else {
      $data = response()->json(array(
        'status'    =>  'Not found',
        'code'      =>   404,
        'data'      =>  'Esta sede no existe'
      ), 404);
    }
    return $data;
  }

  public function getSolicitud($id)
  {
    $certificado = Certificados::find($id);
    try {
      $estudiante = Personas::find($certificado->fk_persona);
      $parentesco = Familiares::where('fk_estudiante', $certificado->fk_persona)->first();

      try {
        $acudiente = Personas::find($parentesco->fk_parentesco);

        $estudiante_array = [
          'p_nombre'               =>  $estudiante->p_nombre,
          's_nombre'               =>  $estudiante->s_nombre,
          'p_apellido'             =>  $estudiante->p_apellido,
          's_apellido'             =>  $estudiante->s_apellido,
          'dni'                    =>  $estudiante->dni,
          'fecha_nacimiento'       =>  $acudiente->fecha_nacimiento,
          'img_foto'               =>  $estudiante->img_foto
        ];
        $estudiante_object = (object) $estudiante_array;

        $acudiente_array = [
          'p_nombre'               =>  $acudiente->p_nombre,
          's_nombre'               =>  $acudiente->s_nombre,
          'p_apellido'             =>  $acudiente->p_apellido,
          's_apellido'             =>  $acudiente->s_apellido,
          'dni'                    =>  $acudiente->dni,
          'fecha_nacimiento'       =>  $acudiente->fecha_nacimiento,
          'img_foto'               =>  $acudiente->img_foto
        ];
        $acudiente_object = (object) $acudiente_array;

        $solicitud = [
          'estatus'                =>  $certificado->estatus,
          'nombre'                 =>  $certificado->nombre,
          'descripcion'            =>  $certificado->descripcion,
          'motivo'                 =>  $certificado->motivo,
          'url_certificado'        =>  $certificado->url_certificado,
          'estudiante'             =>  $estudiante_object,
          'acudiente'              =>  $acudiente_object,
        ];
        $data = response()->json(array(
          'status'    =>  'success',
          'code'      =>  200,
          'data'      =>  $solicitud
        ), 200);
      } catch (\Throwable $th) {
        $data = response()->json(array(
          'status'    =>  'error',
          'code'      =>   404,
          'message'   =>  'Acudientes no asignados al estudiante',
        ), 404);
      }
    } catch (\Throwable $th) {
      $data = response()->json(array(
        'status'    =>  'error',
        'code'      =>   404,
        'message'   =>  'Certificado no encontrado',
      ), 404);
    }

    return $data;
  }

  public function updateEstatusCertificado(Request $request, $id)
  {
    $certificado = Certificados::find($id);
    try {
      $certificado->estatus = is_null($request->input('status')) ? $certificado->estatus : $request->input('status');
      $certificado->motivo = is_null($request->input('motivo')) ? $certificado->motivo : $request->input('motivo');
      $certificado->update();

      $data = response()->json(array(
        'status'    =>  'success',
        'code'      =>   200,
        'message'   =>  'Certificado aprobado correctamente',
        'estatus'    =>  $certificado->estatus
      ), 200);
    } catch (\Throwable $th) {
      $data = response()->json(array(
        'status'    =>  'error',
        'code'      =>   404,
        'message'   =>  'Certificado no encontrado',
      ), 404);
    }

    return $data;
  }

  public function createCertificado(Request $request)
  {
    // return $request;
    $params_array = array(
      "tipo"              => $request->input('tipo'),
      "descripcion"       => $request->input('descripcion'),
      "fk_persona"        => $request->input('estudiante_selected.estudiante.id'), //persona === estudiante
      "fk_usuario"        => $request->input('id_usuario'), //persona que registra el certificado
    );

    // $params = (object) $params_array;

    if (!empty($params_array)) {
      // validamos los datos
      $validate = Validator::make($params_array, [
        "tipo"              => 'required',
        "descripcion"       => 'required',
        "fk_persona"        => 'required|integer',
      ]);
      if (!$validate->fails()) {
        $certificado = new Certificados();
        $certificado->tipo          = $request->input('tipo');
        $certificado->descripcion   = $request->input('descripcion');
        $certificado->estatus       = 'PENDIENTE';
        $certificado->fk_persona    = $request->input('estudiante_selected.estudiante.id');
        $certificado->fk_usuario    = $request->input('id_usuario');
        $certificado->save();

        $data = response()->json(array(
          'status'    => 'success',
          'code'      => 200,
          'message'   => 'Certificado registrado satisfactoriamente',
        ), 200);
      } else {
        $data = response()->json(array(
          'status'    =>  'error',
          'code'      =>   404,
          'message'   =>  'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        ), 404);
      }
    } else {
      $data = response()->json(array(
        'status'    =>  'error',
        'code'      =>   404,
        'message'   =>  'No se han recibido los datos'
      ), 404);
    }

    return $data;
  }
}
