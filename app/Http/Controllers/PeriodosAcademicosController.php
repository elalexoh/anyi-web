<?php

namespace App\Http\Controllers;

use App\Models\Periodos_Academicos;
use Illuminate\Http\Request;

class PeriodosAcademicosController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$periodos_academicos = Periodos_Academicos::all();
		if (is_object($periodos_academicos)) {
			$data = response()->json(array(
				'status'    =>  'Success',
				'data'      =>  $periodos_academicos
			), 200);
		} else {
			$data = response()->json(array(
				'status'    =>  'Resources not found',
				'message'    =>  'No hay registros.',
			), 404);
		}
		return $data;
	}
	public function update(Request $request)
	{
		$periodo_academico = Periodos_Academicos::find($request->input('id'));
		if (is_object($periodo_academico)) {
			$periodo_academico->fecha_inicio = is_null($request->input('fecha_inicio')) ? $periodo_academico->fecha_inicio : $request->input('fecha_inicio');
			$periodo_academico->fecha_fin = is_null($request->input('fecha_fin')) ? $periodo_academico->p_nombre : $request->input('fecha_fin');
			$periodo_academico->update();

			$data = response()->json($periodo_academico, 200);
		} else {
			$data = response()->json(array(
				'status'    	 =>  'Resource not found',
				'message'      =>  'No hemos encontrado el periodo academico'
			), 404);
		}
		return $data;
	}
}
