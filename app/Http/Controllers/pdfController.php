<?php

namespace App\Http\Controllers;

use App\Estudiantes_Grados;
use App\Models\Centros_Educativos;
use App\Models\Familiares;
use App\Models\Grados;
use App\Models\Informaciones_Adicionales;
use App\Models\Matriculas;
use App\Models\Matriculas_Canceladas;
use App\Models\Personas;
use App\Models\Sedes;
use App\Models\Certificados;
use App\Models\Estudiantes_Areas;
use App\Models\Periodos_Academicos;
use App\Models\Areas;
use DateTime;

class pdfController extends Controller
{
  public function downloadMatricula($id)
  {
    $pdf = app('dompdf.wrapper');
    $matricula = Matriculas::find($id);
    $html = '';

    if (is_object($matricula)) {
      $estudiante = $matricula->personas;
      // informacion extra
      $informaciones_adicionales = Informaciones_Adicionales::where('fk_matricula', $id)->first();

      // acudiente info
      $familiar = Familiares::where('fk_estudiante', $estudiante->id)->first();
      $acudiente = Personas::where('id', $familiar->fk_parentesco)->first();
      $sede = Sedes::where('id', $acudiente->fk_sede)->first();
      $centro = Centros_Educativos::where('identificacion', $sede->fk_centro_educativo)->first();

      // cumpleaños
      $bday = new DateTime($estudiante->fecha_nacimiento); // Your date of birth
      $today = new Datetime(date('m.d.y'));
      $diff = $today->diff($bday);

      $religion = $matricula->religion != 0  ? 'Si' : 'No';
      // cancelada
      $cancelada = $matricula->estatus == 'CANCELADA' ? true : false;

      // $imagedata = file_get_contents($centro->img_logo);
      // $base64 = base64_encode($imagedata);

      $htmlCancel = '';
      if ($cancelada) {
        $matricula_cacelada = Matriculas_Canceladas::where('fk_matricula', $matricula->id)->first();
        $htmlCancel = '
                  <h3 class="text-center">Cancelación</h3>
                  <div class="cancel-block mt">
                      <p>
                          Fecha: <span class="underline">' . $matricula_cacelada->fecha_cancelacion . '</span>
                          motivo: <span class="underline">' . $matricula_cacelada->motivo_cancelacion . '</span>
                          Destino: <span class="underline">' . $matricula_cacelada->destino_estudiante . '</span>
                      </p>
                  </div>
              ';
      }
      $html = '
                    <div class="content">
                      <div class="header">
                          <div class="logo">
                          </div>
                          <div class="institute-info">
                              <div class="name text-uppercase">Centro Educativo ' . $centro->nombre . '</div>
                              <div class="date">Resolución ' . $centro->resolucion . '</div>
                              <div class="dane">Dane ' . $sede->dane . '</div>
                              <div class="nit">NIT ' . $centro->nit . '</div>
                          </div>
                      </div>
                      <div class="text-center subheader">
                          <p>Ficha unica de matricula o renovación</p>
                          <p>
                              Sede: <span>' . $sede->nombre . '</span>
                          </p>
                      </div>
                      <div class="body">
                          <p>
                          Nombres y apellidos: <span class="underline">
                          ' . $estudiante->p_nombre . ' ' . $estudiante->s_nombre . ', ' . $estudiante->p_apellido . ' ' . $estudiante->s_apellido . '
                          </span>
                          Fecha de nacimiento <span class="underline">' . $estudiante->fecha_nacimiento . '</span>
                          lugar: <span class="underline"> ' . $estudiante->direccion . ' </span>
                          Tipo de documento <span class="underline">Matricula</span>
                          N° de documento: <span class="underline">' . $matricula->id . '</span>
                          lugar de expedición: <span class="underline">' . $centro->direccion . '</span>
                          Edad: <span class="underline">' . $diff->y . ' años</span>
                          Sexo: <span class="underline">' . $estudiante->sexo . '</span>
                          Rh: <span class="underline">por especificar</span>
                          Direccion: <span class="underline"> ' . $estudiante->direccion . ' </span>
                          Nombre del acudiente <span class="underline">
                          ' . $acudiente->p_nombre . ' ' . $acudiente->s_nombre . ', ' . $acudiente->p_apellido . ' ' . $acudiente->s_apellido . '
                          </span>
                          cc: <span class="underline">Contenido</span>
                          Número de hermanos: <span class="underline">' . $informaciones_adicionales->nro_hermanos . '</span>
                          Lugar que ocupa: <span class="underline">' . $informaciones_adicionales->posicion_hermanos . '</span>
                          Enfermedades sufridas: <span class="underline">' . $informaciones_adicionales->enfermedades_sufridas . '</span>
                          Sisben <span class="underline">' . $informaciones_adicionales->sisben . '</span>
                          Estrato socioeconomico: <span class="underline">' . $informaciones_adicionales->estrato_socioeconimico . '</span>
                          Plantel de procedencia: <span class="underline">' . $informaciones_adicionales->procedencia . '</span>
                          Nuevo <span class="underline">' . $informaciones_adicionales->nuevo . '</span>
                          <span style="display:none;"> repitiente: <span class="underline">Contenido</span> </span>
                          </p>
                          <p>
                          ¿Está deacuerdo que su hijo(a) reciba la catedra de religion que ofrece el centro educativo? <span class="underline">' . $religion  . '</span>
                          </p>
                      </div>
                      <table>
                          <tr>
                              <th>Año</th>
                              <th>Fecha de matricula</th>
                              <th>Grado</th>
                              <th>Edad</th>
                              <th>Firma del acudiente</th>
                              <th>Firma del estudiante</th>
                          </tr>
                          <tr>
                              <td>Alfreds Futterkiste</td>
                              <td>' . $matricula->fecha_matricula . '</td>
                              <td>' . $informaciones_adicionales->grado . '</td>
                              <td>' . $diff->y . ' años</td>
                              <td></td>
                              <td></td>
                          </tr>
                      </table>
                      ' . $htmlCancel . '

                      <div class="firms">
                          <p>Padre de familia y/o acudiente:</p>
                          <p>Estudiante :</p>
                          <p>Docente :</p>
                          <p>Director :</p>
                      </div>
                    </div>
                    <style>
                        .text-uppercase{
                            text-transform: uppercase;
                        }
                        .header{
                            background-color: #bada55;
                            display: flex;
                        }
                        .institute-info{
                            text-align: center;
                        }
                        .text-center{
                            text-align: center;
                        }
                        .logo{
                            background-color: #000;
                            width: 150px;
                            height: auto;
                        }
                        .underline{
                            text-decoration: underline;
                            font-weight: bold;
                        }
                        table {
                            font-family: arial, sans-serif;
                            border-collapse: collapse;
                            width: 100%;
                        }

                        td, th {
                            border: 1px solid #dddddd;
                            text-align: left;
                            padding: 8px;
                        }

                        tr:nth-child(even) {
                            background-color: #dddddd;
                        }
                        .mt{
                            margin-top: 1rem;
                        }
                        .firma-img{
                            heigh: 100px;
                            width:auto
                        }
                    </style>
                  ';
    } else {
      $html = 'La matricula no esta registrada en la plataforma';
    }



    $pdf->loadHTML($html);

    return $pdf->stream('mi-archivo.pdf');
    // return $direccion_info;
  }

  public function getMatricula($id)
  {
    // matricula
    $matricula              = Matriculas::find($id);
    $matricula_cacelada     = '';

    if (is_object($matricula)) {
      $estudiante = $matricula->personas;
      // Grado
      $grado_estudiante = Estudiantes_Grados::where('fk_estudiante', $estudiante->id)->first();
      $grado = Grados::where('id', $grado_estudiante->fk_grado)->first();

      // informacion extra
      $informaciones_adicionales = Informaciones_Adicionales::where('fk_matricula', $id)->first();

      // acudiente info
      $familiar = Familiares::where('fk_estudiante', $estudiante->id)->first();
      $acudiente = Personas::where('id', $familiar->fk_parentesco)->first();
      $sede = Sedes::where('id', $acudiente->fk_sede)->first();
      $centro = Centros_Educativos::where('identificacion', $sede->fk_centro_educativo)->first();

      // cumpleaños
      $bday = new DateTime($estudiante->fecha_nacimiento); // Your date of birth
      $today = new Datetime(date('m.d.y'));
      $estudiante_edad = $today->diff($bday);

      $religion = $matricula->religion != 0  ? 'Si' : 'No';

      // cancelada
      $cancelada = $matricula->estatus == 'CANCELADO' ? true : false;

      if ($cancelada) {
        $matricula_cacelada = Matriculas_Canceladas::where('fk_matricula', $matricula->id)->first();
      } else {
        $matricula_cacelada = false;
      }

      $info = array(
        'matricula_id'                  => $matricula->id,
        'matricula_fecha_matricula'     => $matricula->fecha_matricula,
        'firma_acudiente'               => $matricula->img_firma_acudiente,
        'firma_estudiante'              => $matricula->img_firma_estudiante,
        'religion'                      => $religion,

        // centro
        'centro_img_logo'               => $centro->img_logo,
        'centro_img_firma'              => $centro->img_firma,
        'centro_nombre'                 => $centro->nombre,
        'centro_resolucion'             => $centro->resolucion,
        'centro_nit'                    => $centro->nit,
        'centro_direccion'              => $centro->direccion,

        // sede
        'sede_dane'                     => $sede->dane,
        'sede_nombre'                   => $sede->nombre,

        // estudiante
        'estudiante_p_nombre'           => $estudiante->p_nombre,
        'estudiante_s_nombre'           => $estudiante->s_nombre,
        'estudiante_p_apellido'         => $estudiante->p_apellido,
        'estudiante_s_apellido'         => $estudiante->s_apellido,
        'estudiante_fecha_nacimiento'   => $estudiante->fecha_nacimiento,
        'estudiante_direccion'          => $estudiante->direccion,
        'estudiante_sexo'               => $estudiante->sexo,
        'estudiante_direccion'          => $estudiante->direccion,
        'estudiante_edad'               => $estudiante_edad->y,
        'grado'                         => $grado->nombre,

        // acudiente
        'acudiente_p_nombre'            => $acudiente->p_nombre,
        'acudiente_s_nombre'            => $acudiente->s_nombre,
        'acudiente_p_apellido'          => $acudiente->p_apellido,
        'acudiente_s_apellido'          => $acudiente->s_apellido,

        // extra_info
        'informaciones_adicionales_nro_hermanos'                => $informaciones_adicionales->nro_hermanos,
        'informaciones_adicionales_posicion_hermanos'           => $informaciones_adicionales->posicion_hermanos,
        'informaciones_adicionales_enfermedades_sufridas'       => $informaciones_adicionales->enfermedades_sufridas,
        'informaciones_adicionales_sisben'                      => $informaciones_adicionales->sisben,
        'informaciones_adicionales_estrato_socioeconimico'      => $informaciones_adicionales->estrato_socioeconimico,
        'informaciones_adicionales_procedencia'                 => $informaciones_adicionales->procedencia,
        'informaciones_adicionales_nuevo'                       => $informaciones_adicionales->nuevo,
        'informaciones_adicionales_grado'                       => $informaciones_adicionales->grado,
        'rh'                                                    => $informaciones_adicionales->rh,
        'repitiente'                                            => $informaciones_adicionales->repitiente,

        //matricula cancelada
        'cancelacion'                                           => $matricula_cacelada,
      );
      $info_obj = (object)$info;
      $data = response()->json(array(
        'status'    =>  'Success',
        'data'      =>  $info_obj,
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Resources not found',
        'message'    =>  'No hay registros.',
      ), 404);
    }
    return $data;
  }

  public function getCertificado($id)
  {
    $areas = [];
    $certificado = Certificados::find($id);
    if (is_object($certificado)) {
      $estudiante = $certificado->personas;
      $notas_areas = Estudiantes_Areas::where('fk_estudiante', $estudiante->id)->get();
      // $areas = Areas::where('id', $notas_areas->fk_areas)->firts();
      $grado_estudiante = Estudiantes_Grados::where('fk_estudiante', $estudiante->id)->first();
      $grado = Grados::where('id', $grado_estudiante->fk_grado)->first();
      $periodo = Periodos_Academicos::where('id', $grado->fk_periodo_academico)->first();
      $familiar = Familiares::where('fk_estudiante', $estudiante->id)->first();
      $acudiente = Personas::where('id', $familiar->fk_parentesco)->first();
      $sede = Sedes::where('id', $acudiente->fk_sede)->first();
      $centro = Centros_Educativos::where('identificacion', $sede->fk_centro_educativo)->first();
      $sede_estudiante = Sedes::where('id', $estudiante->fk_sede)->first();
      foreach ($notas_areas as $area) {
        $area_data = Areas::where('id', $area->fk_areas)->first();
        $areas[] = [
          'nombre' => $area_data->nombre,
          'intensidad' => $area_data->intensidad,
          'id' => $area_data->id,
          'nota' => $area->nota_parcial
        ];
      }
      // faltan
      // cargo
      // DNI estudiante

      $info = array(
        'certificado_id'                => $certificado->id,
        'firmado'                       => $certificado->firmado,

        // centro
        'centro_img_logo'               => $centro->img_logo,
        'centro_nombre'                 => $centro->nombre,
        'centro_resolucion'             => $centro->resolucion,
        'centro_nit'                    => $centro->nit,
        'centro_direccion'              => $centro->direccion,
        'centro_dane'                   => $centro->dane,
        'centro_firma'                  => $centro->img_firma,
        'cargo'                         => $centro->cargo,

        // sede
        'sede_dane'                     => $sede_estudiante->dane,
        'sede_nombre'                   => $sede_estudiante->nombre,

        //datos del director (responsable de sede?)
        'nombre_autorizado'             => $centro->nombre_autorizado,
        'dni_director'                  => $centro->dni_director,
        'lugar_expedicion'              => $centro->lugar_expedicion,

        //datos del certificado
        'certificado_tipo'              => $certificado->tipo,

        // estudiante
        'estudiante_p_nombre'           => $estudiante->p_nombre,
        'estudiante_s_nombre'           => $estudiante->s_nombre,
        'estudiante_p_apellido'         => $estudiante->p_apellido,
        'estudiante_s_apellido'         => $estudiante->s_apellido,
        'estudiante_fecha_nacimiento'   => $estudiante->fecha_nacimiento,
        'estudiante_direccion'          => $estudiante->direccion,
        'estudiante_sexo'               => $estudiante->sexo,
        'estudiante_dni'                => $estudiante->dni,

        //datos del grado y periodo academico
        'grado'                         => $grado->nombre,
        'periodo_fecha_inicio'          => $periodo->fecha_inicio,

        //areas
        'notas_areas'          => $areas,
      );
      $info_obj = (object)$info;
      $data = response()->json(array(
        'status'    =>  'Success',
        'data'      =>  $info_obj,
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Resources not found',
        'message'    =>  'No hay registros.',
      ), 404);
    }
    return $data;

    // return $estudiante;
  }

  public function getCertificadoFirmed($id, $isFirmed)
  {
    $certificado = Certificados::find($id);
    $certificado->firmado = $isFirmed == "true";
    $certificado->update();
    return $this->getCertificado($id);
  }
}
