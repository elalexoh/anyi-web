<?php

namespace App\Http\Controllers;

use App\Models\Areas;
use App\Models\Centros_Educativos;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Roles;
use App\Models\Usuarios;
use App\Models\Personas;
use App\Models\Correos_Electronicos;
use App\Models\Direcciones;
use App\Models\Estudiantes_Areas;
use App\Models\Familiares;
use App\Models\Grados;
use App\Models\Informaciones_Adicionales;
use App\Models\Matriculas;
use App\Models\Periodos_Academicos;
use App\Models\Sedes;
use App\Models\Telefonos;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DocenteAdministrativoController extends Controller
{
  // crear usuario
  public function createUsuario(Request $request)
  {
    $params_array = array(
      //'id_rol'         =>  $request->input('id_rol'),
      'p_nombre'       =>  $request->input('p_nombre'),
      'id_sede'        =>  $request->input('id_sede'),
      's_nombre'       =>  $request->input('s_nombre'),
      'p_apellido'     =>  $request->input('p_apellido'),
      's_apellido'     =>  $request->input('s_apellido'),
      'dni'            =>  $request->input('dni'),
      'phoneNumber'    =>  $request->input('phoneNumber'),
      'estado'         =>  $request->input('estado'),
      'email'          =>  strtolower($request->input('email')),
      'username'       =>  $request->input('username'),
      'password'       =>  Hash::make($request->input('password')),
      'descripcion'    =>  $request->input('descripcion'),
      'fechaNac'       =>  $request->input('fechaNac'),
      'codArea'        =>  $request->input('codArea'),
      'id_lugar'       =>  $request->input('id_lugar'),
      'id_rol'         =>  $request->input('id_rol'),
      'direccion'      =>  $request->input('direccion'),
      'img_foto'       =>  $request->file('img_foto'),
      'sexo'           =>  $request->input('sexo'),
    );

    // transformando a objeto
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // validamos los datos
      $validate = Validator::make($params_array, [
        'id_sede'     => 'required|integer',
        'id_rol'      => 'required|integer',
        'p_nombre'    => 'required|alpha',
        's_nombre'    => 'nullable|alpha',
        'p_apellido'  => 'required|alpha',
        's_apellido'  => 'nullable|alpha',
        'dni'         => 'required|unique:App\Models\Personas,dni',
        'fechaNac'    => 'required',
        'codArea'     => 'required',
        'phoneNumber' => 'required',
        'email'       => 'required|email|unique:App\Models\Correos_Electronicos,direccion_correo',
        'estado'      => 'required',
        'username'    => 'required|unique:App\Models\Usuarios,nombre_usuario',
        'password'    => 'required',
        'direccion'   => 'required',
        'img_foto'    => 'nullable|mimes:jpg,bmp,png'
      ]);

      if (!$validate->fails()) {

        // rol usuario

        $rol = Roles::find($params->id_rol);
        $url_img_foto = null;
        if ($request->file('img_foto')) {
          $img_foto        = $request->file('img_foto')->store('usuarios/fotos/', 'public');
          $url_img_foto    = asset('storage/' . '' . $img_foto);
        }


        $persona = new Personas();
        $persona->dni               = $params->dni;
        $persona->tipo              = $rol->nombre;
        $persona->p_nombre          = $params->p_nombre;
        $persona->s_nombre          = $params->s_nombre;
        $persona->p_apellido        = $params->p_apellido;
        $persona->s_apellido        = $params->s_apellido;
        $persona->fecha_nacimiento  = $params->fechaNac;
        $persona->direccion         = $params->direccion;
        $persona->fk_sede           = $params->id_sede;
        $persona->img_foto          = $url_img_foto;
        $persona->sexo              = $params->sexo;
        $persona->save();

        $telefono = new Telefonos();
        $telefono->cod_area     = $params->codArea;
        $telefono->numero_telf  = $params->phoneNumber;
        $telefono->fk_persona   = $persona->id;
        $telefono->save();

        $correo = new Correos_Electronicos();
        $correo->direccion_correo   = $params->email;
        $correo->fk_persona         = $persona->id;
        $correo->save();

        $today = new DateTime();

        $usuario = new Usuarios();
        $usuario->nombre_usuario    = $params->username;
        $usuario->clave_usuario     = $params->password;
        $usuario->nombre_usuario    = $params->username;
        $usuario->fecha_registro    = $today->format('Y-m-d H:i:s');
        $usuario->politicas         = false;
        $usuario->estado            = $params->estado;
        $usuario->fk_rol            = $rol->id;
        $usuario->fk_correo         = $correo->id;
        $usuario->save();

        $data = response()->json(array(
          'status'    => 'success',
          'code'      => 200,
          'message'   => 'Usuario registrado satisfactoriamente',
        ), 200);
      } else {
        // Validaciones fallan
        $data = response()->json(array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        ), 404);
      }
    } else {
      // Request vacio
      $data = response()->json(array(
        'status'    => 'error',
        'code'      => 404,
        'message'   => 'No se han recibido los datos',
        'datos'     => $request->input()
      ), 404);
    }
    return $data;
  }
  // actualizar Usuario
  public function updateUsuario(Request $request, $id)
  {
    $today = new DateTime();
    $usuario = Usuarios::find($id);
    $rol = Roles::find($usuario->fk_rol);

    // return $request;

    if (($rol->nombre == 'DOCENTE') || $rol->nombre == 'ACUDIENTE') {
      try {
        $usuario->nombre_usuario        = is_null($request->input('nombre_usuario')) ? $usuario->nombre_usuario : $request->input('nombre_usuario');
        $usuario->estado                = is_null($request->input('estado')) ? $usuario->estado : $request->input('estado');
        $usuario->fecha_modificacion    = $today->format('Y-m-d H:i:s');
        $usuario->clave_usuario         = is_null($request->input('clave_usuario')) ? $usuario->clave_usuario : Hash::make($request->input('clave_usuario'));

        $usuario->update();

        $correo = Correos_Electronicos::find($usuario->fk_correo);
        $correo->direccion_correo   = is_null($request->input('direccion_correo')) ? $correo->direccion_correo : $request->input('direccion_correo');
        $correo->update();

        $persona = Personas::find($correo->fk_persona);

        //guardar foto
        $img_foto        = is_null($request->file('img_foto')) ? $persona->img_foto : $request->file('img_foto')->store('usuarios/fotos/', 'public');

        //obtener url de foto
        $url_img_foto    = is_null($request->file('img_foto')) ? $persona->img_foto : asset('storage/' . '' . $img_foto);

        $persona->p_nombre          = is_null($request->input('p_nombre')) ? $persona->p_nombre : $request->input('p_nombre');
        $persona->s_nombre          = is_null($request->input('s_nombre')) ? $persona->s_nombre : $request->input('s_nombre');
        $persona->p_apellido        = is_null($request->input('p_apellido')) ? $persona->p_apellido : $request->input('p_apellido');
        $persona->s_apellido        = is_null($request->input('s_apellido')) ? $persona->s_apellido : $request->input('s_apellido');
        $persona->dni               = is_null($request->input('dni')) ? $persona->dni : $request->input('dni');
        $persona->fecha_nacimiento  = is_null($request->input('fecha_nacimiento')) ? $persona->fecha_nacimiento : $request->input('fecha_nacimiento');
        $persona->fk_sede           = is_null($request->input('id_sede')) ? $persona->fk_sede : $request->input('id_sede');
        $persona->direccion          = is_null($request->input('direccion')) ? $persona->direccion : $request->input('direccion');
        $persona->img_foto          = $url_img_foto;
        $persona->update();

        $telefono = Telefonos::where('fk_persona', $persona->id)->first();

        if (is_object($telefono)) {
          $telefono->cod_area     = is_null($request->input('cod_area')) ? $telefono->cod_area : $request->input('cod_area');
          $telefono->numero_telf     = is_null($request->input('numero_telf')) ? $telefono->numero_telf : $request->input('numero_telf');
          $telefono->update();
        } else {
          $telefono               = new Telefonos();
          $telefono->cod_area     = $request->input('cod_area');
          $telefono->numero_telf  = $request->input('numero_telf');
          $telefono->fk_persona   = $persona->id;
          $telefono->save();
        }

        $data = response()->json(array(
          'status'    =>  'success',
          'code'      =>   200,
          'message'   =>  'Usuario actualizado correctamente'
        ), 200);
      } catch (\Throwable $th) {
        $data = response()->json(array(
          'status'    =>  'error',
          'code'      =>   404,
          'message'   =>  'Usuario no encontrado',
        ), 404);
      }
    } else {
      $data = response()->json(array(
        'status'    =>  'error',
        'code'      =>   404,
        'message'   =>  'No tienes permisos para actualizar este usuario',
      ), 404);
    }

    return $data;
  }

  // get usuario
  public function getUsuario($id)
  {
    $persona = Personas::where('id', $id)->first();

    // verificando si existe el usuario
    if (is_object($persona)) {
      $correo = Correos_Electronicos::where('fk_persona', $persona->id)->first();
      $usuario = Usuarios::where('fk_correo', $correo->id)->first();
      $telefono = Telefonos::where('fk_persona', $persona->id)->first();

      // informacion del usuario
      $data = array(
        'nombre_usuario'              => $usuario->nombre_usuario,
        'estado'                      => $usuario->estado,
        'fecha_modificacion'          => $usuario->fecha_modificacion,
        'direccion_correo'            => strtolower($correo->direccion_correo),
        'p_nombre'                    => $persona->p_nombre,
        's_nombre'                    => $persona->s_nombre,
        'p_apellido'                  => $persona->p_apellido,
        's_apellido'                  => $persona->s_apellido,
        'dni'                         => $persona->dni,
        'fecha_nacimiento'            => $persona->fecha_nacimiento,
        'cod_area'                    => isset($telefono->cod_area) ? $telefono->cod_area : '',
        'numero_telf'                 => isset($telefono->numero_telf) ? $telefono->numero_telf : '',
        'id_rol'                      => $usuario->roles->id,
        'rol'                         => $usuario->roles->nombre,
        'sede'                        => $persona->sedes->nombre,
        'id_sede'                     => $persona->sedes->id,
        'direccion'                   => $persona->direccion,
        'img_foto'                    => $persona->img_foto,
        'sexo'                        => $persona->sexo,
        'img_foto'                    => $persona->img_foto,
        'user_id'                     => $usuario->id,
      );
      $response = response()->json(array(
        'status'    => 'Sucess',
        'code'      =>  200,
        'message'   => 'Exito, hemos encontrado al usuario',
        'data'      => $data
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Not Found',
        'code'      =>  404,
        'message'   => 'No hemos encontrado el usuario',
      ), 404);
    }

    return $response;
  }

  // get all usuarios
  public function indexUsuariosAll()
  {
    $usuarios = Usuarios::all();
    $user_persons = [];

    if (!!count($usuarios)) {
      foreach ($usuarios as $item) {
        $user_persons[] = [
          'usuario' => $item,
          'persona' => Correos_Electronicos::where('id', $item->fk_correo)->first()->personas,
        ];
      }
      $response = response()->json(array(
        'status'    => 'success',
        'code'      =>  200,
        'data'      => $user_persons
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Resources not found',
        'code'      =>  404,
        'message'   => 'no se han registrado usuarios en el sistema'
      ), 404);
    }
    return $response;
  }
  //listar usuario
  public function indexUsuarios()
  {
    $rolUsuarioAcudiente = Roles::where('nombre', 'ACUDIENTE')->first();
    $rolUsuarioDocente = Roles::where('nombre', 'DOCENTE')->first();
    return $rolUsuarioDocente;

    //! --FALTA-- usar token para filtrar por sede
    $usuarios = Usuarios::where('fk_rol', $rolUsuarioAcudiente->id)->orWhere('fk_rol', $rolUsuarioDocente->id)->get();

    if (!!count($usuarios)) {
      foreach ($usuarios as $usuario) {
        $email = Correos_Electronicos::find($usuario->fk_correo);
        $persona = Personas::find($email->fk_persona);
        $sede = Sedes::find($persona->fk_sede);
        $rol = Roles::where('id', $usuario->fk_rol)->first();

        // Datos del usuario
        $usuarios_arr[] = [
          'id_user'           =>  $usuario->id,
          'id'                =>  $usuario->id,
          'usuario'           =>  $usuario->nombre_usuario,
          'status'            =>  $usuario->estado,
          'fecha_registro'    =>  $usuario->fecha_registro,
          'email'             =>  $email->direccion_correo,
          'p_nombre'          =>  $persona->p_nombre,
          'p_apellido'        =>  $persona->p_apellido,
          'dni'               =>  $persona->dni,
          'sede'              =>  $sede->nombre,
          'rol'               =>  $rol->nombre,
          'rol_id'            =>  $rol->id,
          'clave_usuario'     =>  $usuario->clave_usuario
        ];
      }
      $response = response()->json($usuarios_arr, 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Not found',
        'code'      =>  404,
        'message'   =>  'No hay usuarios registrados en la plataforma'
      ), 404);
    }
    return $usuarios;
  }

  //borrar usuario
  public function deleteUsuario($id)
  {

    try {
      DB::beginTransaction();
      $persona = Personas::findOrFail($id);
      $correo = Correos_Electronicos::where('fk_persona', $persona->id)->firstOrFail();
      $usuario = Usuarios::where('fk_correo', $correo->id)->firstOrFail();
      $telefono = Telefonos::where('fk_persona', $persona->id)->firstOrFail();

      $usuario->delete();
      $correo->delete();
      $persona->delete();
      $telefono->delete();

      DB::commit();
      $response = response()->json(array(
        'status'        => 'Success',
        'message'       => 'Usuario Eliminado satisfactoriamente',
      ), 200);
    } catch (Exception $ex) {
      DB::rollback();
      $response = response()->json(array(
        'status'        => 'Error',
        'message'       => $ex->getMessage(),
      ), 500);
    }
    return $response;
  }

  public function indexDocentes(Request $request)
  {

    $token = $request->header('Authorization');
    $jwtAuth = new \JwtAuth();
    $checkToken = $jwtAuth->checkToken($token);

    if ($checkToken) {
      $identity = $jwtAuth->checkToken($token, true);
      $user = Personas::find($identity->id_user);
      $docentes_info = [];

      if ($identity->rol == 'DOCENTE ADMINISTRATIVO' || $identity->rol == 'DOCENTE') {
        $docentes = Usuarios::where('fk_rol', 3)->get();
        if (!!count($docentes)) {
          foreach ($docentes as $docente) {
            $docentes_info[] = [
              'docente' => $docente->correos_electronicos->personas
            ];
          }
          $data = response()->json(array(
            'status'    =>  'Success',
            'data'    =>  $docentes_info,
          ), 200);
        } else {
          $data = response()->json(array(
            'status'    =>  'Resources not found',
            'message'    =>  'No se han encontrado registros',
          ), 400);
        }
      } else {
        $data = response()->json(array(
          'status'    =>  'Forbiden Access',
          'message'    =>  'No tiene los permisos para ver esta información',
        ), 404);
      }
    } else {
      $data = response()->json(array(
        'status'    =>  'Forbiden Access',
        'message'    =>  'Debe estar logeado para ver esta información',
      ), 404);
    }

    return $data;
  }

  public function indexAcudientes()
  {
    $acudientes = Usuarios::where('fk_rol', 4)->get();
    $acudientes_full = [];


    if (!!count($acudientes)) {
      foreach ($acudientes as $acudiente) {
        $correo = Correos_Electronicos::where('id', $acudiente->fk_correo)->first();
        $persona = Personas::where('id', $correo->fk_persona)->first();
        $acudientes_full[] = [
          'user' => $acudiente,
          'persona' => $persona,
        ];
      }
      $response = response()->json(array(
        'status'    => 'Success',
        'data'   => $acudientes_full
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Not Resources',
        'message'   => 'No se han encontrado registros en la plataforma',
      ), 404);
    }
    return $response;
  }

  public function getResponsableBySede($id)
  {
    $sede = Sedes::find($id);

    try {
      $persona = Personas::where('tipo', 'RESPONSABLE')->where('fk_sede', $sede->id)->first();

      $responsable_array = [
        'p_nombre'               =>  $persona->p_nombre,
        'p_apellido'             =>  $persona->p_apellido,
      ];
      $responsable_object = (object) $responsable_array;

      $data = response()->json(array(
        'status'    =>  'success',
        'code'      =>   200,
        'message'   =>  'Responsable de sede encontrado',
        'data'      =>  $responsable_object
      ), 200);
    } catch (\Throwable $th) {
      $data = response()->json(array(
        'status'    =>  'Not Found',
        'code'      =>   404,
        'message'   =>  'Sede sin responsable',
      ), 404);
    }

    return $data;
  }

  public function indexEstudiantes()
  {
    $familiares = Familiares::all();
    $estudiantes = [];
    $areas = [];
    if ($familiares) {
      foreach ($familiares as $estudiante) {
        $estudiante_data = Personas::where('id', $estudiante->fk_estudiante)->first();
        $acudiente_data = Personas::where('id', $estudiante->fk_parentesco)->first();
        $matricula = Matriculas::where('fk_estudiante', $estudiante->fk_estudiante)->first();
        $sede = Sedes::where('id', $estudiante_data->fk_sede)->first();
        $centro = Centros_Educativos::where('identificacion', $sede->fk_centro_educativo)->first();
        if (is_object($matricula)) {
          $extra_info = Informaciones_Adicionales::where('fk_matricula', $matricula->id)->first();
          $grado = Grados::where('id', $extra_info->fk_grado)->first();
          $periodo = Periodos_Academicos::where('id', $grado->fk_periodo_academico)->first();
          $areas_estudiantes = Estudiantes_Areas::where('fk_estudiante', $estudiante->fk_estudiante)->get();
          foreach ($areas_estudiantes as $area) {
            $area_data = Areas::where('id', $area->fk_areas)->first();
            $areas[] = [
              'nombre' => $area_data->nombre,
              'intensidad' => $area_data->intensidad,
              'id' => $area_data->id,
              'nota' => $area->nota_parcial
            ];
          }
          $estudiantes[] = [
            'estudiante'    => $estudiante_data,
            'acudiente'     => $acudiente_data,
            'sede'          => $sede,
            'centro'        => $centro,
            'matricula'     => $matricula,
            'extra_info'    => $extra_info,
            'grado'         => $grado,
            'periodo'       => $periodo,
            'areas'         => $areas,
          ];
        } else {
          $estudiantes[] = [
            'estudiante'    => $estudiante_data,
            'acudiente'     => $acudiente_data,
            'sede'          => $sede,
            'centro'        => $centro,
          ];
        }
      };
      $data = array(
        'status'    => 'success',
        'code'      => 200,
        'data'     => $estudiantes
      );
    } else {
      $data = array(
        'status'    => 'Not Resources',
        'code'      => 200,
        'messages'     => 'No hay estudiantes registrados'
      );
    }
    return $data;
  }

  public function usersBySede($id)
  {
    $usuarios = [];
    $personas = Personas::where('fk_sede', $id)->where('tipo', "!=", "Administrador")->get();
    if (count($personas)) {
      foreach ($personas as $persona) {
        $correo = Correos_Electronicos::where('fk_persona', $persona->id)->first();

        if (is_object($correo)) {
          $usuario = Usuarios::where('fk_correo', $correo->id)->first();
          $sede = Sedes::find($persona->fk_sede);
          $rol = Roles::where('id', $usuario->fk_rol)->first();
          $usuarios[] = [
            'id_user'           =>  $usuario->id,
            'id'                =>  $persona->id,
            'usuario'           =>  $usuario->nombre_usuario,
            'status'            =>  $usuario->estado,
            'fecha_registro'    =>  $usuario->fecha_registro,
            'email'             =>  $correo->direccion_correo,
            'p_nombre'          =>  $persona->p_nombre,
            'p_apellido'        =>  $persona->p_apellido,
            'dni'               =>  $persona->dni,
            'sede'              =>  $sede->nombre,
            'rol'               =>  $rol->nombre,
            'rol_id'            =>  $rol->id,
          ];
        }
      }
      $response = response()->json(array(
        'status'    => 'Success',
        'data'      => $usuarios
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Resources not found',
        'message'   => 'no se han registrado usuarios en el sistema'
      ), 404);
    }
    return $response;
  }
}
