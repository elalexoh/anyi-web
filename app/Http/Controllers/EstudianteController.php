<?php

namespace App\Http\Controllers;

use App\Estudiantes_Grados;
use App\Models\Areas;
use App\Models\Correos_Electronicos;
use App\Models\Estudiantes_Areas;
use App\Models\Familiares;
use App\Models\Grados;
use App\Models\Informaciones_Adicionales;
use App\Models\Matriculas;
use App\Models\Periodos_Academicos;
use Illuminate\Http\Request;
use App\Models\Personas;
use App\Models\Sedes;
use App\Models\Usuarios;

class EstudianteController extends Controller
{
  public function indexEstudiantesbySede($id)
  {
    $sede = Sedes::where('id', $id)->first();
    $estudiantes = Personas::where('tipo', 'Estudiante')->where('fk_sede', $sede->id)->get();

    return $estudiantes;
  }
  public function indexEstudiantesbyGrado($id)
  {
    // $grado = Grados::find($id);
    $estudiantes = Personas::where('tipo', 'estudiante')->get();
    $estudiantes_sede = [];
    if (!!count($estudiantes)) {

      foreach ($estudiantes as $estudiante) {
        $estudiantes_sede[] = [
          'estudiante' => $estudiante,
          'sede' => $estudiante->sedes,
          'notas' => Estudiantes_Areas::where('fk_estudiante', $estudiante->id)->first()
        ];
      }
      $response = response()->json(array(
        'status'    => 'Sucess',
        'message'   => 'Exito, hemos encontrado al estudiante',
        'data'      => $estudiantes_sede
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'not resources',
        'message'   => 'No hemos encontrado al estudiantes',
      ), 404);
    }
    return $response;
  }
  public function indexEstudiantesbySedeAndGrado($sede, $grado)
  {
    $estudiantes = [];
    $estudiantes_grados = Estudiantes_Grados::where('fk_grado', $grado)->get();
    if (count($estudiantes_grados)) {
      foreach ($estudiantes_grados as $estudiante) {
        $estudiante_query = Personas::where('id', $estudiante->fk_estudiante)->where('fk_sede', $sede)->first();
        $sede_data = Sedes::find($sede);
        if (is_object($estudiante_query)) {
          $estudiantes[] = [
            'estudiante' => $estudiante_query,
            'estatus'    => $estudiante->estatus,
            'sede'    => $sede_data,
          ];
        }
      };
      $response = response()->json(array(
        'status'    => 'Success',
        'data'      => $estudiantes,
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Not Resources',
        'message'   => 'No hay estudiantes para este grado',
      ), 404);
    }
    return $response;
  }

  public function getEstudiante($id)
  {
    $estudiante = Personas::where('tipo', 'Estudiante')->where('id', $id)->first();
    $__areas = [];

    if (is_object($estudiante)) {
      $estudiante_grado = Estudiantes_Grados::where('fk_estudiante', $estudiante->id)->first();
      $grado = Grados::where('id', $estudiante_grado->fk_grado)->first();
      $periodo = Periodos_Academicos::where('id', $grado->fk_periodo_academico)->first();
      $docente = Personas::where('id', $grado->fk_persona)->first();
      $docente_mail = Correos_Electronicos::where('fk_persona', $docente->id)->first();
      $docente_user = Usuarios::where('fk_correo', $docente_mail->id)->first();
      $areas = Estudiantes_Areas::where('fk_estudiante', $estudiante->id)->get();
      $extra_info = Informaciones_Adicionales::where('fk_estudiante', $estudiante->id)->first();

      if (count($areas)) {
        foreach ($areas as $area) {
          $__areas[] = Areas::where('id', $area->fk_areas)->first();
        }
      }

      $data = array(
        'p_nombre'      => $estudiante->p_nombre,
        's_nombre'      => $estudiante->s_nombre,
        'p_apellido'    => $estudiante->p_apellido,
        's_apellido'    => $estudiante->s_apellido,
        'dni'           => $estudiante->dni,
        'sede'          => $estudiante->sedes->nombre,
        'sede_full'     => $estudiante->sedes->centros_educativos,
        'grado'         => $grado,
        'periodo'       => $periodo,
        'docente'       => $docente,
        'docente_user'  => $docente_user->id,
        'areas'         => $__areas,
        'extra_info'    => $extra_info,
      );
      $response = response()->json(array(
        'status'    => 'Sucess',
        'code'      =>  200,
        'message'   => 'Exito, hemos encontrado al estudiante',
        'data'      => $data
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Not Found',
        'code'      =>  404,
        'message'   => 'No hemos encontrado al estudiante',
      ), 404);
    }

    return $response;
  }

  public function getNotaById($id)
  {
    $nota = Estudiantes_Areas::find($id);
    if (is_object($nota)) {
      $response = response()->json(array(
        'status'    => 'Sucess',
        'message'   => 'Exito, hemos encontrado la nota',
        'data'      => $nota
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'not resources',
        'message'   => 'No hemos encontrado la nota',
      ), 404);
    }
    return $response;
  }

  public function updateNota(Request $request, $id)
  {
    $nota = Estudiantes_Areas::find($id);

    if (is_object($nota)) {
      $nota->nota_parcial   = is_null($request->input('nota_parcial')) ? $nota->nota_parcial : $request->input('nota_parcial');
      $nota->update();
      $response = response()->json(array(
        'status'    => 'Sucess',
        'message'   => 'Exito, hemos encontrado la nota',
        'data'      => $nota
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'not resources',
        'message'   => 'No hemos encontrado la nota',
      ), 404);
    }
    return $response;
  }

  public function getNotas($id)
  {
    $notas = Estudiantes_Areas::where('fk_estudiante', $id)->get();
    $notas_areas = [];
    if (!!count($notas)) {
      foreach ($notas as $nota) {
        $notas_areas[] = [
          'nota' => $nota,
          'area' => Areas::where('id', $nota->fk_areas)->first()
        ];
      }
      $response = response()->json(array(
        'status'    => 'Sucess',
        'message'   => 'Exito, hemos encontrado la nota',
        'data'      => $notas_areas
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'not resources',
        'message'   => 'No hemos encontrado la nota',
      ), 404);
    }
    return $response;
  }

  public function getEstudiantesByAcudiente($id)
  {
    $estudiante_data = [];

    //conseguir el usuario
    $usuario = Usuarios::where('id', $id)->first();

    if (is_object($usuario)) {
      // conseguir el correo
      $correo = Correos_Electronicos::where('id', $usuario->fk_correo)->first();

      // conseguir a la persona
      $acudiente = Personas::where('id', $correo->fk_persona)->first();

      // buscar en familiares los estudiantes
      $familiares = Familiares::where('fk_parentesco', $acudiente->id)->get();

      if (count($familiares)) {
        foreach ($familiares as $estudiante) {
          // detallar cada estudiante
          $estudiante = Personas::where('id', $estudiante->fk_estudiante)->first();
          $estudiante_grado = Estudiantes_Grados::where('fk_estudiante', $estudiante->id)->where('estatus', 'cursando')->first();
          if ($estudiante_grado) {
            $grado = Grados::where('id', $estudiante_grado->fk_grado)->first();
          } else {
            $grado = [];
          }

          $estudiante_data[] = [
            'direccion'         => $estudiante->direccion,
            'dni'               => $estudiante->dni,
            'fecha_nacimiento'  => $estudiante->fecha_nacimiento,
            'sede'              => Sedes::where('id', $estudiante->fk_sede)->first()->nombre,
            'id'                => $estudiante->id,
            'img_foto'          => $estudiante->img_foto,
            'p_apellido'        => $estudiante->p_apellido,
            'p_nombre'          => $estudiante->p_nombre,
            's_apellido'        => $estudiante->s_apellido,
            's_nombre'          => $estudiante->s_nombre,
            'sexo'              => $estudiante->sexo,
            'tipo'              => $estudiante->tipo,
            'grado'             => $grado,
          ];
        }
      } else {
        $response = response()->json(array(
          'status'    => 'not resources',
          'message'   => 'No hemos encontrado familiares',
        ), 404);
      }
      $response = response()->json(array(
        'status'    => 'Success',
        'data'      => $estudiante_data,
      ), 200);
    } else {
      $response = response()->json(array(
        'status'    => 'not resources',
        'message'   => 'Usuario no registrado',
      ), 404);
    }
    return $response;
  }

  public function endCource(Request $request, $id)
  {
    $estudiantes = [];
    if (count($request->estudiantes)) {
      foreach ($request->estudiantes as $estudiante) {
        $estudiante_grado = Estudiantes_Grados::where('fk_estudiante', $estudiante['estudiante']['id'])->where('fk_grado', $id)->first();
        $estudiante_grado->estatus = 'cursado';
        $estudiante_grado->update();
      }
      $response = response()->json($estudiantes, 200);
    } else {
      $response = response()->json(array(
        'status'    => 'Empty Resources',
        'message'   => 'Deben exitir estudiantes para cerrar el grado',
      ), 404);
    }
    return $response;
  }

  public function angieindex()
  {
    return 'hola angie';
  }
}
