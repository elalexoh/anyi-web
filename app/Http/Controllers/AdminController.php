<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\Usuarios;
use App\Models\Roles;
use App\Models\Correos_Electronicos;
use App\Models\Telefonos;
use App\Models\Personas;
use App\Models\Sedes;
use App\Models\Lugares;
use App\Models\Centros_Educativos;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

  // Listar docentes
  public function indexDocente()
  {
    $rolDocente = Roles::where('nombre', 'DOCENTE')->first();
    $rolDocenteAdmin = Roles::where('nombre', 'DOCENTE ADMINISTRATIVO')->first();

    $docentes = Usuarios::where('fk_rol', $rolDocente->id)->orWhere('fk_rol', $rolDocenteAdmin->id)->get();

    if ($docentes) {
      foreach ($docentes as $item) {
        $email = Correos_Electronicos::find($item->fk_correo);
        $persona = Personas::find($email->fk_persona);
        $sede = Sedes::find($persona->fk_sede);
        $rol = Roles::where('id', $item->fk_rol)->first();

        // Datos del docente
        $docente[] = [
          'id'                =>  $item->id,
          'usuario'           =>  $item->nombre_usuario,
          'status'            =>  $item->estado,
          'fecha_registro'    =>  $item->fecha_registro,
          'email'             =>  $email->direccion_correo,
          'p_nombre'          =>  $persona->p_nombre,
          'p_apellido'        =>  $persona->p_apellido,
          'dni'               =>  $persona->dni,
          'sede'              =>  $sede->nombre,
          'rol'               =>  $rol->nombre,
          'persona'           =>  $persona
        ];
      }

      $data = $docente;
    } else {
      $data = array(
        'status'    => 'Not found',
        'code'      =>  404,
        'message'   =>  'No hay docentes registrados en la plataforma'
      );
    }
    return $data;
  }

  // Borrar docente
  public function deleteDocente($id)
  {
    $docente = Usuarios::find($id);

    if (is_object($docente)) {

      /*Se debe borrar tambien el registro del docente de la tabla personas?*/
      $docente->delete();

      $data = array(
        'status'    => 'success',
        'code'      =>  200,
        'message'   => 'El docente se ha eliminado satistafactotiamente',
      );
    } else {
      $data = array(
        'status'    => 'error',
        'code'      =>  404,
        'message'   => 'Ha ocurrido un error con la solicitud',
      );
    }

    return response()->json($data, 200);
  }

  // Crear docente
  public function createDocente(Request $request)
  {
    // return $request;
    // Recibir datos del post
    $params_array = array(
      'id_rol'      =>  $request->input('id_rol'),
      'id_sede'     =>  $request->input('id_sede'),
      'p_nombre'    =>  $request->input('p_nombre'),
      's_nombre'    =>  $request->input('s_nombre'),
      'p_apellido'  =>  $request->input('p_apellido'),
      's_apellido'  =>  $request->input('s_apellido'),
      'dni'         =>  $request->input('dni'),
      'fechaNac'    =>  $request->input('fechaNac'),
      'codArea'     =>  $request->input('codArea'),
      'phoneNumber' =>  $request->input('phoneNumber'),
      'email'       =>  $request->input('email'),
      'estado'      =>  $request->input('estado'),
      'username'    =>  $request->input('username'),
      'password'    =>  Hash::make($request->input('password')),
      'responsable' =>  $request->input('responsable'),
      // direccion
      'direccion'    =>  $request->input('direccion'),
      'img_foto'     =>  $request->file('img_foto'),
    );
    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // Validamos los datos
      $validate = Validator::make($params_array, [
        'id_rol'      => 'required|integer',
        'id_sede'     => 'required|integer',
        'p_nombre'    => 'required|alpha',
        's_nombre'    => 'nullable|alpha',
        'p_apellido'  => 'required|alpha',
        's_apellido'  => 'nullable|alpha',
        'responsable' => 'nullable',
        'dni'         => 'required|unique:App\Models\Personas,dni',
        'fechaNac'    => 'required',
        'codArea'     => 'required',
        'phoneNumber' => 'required',
        'email'       => 'required|email|unique:App\Models\Correos_Electronicos,direccion_correo',
        'estado'      => 'required',
        'username'    => 'required|unique:App\Models\Usuarios,nombre_usuario',
        'password'    => 'required',
        'img_foto'    => 'nullable|mimes:jpg,bmp,png'
      ]);

      if ($validate->fails()) {
        // Validaciones fallan
        $data = response()->json(array(
          'status'    =>  'error',
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        ), 404);
      } else {
        // Los datos fueron validados correctamente

        // DOCENTE O DOCENTE ADMINISTRATIVO?
        $rol = Roles::find($params->id_rol);

        if (is_object($rol)) {

          $persona = new Personas();
          //guardar foto
          $url_img_foto = null;
          if ($request->file('img_foto')) {
            $img_foto                   = $request->file('img_foto')->store('usuarios/fotos/', 'public');
            $url_img_foto               = asset('storage/' . '' . $img_foto);
          }
          //obtener url de la foto
          $persona->dni               = $params->dni;
          $persona->tipo              = $rol->nombre;
          $persona->p_nombre          = $params->p_nombre;
          $persona->s_nombre          = $params->s_nombre;
          $persona->p_apellido        = $params->p_apellido;
          $persona->s_apellido        = $params->s_apellido;
          $persona->fecha_nacimiento  = $params->fechaNac;
          $persona->direccion         = $params->direccion;
          $persona->fk_sede           = $params->id_sede;
          $persona->img_foto          = $url_img_foto;
          $persona->save();

          $telefono = new Telefonos();
          $telefono->cod_area     = $params->codArea;
          $telefono->numero_telf  = $params->phoneNumber;
          $telefono->fk_persona   = $persona->id;
          $telefono->save();

          $correo = new Correos_Electronicos();
          $correo->direccion_correo   = $params->email;
          $correo->fk_persona         = $persona->id;
          $correo->save();

          $today = new DateTime();

          $usuario = new Usuarios();
          $usuario->nombre_usuario    = $params->username;
          $usuario->clave_usuario     = $params->password;
          $usuario->nombre_usuario    = $params->username;
          $usuario->fecha_registro    = $today->format('Y-m-d H:i:s');
          $usuario->politicas         = false;
          $usuario->estado            = $params->estado;
          $usuario->fk_rol            = $rol->id;
          $usuario->fk_correo         = $correo->id;
          $usuario->save();

          if ($params->responsable == true) {
            $sede = Sedes::where('id', $persona->fk_sede)->first();
            $sede->fk_responsable = $persona->id;
            $sede->update();
          }

          $data = response()->json(array(
            'status'    =>  'success',
            'message'   =>  'Docente registrado exitosamente'
          ), 200);
        } else {
          $data = response()->json(array(
            'status'    =>  'error',
            'message'   =>  'Ha ocurrido un problema'
          ), 400);
        }
      }
    } else {
      // Si los datos están vacíos.
      $data = response()->json(array(
        'status'    =>  'error',
        'message'   =>  'No se han recibido los datos',
        'datos'     => $request->input()
      ), 404);
    }
    return $data;
  }

  // Seleccion de docente a modificar
  public function editDocente($id)
  {
    $usuario = Usuarios::find($id);

    if (is_object($usuario)) {

      $correo = Correos_Electronicos::find($usuario->fk_correo);
      $persona = Personas::find($correo->fk_persona);
      $sede = Sedes::find($persona->fk_sede);
      $telefono = Telefonos::where('fk_persona', $persona->id)->first();

      if (is_object($telefono)) {
        $cod_area = $telefono->cod_area;
        $numero_telf = $telefono->numero_telf;
      } else {
        $cod_area = 100;
        $numero_telf = 1000000;
      }

      $data = array(
        'nombre_usuario'    =>  $usuario->nombre_usuario,
        'clave_usuario'     =>  $usuario->clave_usuario,
        'estado'            =>  $usuario->estado,
        'correo'            =>  $correo->direccion_correo,
        'cod_area'          =>  $cod_area,
        'numero_telf'       =>  $numero_telf,
        'p_nombre'          =>  $persona->p_nombre,
        's_nombre'          =>  $persona->s_nombre,
        'p_apellido'        =>  $persona->p_apellido,
        's_apellido'        =>  $persona->s_apellido,
        'dni'               =>  $persona->dni,
        'fecha_nacimiento'  =>  $persona->fecha_nacimiento,
        'direccion'         =>  $persona->direccion,
        'img_foto'          =>  $persona->img_foto,
        'responsable'       =>  $sede->fk_responsable === $persona->id,
      );
    } else {

      $data = array(
        'status'    => 'error',
        'code'      =>  400,
        'message'   => 'Ha ocurrido un error'
      );
    }
    return response()->json($data);
  }

  // Actualizar docente
  public function updateDocente(Request $request, $id)
  {
    // return $request;
    $today = new DateTime();

    $usuario = Usuarios::find($id);
    $usuario->nombre_usuario        = $request->input('nombre_usuario');
    $usuario->estado                = $request->input('estado');
    $usuario->descripcion_modif     = $request->input('descripcion');
    $usuario->fecha_modificacion    = $today->format('Y-m-d H:i:s');
    $usuario->update();

    $correo = Correos_Electronicos::find($usuario->fk_correo);
    $correo->direccion_correo   = $request->input('correo');
    $correo->update();

    $persona = Personas::find($correo->fk_persona);
    //guardar foto
    $img_foto        = is_null($request->file('img_foto')) ? $persona->img_foto : $request->file('img_foto')->store('usuarios/fotos/', 'public');

    //obtener url de foto
    $url_img_foto    = is_null($request->file('img_foto')) ? $persona->img_foto : asset('storage/' . '' . $img_foto);

    $persona->p_nombre          = is_null($request->input('p_nombre')) ? $persona->p_nombre ? null : $persona->p_nombre : $request->input('p_nombre');
    //!
    $persona->s_nombre          = $request->input('s_nombre') === 'null' ? null : $request->input('s_nombre');
    $persona->p_apellido        = is_null($request->input('p_apellido')) ? $persona->p_apellido ? null : $persona->p_apellido : $request->input('p_apellido');
    //!
    $persona->s_apellido        = $request->input('s_apellido') === 'null' ? null : $request->input('s_apellido');
    $persona->dni               = is_null($request->input('dni')) ? $persona->dni : $request->input('dni');
    $persona->fecha_nacimiento  = is_null($request->input('fecha_nacimiento')) ? $persona->fecha_nacimiento : $request->input('fecha_nacimiento');
    $persona->direccion         = is_null($request->input('direccion')) ? $persona->direccion : $request->input('direccion');
    $persona->img_foto          = $url_img_foto;
    $persona->update();

    $telefono = Telefonos::where('fk_persona', $persona->id)->first();
    if (is_object($telefono)) {
      $telefono->cod_area     = $request->input('cod_area');
      $telefono->numero_telf  = $request->input('numero_telf');
      $telefono->update();
    }
    if ($request->input('responsable') == true) {
      $sede = Sedes::where('id', $persona->fk_sede)->first();
      $sede->fk_responsable = $persona->id;
      $sede->update();
    }

    $data = array(
      'status'    =>  'success',
      'code'      =>   200,
      'message'   =>  'Usuario actualizado correctamente'
    );

    return response()->json($data, 200);
  }

  // Listar centros educativos
  public function indexCentros()
  {
    $centros = Centros_Educativos::all();
    // $centro_direccion = [];
    // if (!!count($centros)) {
    //     foreach ($centros as $centro) {
    //         $direccion = Direcciones::where('id', $centro->fk_direccion)->first();
    //         $pais = Lugares::where('id', $direccion->fk_pais)->first();
    //         $estado = Lugares::where('id', $direccion->fk_estado)->first();
    //         $municipio = Lugares::where('id', $direccion->fk_municipio)->first();
    //         $centro_direccion[] = [
    //             'centro' => $centro,
    //             'pais' => $pais->nombre,
    //             'estado' => $estado->nombre,
    //             'municipio' => $municipio->nombre,
    //         ];
    //     }
    // }
    return $centros;
  }

  // Selección de centro educativo a modificar
  public function editCentro($id)
  {

    $centro = Centros_Educativos::find($id);

    if (is_object($centro)) {
      $data = $centro;
    } else {

      $data = array(
        'status'    => 'error',
        'code'      =>  400,
        'message'   => 'Ha ocurrido un error'
      );
    }
    return response()->json($data, 200);
  }

  // Actualizar centro educativo
  public function updateCentro(Request $request, $id)
  {
    // Verificamos que tenga permisos

    $params_array = array(
      'identificacion'    => $request->input('identificacion'),
      'nombre'            => $request->input('nombre'),
      'resolucion'        => $request->input('resolucion'),
      'dane'              => $request->input('dane'),
      'consecutivo_sede'  => $request->input('consecutivo_sede'),
      'nit'               => $request->input('nit'),
      'nombre_autorizado' => $request->input('nombre_autorizado'),
      'cargo'             => $request->input('cargo'),
      'direccion'         => $request->input('direccion'),
      'logo'              => $request->file('logo'),
      'firma'             => $request->file('firma'),
      'dni_director'      => $request->input('dni_director'),
      'lugar_expedicion'  => $request->input('lugar_expedicion'),
    );

    // return $request;
    $params = (object) $params_array;
    $centro = Centros_Educativos::find($id);

    if (is_object($centro)) {

      //guardar imagenes
      $img_logo        = is_null($request->file('logo')) ? $centro->img_logo : $request->file('logo')->store('centros/logos', 'public');
      $img_firma       = is_null($request->file('firma')) ? $centro->img_firma : $request->file('firma')->store('centros/firmas', 'public');

      //obtener las urls de las imagenes
      $url_img_logo    = is_null($request->file('logo')) ? $centro->img_logo : asset('storage/' . '' . $img_logo);
      $url_img_firma   = is_null($request->file('firma')) ? $centro->img_firma : asset('storage/' . '' . $img_firma);

      $centro->nombre             = $params->nombre;
      $centro->resolucion         = $params->resolucion;
      $centro->dane               = $params->dane;
      $centro->consecutivo_sede   = $params->consecutivo_sede;
      $centro->nit                = $params->nit;
      $centro->nombre_autorizado  = $params->nombre_autorizado;
      $centro->cargo              = $params->cargo;
      $centro->direccion          = $params->direccion;
      $centro->dni_director       = $params->dni_director;
      $centro->lugar_expedicion   = $params->lugar_expedicion;
      $centro->img_logo           = $url_img_logo;
      $centro->img_firma          = $url_img_firma;
      $centro->update();

      $data = response()->json(array(
        'status'    =>  'success',
        'code'      =>   200,
        'message'   =>  'Matricula actualizada correctamente'
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    => 'error',
        'code'      =>  400,
        'message'       => 'Ha ocurrido un error'
      ), 400);
    }

    return $data;
  }
  // Obtener roles
  public function getRoles()
  {
    $roles = Roles::where('nombre', 'DOCENTE')->orWhere('nombre', 'DOCENTE ADMINISTRATIVO')->get();
    return $roles;
  }

  // Obtener sedes
  public function getSedes()
  {
    $sedes = Sedes::all()->toArray();
    return $sedes;
  }

  // Obtener lugares
  public function getLugares()
  {
    $lugares = Lugares::all()->toArray();
    return $lugares;
  }
}
