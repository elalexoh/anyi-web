<?php

namespace App\Http\Controllers;

use App\Models\Centros_Educativos;
use Illuminate\Http\Request;
use App\Models\Sedes;
use App\Models\Usuarios;
use App\Models\Personas;
use App\Models\Correos_Electronicos;

class SedeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function indexSedes()
  {
    $sedes_personas = [];
    $sedes = Sedes::all();
    if (count($sedes)) {
      foreach ($sedes as $sede) {
        $persona = Personas::where('id', $sede->fk_responsable)->first();
        $sedes_personas[] = [
          "id"                     => $sede->id,
          "nombre"                 => $sede->nombre,
          "dane"                   => $sede->dane,
          "consecutivo_sede"       => $sede->consecutivo_sede,
          "fk_centro_educativo"   => $sede->fk_centro_educativo,
          "fk_lugar"               => $sede->fk_lugar,
          "responsable"           => $persona,
        ];
      }
      $data = response()->json($sedes_personas, 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Resource not Found',
        'message'   =>  'No se han encontrado sedes',
      ), 404);
    }
    return $data;
  }

  //obtener sede del usuario
  public function getSede($id)
  {
    $usuario = Usuarios::find($id);

    try {
      $correo = Correos_Electronicos::find($usuario->fk_correo);
      $persona = Personas::find($correo->fk_persona);
      $sede = Sedes::where('id', $persona->fk_sede)->first();
      $data = response()->json(array(
        'name'    =>  $sede->nombre,
        'id'      =>   $sede->id,
      ), 200);
    } catch (\Throwable $th) {
      $data = response()->json(array(
        'status'    =>  'error',
        'code'      =>   404,
        'message'   =>  'Sede no encontrada',
        'data' => $usuario
      ), 404);
    }

    return $data;
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */


  public function getSedesbyCentro($id)
  {
    $sedes = Sedes::where('fk_centro_educativo', $id)->get();

    if (count($sedes)) {
      $data = response()->json(array(
        'status'    =>  'Success',
        'data'    =>  $sedes,
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'error',
        'code'      =>   404,
        'message'   =>  'No hay sedes asignadas para este centro',
      ), 404);
    }
    return $data;
  }
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}
