<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Areas;
use App\Models\Estudiantes_Areas;
use App\Models\Grados;
use App\Models\Periodos_Academicos;
use App\Models\Personas;

class AreaController extends Controller
{
  public function indexAreas()
  {
    $areas = Areas::all();
    // $areas_detalle = [];

    if (!!count($areas)) {
      foreach ($areas as $area) {
        $areas_detalle[] = [
          'area' => $area,
          'periodo_academico' => Periodos_Academicos::find($area->fk_periodo_academico),
          'grado' => Grados::find($area->fk_grado),
        ];
      }
      $data = response()->json(array(
        'status'    =>  'Success',
        'data'      =>  $areas_detalle
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Resources not found',
        'message'    =>  'No hay registros.',
      ), 404);
    }
    return $data;
  }

  public function createArea(Request $request)
  {
    $params_array = array(
      "nombre"               => $request->input('nombre'),
      "intensidad"           => $request->input('carga_horaria'),
      "fk_grado"             => $request->input('grado_periodo.grado.id'),
      "fk_periodo_academico" => $request->input('grado_periodo.periodo_academico.id'),
    );

    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // validamos los datos
      $validate = \Validator::make($params_array, [
        "nombre"               => 'required|string',
        "intensidad"           => 'required|integer',
        "fk_grado"             => 'required|integer',
        "fk_periodo_academico" => 'required|integer',
      ]);

      if (!$validate->fails()) {
        $Area = new Areas();
        $Area->nombre               = $params->nombre;
        $Area->intensidad           = $params->intensidad;
        $Area->fk_grado             = $params->fk_grado;
        $Area->fk_periodo_academico = $params->fk_periodo_academico;
        $Area->save();

        //! nose en que momento se crea esto pero es necesario para listar las areas luego
        // $estudiante_area = new Estudiantes_Areas();
        // $estudiante_area->nota_parcial = 00.0; //default
        // $estudiante_area->fk_matricula = 4; //default
        // $estudiante_area->fk_estudiante = 17; //default
        // $estudiante_area->fk_areas = $Area->id; //default

        $data = array(
          'status'    => 'success',
          'code'      => 200,
          'message'   => 'Usuario registrado satisfactoriamente',
          'params'    => $params
        );
      } else {
        $data = array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        );
      }
    }
    return $data;
  }

  public function update(Request $request, $id)
  {
    $area = Areas::find($id);
    if (is_object($area)) {
      try {
        $area->nombre = is_null($request->input('nombre')) ? $area->nombre : $request->input('nombre');
        $area->intensidad = is_null($request->input('intensidad')) ? $area->intensidad : $request->input('intensidad');
        $area->update();

        $data = response()->json(array(
          'status'    =>  'Success',
          'message'    =>  'Actualización completada',
        ), 200);
      } catch (\Throwable $th) {
        $data = response()->json(array(
          'status'    =>  'Internal error',
          'message'    =>  'Error al actualizar',
        ), 500);
      }
    } else {
      $data = response()->json(array(
        'status'    =>  'Resources not found',
        'message'    =>  'No hay registros.',
      ), 404);
    }
    return $data;
  }

  public function destroy($id)
  {
    $area = Areas::find($id);
    if (is_object($area)) {
      $data = response()->json(array(
        'status'    =>  'Success',
        'data'      =>  $area
      ), 200);
      $area->delete();
    } else {
      $data = response()->json(array(
        'status'    =>  'Resources not found',
        'message'    =>  'No hay registros.',
      ), 404);
    }
    return $data;
  }

  public function getArea($id)
  {
    $area = Areas::find($id);
    if (is_object($area)) {
      $data = response()->json(array(
        'status'    =>  'Resources not found',
        'message'    =>  'No hay registros.',
      ), 200);
    } else {
      $data = response()->json(array(
        'status'    =>  'Resources not found',
        'message'    =>  'Registro no encontrado.',
      ), 404);
    }
    return $area;
  }

  public function areasByGrado($id)
  {
    $grado = Grados::where('id', $id)->first();

    if (is_object($grado)) {
      $areas = Areas::where('fk_grado', $grado->id)->get();
      if (count($areas)) {
        $data = response()->json(array(
          'status'    =>  'Success',
          'data'      =>  $areas
        ), 200);
      } else {
        $data = response()->json(array(
          'status'    =>  'Not resources',
          'message'   =>  'No se han encontrado areas para este grado'
        ), 404);
      }
    } else {
      $data = response()->json(array(
        'status'    =>  'Error',
        'message'   =>  'El grado que busca no existe'
      ), 404);
    }
    return $data;
  }
  public function calificar(Request $request)
  {
    $params_array = array(
      "nota"              => $request->input('nota'),
      "id_area"           => $request->input('id_area'),
      "id_estudiante"     => $request->input('id_estudiante'),
    );

    $params = (object) $params_array;

    if (!empty($params) && !empty($params_array)) {

      // validamos los datos
      $validate = \Validator::make($params_array, [
        "nota"              => 'required',
        "id_area"           => 'required|integer',
        "id_estudiante"     => 'required|integer',
      ]);

      if (!$validate->fails()) {
        $calificacion = new Estudiantes_Areas();
        $calificacion->nota_parcial         = $params->nota;
        $calificacion->fk_estudiante        = $params->id_estudiante;
        $calificacion->fk_areas             = $params->id_area;
        $calificacion->save();

        $data = array(
          'status'    => 'success',
          'code'      => 200,
          'message'   => 'Registro exitoso',
          'params'    => $params
        );
      } else {
        $data = array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'Ha ocurrido un problema con la validación de los datos',
          'errors'    => $validate->errors()
        );
      }
    }
    return $data;
  }
  public function areasByDocente($id)
  {
    $persona = Personas::findOrFail($id);
    $areas_arr = [];
    $grados = Grados::where('fk_persona', $persona->id)->get();
    if (count($grados)) {
      foreach ($grados as $grado) {
        $areas = Areas::where('fk_grado', $grado->id)->get();
        if (count($grados)) {
          foreach ($areas as $area) {
            $areas_arr[] = [
              'area' => $area,
              'periodo_academico' => Periodos_Academicos::find($area->fk_periodo_academico),
              'grado' => Grados::find($area->fk_grado),
            ];
          }
          $response = response()->json([
            "status" => "Success",
            "data" => $areas_arr,
          ], 200);
        } else {
          $response = response()->json([
            "status" => "Error",
            "message" => "no se encontraron areas registradas",
          ], 204);
        }
      }
    } else {
      $response = response()->json([
        "status" => "Error",
        "message" => "no se encontraron grados registrados",
      ], 204);
    }
    return $response;
  }
}
