<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Direcciones extends Model
{
    protected   $table      = 'direccion';
    public      $timestamps = false;

    protected $fillable = [
        'fk_pais', 'fk_municipio', 'fk_estado'
    ];
}
