<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estudiantes_Areas extends Model
{
  protected   $table      = 'estudiantes_areas';
  public      $timestamps = false;

  protected $fillable = ['nota_parcial'];

  //Relacion de muchos a uno con la tabla matriculas
  public function matriculas()
  {
    return $this->belongsTo('App\Models\matriculas', 'fk_matricula');
  }

  //Relacion de muchos a uno con la tabla areas
  public function areas()
  {
    return $this->belongsTo('App\Models\areas', 'fk_area');
  }

  //Relacion muchos a muchos con la tabla evaluaciones
  public function evaluaciones()
  {
    return $this->belongsToMany('App\Models\evaluaciones', 'estudiantes_evaluaciones', 'fk_area', 'fk_estudiante', 'fk_matricula', 'fk_evaluacion');
  }
}
