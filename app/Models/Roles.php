<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected   $table      = 'roles';
    public      $timestamps = false;

    protected $fillable = ['nombre'];

    //Relacion muchos a muchos con la tabla privilegios
    public function privilegios(){
        return $this->belongsToMany('App\Models\privilegios' ,'roles_privilegios', 'fk_rol', 'fk_privilegio');
    }

    // Relación uno a muchos con la tabla usuarios
    public function usuarios(){
        return $this->HasMany('App\Models\usuarios' ,'fk_rol');
    }

}
