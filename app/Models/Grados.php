<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grados extends Model
{
    protected   $table      = 'grados';
    public      $timestamps = false;

    protected $fillable = ['nombre'];

    //Relacion muchos a muchos con la tabla periodos academicos
    public function periodos_academicos()
    {
        return $this->belongsToMany('App\Models\periodos_academicos', 'grados_periodos', 'fk_grado', 'fk_periodo_academico');
    }

    //Relacion de muchos a uno con la tabla personas
    public function personas()
    {
        return $this->belongsTo('App\Models\personas', 'fk_persona');
    }

    //Relacion de muchos a uno con la tabla sedes
    public function sede()
    {
        return $this->belongsTo('App\Models\sedes', 'fk_sede');
    }
}
