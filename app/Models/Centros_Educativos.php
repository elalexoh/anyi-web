<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Centros_Educativos extends Model
{
    protected   $table      = 'centros_educativos';
    public      $timestamps = false;

    protected $fillable = [
        'nombre', 'resolucion', 'dane', 'consecutivo_sede', 'nit', 'nombre_autorizado', 'cargo', 'img_logo', 'img_firma'
    ];

    // Relación uno a muchos con la tabla sedes
    public function sedes()
    {
        return $this->HasMany('App\Models\sedes', 'fk_centro_educativo');
    }

    protected $primaryKey = 'identificacion';
}
