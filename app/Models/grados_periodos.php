<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class grados_periodos extends Model
{
    
    protected   $table      = 'grados_periodos';
    public      $timestamps = false;

    //Relacion de muchos a uno con la tabla grados
    public function grados(){
        return $this->belongsTo('App\Models\grados' ,'fk_grado');
    }

    //Relacion de muchos a uno con la tabla grados
    public function periodos_academicos(){
        return $this->belongsTo('App\Models\periodos_academicos' ,'fk_periodo_academico');
    }

    // Relación uno a muchos con la tabla sedes
    public function areas(){
        return $this->HasMany('App\Models\areas' ,'fk_grado', 'fk_persona', 'fk_periodo_academico');
    }


    
}
