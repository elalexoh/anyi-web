<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Periodos_Academicos extends Model
{
    protected   $table      = 'periodos_academicos';
    public      $timestamps = false;

    protected $fillable = [
        'nombre','fecha_inicio','fecha_fin'
    ];

    //Relacion muchos a muchos con la tabla periodos academicos
    public function grados(){
        return $this->belongsToMany('App\Models\grados' ,'grados_periodos', 'fk_periodo_academico', 'fk_grado');
    }

}
