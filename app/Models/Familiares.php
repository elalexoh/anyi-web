<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Familiares extends Model
{
    protected   $table      = 'familiares';
    public      $timestamps = false;

    //Relacion de muchos a uno con la tabla personas
    public function personas(){
        return $this->belongsTo('App\Models\personas' ,'fk_parentesco');
    }

    //Relacion de muchos a uno con la tabla personas
    // public function personas(){
    //     return $this->belongsTo('App\Models\personas' ,'fk_estudiante');
    // }
}
