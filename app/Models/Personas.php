<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Personas extends Model
{
  protected   $table      = 'personas';
  public      $timestamps = false;

  protected $fillable = [
    'dni', 'tipo', 'p_nombre', 's_nombre', 'p_apellido', 's_apellido', 'fecha_nacimiento', 'img_foto'
  ];

  // Relación uno a muchos con la tabla certificados
  public function certificados()
  {
    return $this->HasMany('App\Models\certificados', 'fk_persona');
  }

  // Relación uno a muchos con la tabla correos_electronicos
  public function correos_electronicos()
  {
    return $this->HasMany('App\Models\correos_electronicos', 'fk_persona');
  }

  // Relación uno a muchos con la tabla grados
  public function grados()
  {
    return $this->HasMany('App\Models\grados', 'fk_persona');
  }

  // Relación uno a muchos con la tabla matriculas
  public function matriculas()
  {
    return $this->HasMany('App\Models\matriculas', 'fk_estudiante');
  }

  // Relación uno a muchos con la tabla telefonos
  public function telefonos()
  {
    return $this->HasMany('App\Models\telefonos', 'fk_persona');
  }

  //Relacion de muchos a uno con la tabla lugares
  public function lugares()
  {
    return $this->belongsTo('App\Models\lugares', 'fk_lugar');
  }

  //Relacion de muchos a uno con la tabla sedes
  public function sedes()
  {
    return $this->belongsTo('App\Models\sedes', 'fk_sede');
  }

  //Relacion muchos a muchos con la tabla personas
  public function personas1()
  {
    return $this->belongsToMany('App\Models\personas', 'familiares', 'fk_parentesco', 'fk_estudiante');
  }

  //Relacion muchos a muchos con la tabla personas
  public function personas2()
  {
    return $this->belongsToMany('App\Models\personas', 'familiares', 'fk_estudiante', 'fk_parentesco');
  }

  // Relación uno a muchos con la tabla foros
  public function foros()
  {
    return $this->HasMany('App\Models\foros', 'fk_estudiante');
  }
}
