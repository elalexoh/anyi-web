<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reset_Pass extends Model
{
    protected   $table      = 'reset_pass';
    public      $timestamps = false;

    protected $fillable = [
        'token_recuperacion','fecha_solicitud'
    ];

    //Relacion de muchos a uno con la tabla usuarios
    public function usuarios(){
        return $this->belongsTo('App\Models\usuarios' ,'fk_usuario');
    }
}
