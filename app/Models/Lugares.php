<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lugares extends Model
{
    protected   $table      = 'lugares';
    public      $timestamps = false;

    protected $fillable = [
        'nombre','tipo'
    ];

    // Relación uno a muchos con la tabla personas
    public function personas(){
        return $this->HasMany('App\Models\personas' ,'fk_lugar');
    }

    // Relación uno a muchos con la tabla sedes
    public function sedes(){
        return $this->HasMany('App\Models\sedes' ,'fk_lugar');
    }

    // Relación uno a muchos con la tabla personas
    public function lugares1(){
        return $this->HasMany('App\Models\lugares' ,'fk_lugar');
    }

    //Relacion de muchos a uno con la tabla lugares
    public function lugares2(){
        return $this->belongsTo('App\Models\lugares' ,'fk_lugar');
    }
}
