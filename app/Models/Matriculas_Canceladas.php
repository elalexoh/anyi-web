<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matriculas_Canceladas extends Model
{
    protected   $table      = 'matriculas_canceladas';
    public      $timestamps = false;

    protected $fillable = [
        'motivo_cancelacion','destino_estudiante','fecha_cancelacion'
    ];

    // Relación uno a muchos con la tabla foros
    public function foros(){
        return $this->HasMany('App\Models\foros' ,'fk_cancela_matricula');
    }

    //Relacion de muchos a uno con la tabla matriculas
    public function matriculas(){
        return $this->belongsTo('App\Models\matriculas' ,'fk_matricula', 'fk_estudiante');
    }
}
