<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Correos_Electronicos extends Model
{
    protected   $table      = 'correos_electronicos';
    public      $timestamps = false;

    protected $fillable = [
        'direccion_correo'
    ];

    //Relacion de muchos a uno con la tabla personas
    public function personas(){
        return $this->belongsTo('App\Models\personas' ,'fk_persona');
    }

    // Relación uno a muchos con la tabla usuarios
    public function usuarios(){
        return $this->HasMany('App\Models\usuarios' ,'fk_correo');
    }
}
