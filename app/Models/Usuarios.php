<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
	protected   $table      = 'usuarios';
	public      $timestamps = false;

	protected $fillable = [
		'nombre_usuario', 'politicas', 'fecha_politicas', 'clave_usuario', 'fecha_registro', 'fecha_modificacion', 'descripcion_modif', 'estado'
	];

	// Relación uno a muchos con la tabla certificados
	public function certificados()
	{
		return $this->HasMany('App\Models\certificados', 'fk_usuario');
	}

	// Relación uno a muchos con la tabla foros
	public function foros_comenta()
	{
		return $this->HasMany('App\Models\foros', 'fk_usuario_comenta');
	}

	// Relación uno a muchos con la tabla foros
	public function foros_notifica()
	{
		return $this->HasMany('App\Models\foros', 'fk_usuario_notifica');
	}

	// Relación uno a muchos con la tabla reset_pass
	public function reset_pass()
	{
		return $this->HasMany('App\Models\reset_pass', 'fk_usuario');
	}

	//Relacion de muchos a uno con la tabla correos_electronicos
	public function correos_electronicos()
	{
		return $this->belongsTo('App\Models\correos_electronicos', 'fk_correo');
	}

	//Relacion de muchos a uno con la tabla roles
	public function roles()
	{
		return $this->belongsTo('App\Models\roles', 'fk_rol');
	}
}
