<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Informaciones_Adicionales extends Model
{
    protected   $table      = 'informaciones_adicionales';
    public      $timestamps = false;

    protected $fillable = [
        'tipo', 'enfermedades_sufridas', 'sisben', 'estrato_socioeconomico', 'nro_hermanos', 'posicion_hermanos', 'preocedencia', 'nuevo', 'remitente', 'grado'
    ];

    //Relacion de muchos a uno con la tabla matriculas
    public function matriculas()
    {
        return $this->belongsTo('App\Models\matriculas', 'fk_matricula', 'fk_estudiante');
    }
}
