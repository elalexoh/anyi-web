<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificados extends Model
{
    protected   $table      = 'certificados';
    public      $timestamps = false;

    protected $fillable = [
        'nombre', 'descripcion', 'estatus', 'url_certificado'
    ];

    //Relacion de muchos a uno con la tabla personas
    public function personas()
    {
        return $this->belongsTo('App\Models\personas', 'fk_persona');
    }

    //Relacion de muchos a uno con la tabla usuarios
    public function usuarios()
    {
        return $this->belongsTo('App\Models\usuarios', 'fk_usuario');
    }
}
