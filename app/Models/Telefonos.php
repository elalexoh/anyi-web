<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telefonos extends Model
{
    protected   $table      = 'telefonos';
    public      $timestamps = false;

    protected $fillable = [
        'cod_area', 'numero_telf'
    ];

    //Relacion de muchos a uno con la tabla personas
    public function personas()
    {
        return $this->belongsTo('App\Models\personas', 'fk_persona');
    }

    //Relacion de muchos a uno con la tabla sedes
    public function sedes()
    {
        return $this->belongsTo('App\Models\sedes', 'fk_sede');
    }
}
