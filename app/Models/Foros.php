<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foros extends Model
{
    protected   $table      = 'foros';
    public      $timestamps = false;

    protected $fillable = [
        'tipo','descripcion','fecha_creacion','estatus'
    ];

    //Relacion de muchos a uno con la tabla usuarios
    public function usuarios(){
        return $this->belongsTo('App\Models\usuarios' ,'fk_usuario_comenta');
    }

    //Relacion de muchos a uno con la tabla usuarios
    public function usuarios2(){
        return $this->belongsTo('App\Models\usuarios' ,'fk_usuario_notifica');
    }

    //Relacion de muchos a uno con la tabla personas
    public function personas(){
        return $this->belongsTo('App\Models\personas' ,'fk_estudiante');
    }

    //Relacion de muchos a uno con la tabla matriculas_canceladas
    public function matriculas_canceladas(){
        return $this->belongsTo('App\Models\matriculas_canceladas' ,'fk_cancela_matricula');
    }
    
}
