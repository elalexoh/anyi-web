<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    protected   $table      = 'areas';
    public      $timestamps = false;

    protected $fillable = [
        'nombre', 'intensidad'
    ];

    //Relacion de muchos a uno con la tabla grados
    public function grados_periodos()
    {
        return $this->belongsTo('App\Models\grados_periodos', 'fk_grado', 'fk_persona', 'fk_periodo_academico');
    }

    //Relacion muchos a muchos con la tabla matriculas
    public function matriculas()
    {
        return $this->belongsToMany('App\Models\matriculas', 'estudiantes_areas', 'fk_area', 'fk_matricula');
    }
}
