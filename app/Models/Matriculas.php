<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matriculas extends Model
{
    protected   $table      = 'matriculas';
    public      $timestamps = false;

    protected $fillable = [
        'fecha_matricula', 'img_firma_acudiente', 'img_firma_estudiante', 'estatus', 'fecha_renovacion', 'religion', 'img_cedula_papa', 'img_cedula_mama'
    ];

    //Relacion muchos a muchos con la tabla areas
    public function areas()
    {
        return $this->belongsToMany('App\Models\areas', 'estudiantes_areas', 'fk_matricula', 'fk_area');
    }

    //Relacion de muchos a uno con la tabla personas
    public function personas()
    {
        return $this->belongsTo('App\Models\personas', 'fk_estudiante');
    }

    // Relación uno a muchos con la tabla matriculas_canceladas
    public function matriculas_canceladas()
    {
        return $this->HasMany('App\Models\matriculas_canceladas', 'fk_matricula', 'fk_estudiante');
    }

    // Relación uno a muchos con la tabla informaciones_adicionales
    public function informaciones_adicionales()
    {
        return $this->HasMany('App\Models\informaciones_adicionales', 'fk_matricula', 'fk_estudiante');
    }
}
