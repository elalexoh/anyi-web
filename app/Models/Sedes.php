<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sedes extends Model
{
    protected   $table      = 'sedes';
    public      $timestamps = false;

    protected $fillable = ['nombre', 'dane', 'consecutivo_sede'];


    //Relacion de muchos a uno con la tabla lugares
    public function lugares()
    {
        return $this->belongsTo('App\Models\lugares', 'fk_lugar');
    }

    //Relacion de muchos a uno con la tabla lugares
    public function centros_educativos()
    {
        return $this->belongsTo('App\Models\centros_educativos', 'fk_centro_educativo');
    }

    // Relación uno a muchos con la tabla telefonos
    public function telefonos()
    {
        return $this->HasMany('App\Models\telefonos', 'fk_sede');
    }

    // Relación uno a muchos con la tabla personas
    public function personas()
    {
        return $this->HasMany('App\Models\personas', 'fk_sede');
    }

    // Relación uno a muchos con la tabla grados
    public function grados()
    {
        return $this->HasMany('App\Models\grados', 'fk_sede');
    }
}
