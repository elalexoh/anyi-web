<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Usuarios;
use App\Models\Reset_Pass;

class ResetPasswordReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $usuarios;
    public $reset_pass;

    public function __construct(Usuarios $usuarios, Reset_Pass $reset_pass)
    {
        $this->usuarios = $usuarios;
        $this->reset_pass = $reset_pass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.reset_password');
    }
}
