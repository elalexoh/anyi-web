<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiantes_Grados extends Model
{
    protected   $table      = 'estudiantes_grados';
    public      $timestamps = false;
}
