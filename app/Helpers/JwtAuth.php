<?php

namespace App\Helpers;

use App\Models\Centros_Educativos;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Usuarios;
use App\Models\Roles;
use App\Models\Correos_Electronicos;
use App\Models\Personas;
use App\Models\Sedes;

class JwtAuth
{

  public $key;

  public function __construct()
  {
    $this->key = 'ClaveSecretaAnyi2611';
  }

  public function signup($user, $password, $getToken = null)
  {

    // Buscar si existe el usuario con las credenciales
    $usuario = Usuarios::where('nombre_usuario', $user)->first();

    if (is_object($usuario)) {

      // Validación de contraseña correcta
      if (!Hash::check($password, $usuario->clave_usuario)) {

        $data = array(
          'status'    => 'error',
          'code'      => 404,
          'message'   => 'Datos ingresados no son correctos'
        );
      } else {
        // Si el usuario y contraseña son correctos

        $rol = Roles::where('id', $usuario->fk_rol)->first();
        $email = Correos_Electronicos::where('id', $usuario->fk_correo)->first();
        $persona = Personas::where('id', $email->fk_persona)->first();
        $sede = Sedes::where('id', $persona->fk_sede)->first();

        $token = array(
          'id_user'       =>  $usuario->id,
          'usuario'       =>  $usuario->nombre_usuario,
          'email'         =>  $email->direccion_correo,
          'estado'        =>  $usuario->estado,
          'id_rol'        =>  $rol->id,
          'rol'           =>  $rol->nombre,
          'sede'          =>  $sede->nombre,
          'sede_full'     =>  $sede,
          'centro_full'   =>  Centros_Educativos::where('identificacion', $sede->fk_centro_educativo)->first(),
          'sede_id'       =>  $sede->id,
          'id_persona'    =>  $persona->id,
          'politicas'     =>  $usuario->politicas,
          'iat'           =>  time(),
          'exp'           =>  time() + (7 * 24 * 60 * 60) // Una semana de duración.
        );

        $jwt = JWT::encode($token, $this->key, 'HS256');  // La key es una clave que posee el desarrollador
        $decoded = JWT::decode($jwt, $this->key, ['HS256']);

        // Devolver los datos decodificados o el token, en función de un parámetro
        if (is_null($getToken)) {
          $data = $jwt;
        } else {
          $data = $decoded;
        }
      }
    } else {
      $data = array(
        'status'    => 'error',
        'code'      => 404,
        'message'   => 'Datos ingresados no son correctos',
        'form'      => $user
      );
    }

    return $data;
  }

  public function checkToken($jwt, $getIdentity = false)
  {

    // Se usará en las distintas acciones para verificar el token del usuario logueado.

    $auth = false;

    try {
      $jwt = str_replace('"', '', $jwt);
      $decoded = JWT::decode($jwt, $this->key, ['HS256']);
    } catch (\UnexpectedValueException $e) {
      $auth = false;
    } catch (\DomainException $e) {
      $auth = false;
    }

    if (!empty($decoded) && is_object($decoded) && isset($decoded->id_user)) {
      $auth = true;
    } else {
      $auth = false;
    }

    if ($getIdentity) {
      return $decoded;
    }

    return $auth;
  }
}
