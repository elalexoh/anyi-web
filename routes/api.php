<?php



// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
// header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('acudiente/{id}/estudiantes', 'EstudianteController@getEstudiantesByAcudiente');
Route::post('auth', 'UsuarioController@verifyUser');
Route::get('roles', 'RolController@index');

Route::group(['prefix' => 'rol'], function () {
  Route::post('add', 'RolController@create');
  Route::get('edit/{id}', 'RolController@edit');
  Route::match(array('PUT', 'PATCH'), 'update/{id}', 'RolController@update');
  Route::delete('delete/{id}', 'RolController@delete');
});

Route::group(['prefix' => 'admin'], function () {

  Route::get('docentes', 'AdminController@indexDocente');
  Route::get('acudientes', 'DocenteAdministrativoController@indexAcudientes');
  Route::get('getRoles', 'AdminController@getRoles');
  Route::get('getSedes', 'AdminController@getSedes');
  Route::get('getLugares', 'AdminController@getLugares');
  Route::post('add/docente', 'AdminController@createDocente');
  Route::get('edit/docente/{id}', 'AdminController@editDocente');
  Route::match(array('POST', 'PATCH'), 'update/docente/{id}', 'AdminController@updateDocente'); //!cambiado de put a post para enviar el formdata
  Route::delete('delete/docente/{id}', 'AdminController@deleteDocente');

  //Centros
  Route::get('centros', 'AdminController@indexCentros');
  Route::get('edit/centro/{id}', 'AdminController@editCentro');
  Route::match(array('POST', 'PATCH'), 'update/centro/{id}', 'AdminController@updateCentro'); //!cambiado de put a post para enviar el formdata
});

Route::post('login', 'UsuarioController@login');
Route::post('update', 'UsuarioController@update');
Route::post('sendEmailToken', 'UsuarioController@sendEmailToken');
Route::post('checkToken', 'UsuarioController@checkTokenReset');
Route::post('changePassReset', 'UsuarioController@changePassReset');
Route::post('changePass', 'UsuarioController@changePass');

// Docente administrador
Route::group(['prefix' => 'docadmin'], function () {

  // usuarios
  Route::get('usuarios', 'DocenteAdministrativoController@indexUsuarios'); //List
  Route::get('sedes/{id}/usuarios', 'DocenteAdministrativoController@usersBySede'); //usersBySede
  Route::get('usuarios/all', 'DocenteAdministrativoController@indexUsuariosAll'); //List
  Route::get('usuario/{id}', 'DocenteAdministrativoController@getUsuario'); //get

  Route::post('add/usuario', 'DocenteAdministrativoController@createUsuario'); //New

  Route::post('edit/usuario/{id}', 'DocenteAdministrativoController@updateUsuario'); //Update //!cambiado de put a post para enviar el formdata
  Route::delete('delete/usuario/{id}', 'DocenteAdministrativoController@deleteUsuario'); //Delete
  Route::get('usuarios/estudiantes', 'DocenteAdministrativoController@indexEstudiantes'); //all estudiantes

  // estudiantes
  Route::get('sede/{id}/estudiantes', 'EstudianteController@indexEstudiantesbySede'); //get
  Route::get('grado/{id}/estudiantes', 'EstudianteController@indexEstudiantesbyGrado'); //get
  Route::get('sede/{sede}/grado/{grado}/estudiantes/', 'EstudianteController@indexEstudiantesbySedeAndGrado'); //get
  Route::get('estudiante/{id}', 'EstudianteController@getEstudiante'); //get

  // notas
  Route::get('estudiante/{id}/notas', 'EstudianteController@getNotas'); //get
  Route::get('nota/{id}', 'EstudianteController@getNotaById'); //get
  Route::put('nota/{id}', 'EstudianteController@updateNota'); //get

  // areas
  Route::get('areas', 'AreaController@indexAreas'); //get areas
  Route::get('docente/{id}/areas', 'AreaController@areasByDocente'); //get areas
  Route::get('area/{id}', 'AreaController@getArea'); //get area

  Route::get('grado/{id}/areas', 'AreaController@areasByGrado'); //new
  Route::put('area/{id}', 'AreaController@update'); //get area
  Route::post('add/area', 'AreaController@createArea'); //new
  Route::delete('delete/area/{id}', 'AreaController@destroy'); //new

  Route::post('add/calificacion', 'AreaController@calificar'); // nueva calificacion


  Route::get('docentes-index', 'DocenteAdministrativoController@indexDocentes'); //get areas

  // grados
  Route::get('grados', 'GradoController@indexGrados'); //list
  Route::get('grados/all', 'GradoController@index'); //list
  Route::get('sede/{id}/grados/all', 'GradoController@gradosBySede'); //list
  Route::post('grado/add', 'GradoController@asignarGrados');
  Route::get('grado/{id}', 'GradoController@getGrado');
  Route::delete('grado/{id}', 'GradoController@deleteGrado');
  Route::get('grado/{id}/areas', 'GradoController@getAreasbyGrado');
  Route::put('grado/{id}', 'GradoController@editGrado');

  //matriculas
  Route::get('matriculas', 'MatriculaController@indexMatriculas'); // all
  Route::get('sedes/{id}/matriculas', 'MatriculaController@matriculasBySede'); // bySede
  Route::get('acudientes/{id}/matriculas', 'MatriculaController@matriculasByAcudiente'); // byAcudiente

  Route::get('matricula/{id}', 'MatriculaController@getMatricula');
  Route::post('add/matricula', 'MatriculaController@createMatricula'); //New
  Route::post('edit/matricula/{id}', 'MatriculaController@updateMatricula'); //Update //!cambiado de put a post para enviar el formdata
  Route::post('cancel/matricula/{id}', 'MatriculaController@cancelMatricula'); //cancel matricula
  Route::get('matricula/{id}/status/{status}', 'MatriculaController@updateStatus'); //update status matricula

  // certificados
  Route::get('certificados', 'CertificadoController@indexCertificados'); //all
  Route::get('certificados/sede/{id}', 'CertificadoController@getCertificados'); //all by sede
  Route::get('solicitud/{id}', 'CertificadoController@getSolicitud');
  Route::put('edit/solicitud/{id}', 'CertificadoController@updateEstatusCertificado');
  Route::post('add/certificado', 'CertificadoController@createCertificado');

  // sedes
  Route::get('sede/{id}', 'SedeController@getSede'); //get sede
  Route::get('sedes', 'SedeController@indexSedes'); //get sedes
  Route::get('sede/{id}/responsable', 'DocenteAdministrativoController@getResponsableBySede'); //get docente responsable
  Route::get('/centro/{id}/sedes/', 'SedeController@getSedesbyCentro');

  // periodos academcos
  Route::get('periodos-academicos', 'PeriodosAcademicosController@index');
  Route::put('periodo-academico', 'PeriodosAcademicosController@update');
});


//notificaciones
Route::get('docente/{id}/notifications/unread', 'NotificacionesController@NotificationsUnread');
Route::get('docente/{id_docente}/estudiante/{id_estudiante}/fullNotas', 'NotificacionesController@checkFullNotas');
Route::get('docente/{id}/notificaciones', 'NotificacionesController@index');
Route::post('docente/notificacion/new', 'NotificacionesController@create');
Route::post('docente/notificacion/{id}', 'NotificacionesController@load');

// lugar Endpoint
Route::get('paises', 'LugarController@indexPaises');

Route::get('pais/{id}/estados', 'LugarController@indexEstados');

Route::get('estado/{id}/municipios', 'LugarController@indexMunicipios');

// Foros
Route::put('comentarios/{id}', 'ForoController@updateComment');
Route::get('comentarios/usuario/{id}', 'ForoController@index');
Route::post('comentarios', 'ForoController@store');

Route::get('pdf/matricula/{id}', 'PdfController@getMatricula');
Route::get('pdf/certificado/{id}', 'PdfController@getCertificado'); //acudiente
Route::get('pdf/certificado/{id}/firmado/{isFirmed}', 'PdfController@getCertificadoFirmed'); //doc/docadm

Route::put('usuario/{id}/politicas', 'UsuarioController@updatePoliticas');

Route::put('grado/{id}/estudiantes/terminar-curso', 'EstudianteController@endCource');

Route::get('angie', 'EstudianteController@angieindex');
